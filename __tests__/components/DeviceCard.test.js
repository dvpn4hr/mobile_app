import 'react-native';
import React from 'react';
import DeviceCard from '../../src/components/DeviceCard';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import i18n from 'i18n-js';
import { colors } from '../../src/common';
import { mockedNavigate } from '../../jest.setup';
import { PERFECT_DIAG_CODE } from '../../src/Constants';

const initialState = {
  entities: {
    devices: {
      0: {
        id: 0,
        name: 'Device 0',
        status: 0,
      },
      1: {
        id: 1,
        name: 'Device 1',
        status: 1,
      },
      2: {
        id: 2,
        name: 'Device 2',
        status: 2,
        diag_code: PERFECT_DIAG_CODE,
      },
      3: {
        id: 3,
        name: 'Device 3',
        status: 3,
        diag_code: PERFECT_DIAG_CODE,
      },
    },
  },
};

const setup = props => render(<DeviceCard {...props} />, { initialState });

describe('<DeviceCard /> Test', () => {
  it('should assert main components exist', () => {
    const { getByText, getByTestId, queryByText } = setup({ entity_id: 0 });
    expect(getByText(i18n.t('off'))).toBeDefined();
    expect(getByText(i18n.t('VPN'))).toBeDefined();
    expect(getByTestId('shield')).toBeDefined();
    expect(getByTestId('circle').props.style[1].backgroundColor).toBe(
      colors.gray
    );
    expect(queryByText(i18n.t('grant_access'))).toBeNull();
  });

  it('should assert main components exist for device 1', () => {
    const { getByText, getByTestId } = setup({ entity_id: 1 });
    expect(getByText(i18n.t('disabled'))).toBeDefined();
    expect(getByText(i18n.t('VPN'))).toBeDefined();

    expect(getByTestId('shield')).toBeDefined();
    expect(getByTestId('circle').props.style[1].backgroundColor).toBe(
      colors.gray
    );
  });

  it('should assert main components exist for device 2', () => {
    const { getByText, getByTestId } = setup({ entity_id: 2 });
    expect(getByText(i18n.t('VPN'))).toBeDefined();
    expect(getByText(i18n.t('ready_for_people'))).toBeDefined();
    expect(getByTestId('shield')).toBeDefined();
    expect(getByTestId('circle').props.style[1].backgroundColor).toBe(
      colors.success
    );
    expect(getByText(i18n.t('grant_access'))).toBeDefined();
  });

  it('should assert main components exist for device 3', () => {
    const { getByText, getByTestId } = setup({ entity_id: 3 });
    expect(getByText(i18n.t('VPN'))).toBeDefined();
    expect(getByText(i18n.t('sync_can_take_up_to'))).toBeDefined();
    expect(getByTestId('shield')).toBeDefined();
    expect(getByTestId('circle').props.style[1].backgroundColor).toBe(
      colors.white
    );
  });

  it('should navigate to grant access when button pressed', () => {
    const { getByText } = setup({ entity_id: 2 });
    fireEvent.press(getByText(i18n.t('grant_access')));
    expect(mockedNavigate).toHaveBeenCalledWith('GrantAccess', {
      deviceId: 2,
    });
  });

  it('should navigate to device when card pressed', () => {
    const { getByTestId } = setup({ entity_id: 2 });
    fireEvent.press(getByTestId('circle'));
    expect(mockedNavigate).toHaveBeenCalledWith('Device', {
      deviceId: 2,
    });
  });
});

import 'react-native';
import React from 'react';
import InputText from '../../src/components/InputText';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent } from '@testing-library/react-native';
import { mockedGoBack } from '../../jest.setup';
import { colors } from '../../src/common';

const setup = props => render(<InputText {...props} />);

const onChangeText = jest.fn();
const TextProps = {
  leftIcon: 'account',
  placeholder: 'placeholder',
  onChangeText,
};

describe('<InputText /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByPlaceholderText } = setup(TextProps);
    expect(getByTestId('leftIcon')).toBeDefined();
    expect(getByPlaceholderText('placeholder')).toBeDefined();
  });

  it('should update text when user inputs', () => {
    const { getByPlaceholderText, getByTestId } = setup(TextProps);
    fireEvent.changeText(getByPlaceholderText('placeholder'), 'test');
    expect(onChangeText).toHaveBeenCalledWith('test');
    expect(getByTestId('clearIcon')).toBeDefined();
    fireEvent.press(getByTestId('clearIcon'));
    expect(onChangeText).toHaveBeenCalledWith('');
  });

  it('should show secure input for password inputs', () => {
    const { getByTestId, getByPlaceholderText } = setup({
      ...TextProps,
      secureTextEntry: true,
    });
    fireEvent.changeText(getByPlaceholderText('placeholder'), 'test');
    expect(getByTestId('passwordIcon')).toBeDefined();
    fireEvent.press(getByTestId('passwordIcon'));
    expect(getByTestId('textInput').props.secureTextEntry).toBe(false);
  });

  it('should be initialized with defaultValue', () => {
    const { getByPlaceholderText } = setup({
      ...TextProps,
      defaultValue: 'test',
    });
    expect(getByPlaceholderText('placeholder').props.value).toBe('test');
  });

  it('should show an error if passed', () => {
    const { getByText } = setup({
      ...TextProps,
      error: 'error',
    });
    expect(getByText('error')).toBeDefined();
  });

  it('should show bottom border if showBottomBorder is true', () => {
    const { getByTestId } = setup({
      ...TextProps,
      showBottomBorder: true,
    });
    expect(getByTestId('inputContainer').props.style[3].borderBottomWidth).toBe(
      1.5
    );
  });

  it('should show an image if source is passed', () => {
    const { getByTestId } = setup({
      ...TextProps,
      source: 'test',
    });
    expect(getByTestId('img')).toBeDefined();
  });

  it('should update border color if TextInput is focused', () => {
    const { getByPlaceholderText, getByTestId } = setup({
      ...TextProps,
    });

    fireEvent(getByPlaceholderText('placeholder'), 'onFocus');
    expect(getByTestId('inputContainer').props.style[2].borderColor).toBe(
      colors.main_color
    );
  });

  it('should update border color if TextInput is blurred', () => {
    const { getByPlaceholderText, getByTestId } = setup({
      ...TextProps,
    });

    fireEvent(getByPlaceholderText('placeholder'), 'onFocus');
    fireEvent(getByPlaceholderText('placeholder'), 'onBlur');
    expect(getByTestId('inputContainer').props.style[2].borderColor).toBe(
      colors.colorWithAlpha('white', 0.2)
    );
  });
});

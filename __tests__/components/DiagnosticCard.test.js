import 'react-native';
import React from 'react';
import faker from 'faker';
import DiagnosticCard from '../../src/components/DiagnosticCard';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import { showArticle } from 'react-native-zendesk';

const description = faker.lorem.sentence();
const articleId = faker.random.number();
const articleId_1 = faker.random.number();
const initialState = {
  entities: {
    diagnostics: {
      0: {
        id: 0,
        description,
        state: true,
        url: faker.internet.url() + `/${articleId}`,
      },
      1: {
        id: 1,
        description,
        state: false,
        url: faker.internet.url() + `/${articleId_1}`,
      },
    },
  },
};

const setup = props => render(<DiagnosticCard {...props} />, { initialState });

describe('<DiagnosticCard /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText } = setup({
      entity_id: 0,
    });

    expect(getByTestId('diagnostic-card')).toBeDefined();
    expect(getByText(description)).toBeDefined();
    expect(getByTestId('chevron-right')).toBeDefined();
    expect(getByTestId('diagnostic-card-icon')).toBeDefined();
  });

  it('should assert onPress', () => {
    const { getByTestId } = setup({
      entity_id: 0,
    });

    fireEvent.press(getByTestId('diagnostic-card'));
    expect(showArticle).toHaveBeenCalledWith({
      articleId: String(articleId),
      hideContactSupport: true,
    });
  });

  it('should assert onPress with state false', () => {
    const { getByTestId } = setup({
      entity_id: 1,
    });

    fireEvent.press(getByTestId('diagnostic-card'));
    expect(showArticle).toHaveBeenCalledWith({
      articleId: String(articleId_1),
      hideContactSupport: true,
    });
  });
});

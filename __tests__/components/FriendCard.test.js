import 'react-native';
import React from 'react';
import faker from 'faker';
import FriendCard from '../../src/components/FriendCard';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import { mockedNavigate } from '../../jest.setup';

const name = faker.name.findName();
const initialState = {
  entities: {
    friends: {
      0: {
        name,
      },
    },
  },
};

const setup = props => render(<FriendCard {...props} />, { initialState });

describe('<FriendCard /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText } = setup({
      entity_id: 0,
    });
    expect(getByText(name)).toBeDefined();
    expect(getByTestId('arrow-icon')).toBeDefined();
  });

  it('should navigate to friend details', () => {
    const { getByTestId } = setup({
      entity_id: 0,
    });
    fireEvent.press(getByTestId('arrow-icon'));
    expect(mockedNavigate).toHaveBeenCalledWith('Friend', {
      entity_id: 0,
    });
  });
});

import 'react-native';
import React from 'react';
import faker from 'faker';
import AlertDialog from '../../src/components/AlertDialog';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';

const setup = props => render(<AlertDialog {...props} />);

const iconSrc = faker.image.image();
const title = faker.lorem.sentence();
const description = faker.lorem.sentence();
const actions = [
  {
    title: faker.lorem.sentence(),
    onPress: jest.fn(),
  },
  {
    title: faker.lorem.sentence(),
    onPress: jest.fn(),
  },
];

const visibleProps = {
  visible: true,
  iconSrc,
  title,
  description,
  actions,
};

describe('<AlertDialog /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText, queryByTestId } = setup(visibleProps);
    expect(queryByTestId('alert-dialog')).toBeDefined();
    expect(getByTestId('icon-src').props.source).toBe(iconSrc);
    expect(getByText(title)).toBeDefined();
    expect(getByText(description)).toBeDefined();
    expect(getByText(actions[0].title)).toBeDefined();
    expect(getByText(actions[1].title)).toBeDefined();
  });

  it('should assert main components not exist', () => {
    const { queryByTestId } = setup({
      visible: false,
    });
    expect(queryByTestId('alert-dialog')).toBeNull();
  });

  it('should have default value for visible = false', () => {
    const { queryByTestId } = setup({});
    expect(queryByTestId('alert-dialog')).toBeNull();
  });

  it('should be able call onPress of first action', () => {
    const { getByText } = setup(visibleProps);
    fireEvent.press(getByText(actions[0].title));
    expect(actions[0].onPress).toHaveBeenCalled();
  });

  it('should be able call onPress of second action', () => {
    const { getByText } = setup(visibleProps);
    fireEvent.press(getByText(actions[1].title));
    expect(actions[1].onPress).toHaveBeenCalled();
  });
});

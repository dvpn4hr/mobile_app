import 'react-native';
import React from 'react';
import faker from 'faker';
import NavBar from '../../src/components/NavBar';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import { mockedGoBack } from '../../jest.setup';
import i18n from 'i18n-js';
import { colors } from '../../src/common';

const setup = props => render(<NavBar {...props} />);

describe('<NavBar />', () => {
  it('should assert main components exist', () => {
    const { getByTestId, queryByText } = setup();
    expect(getByTestId('close')).toBeTruthy();
    expect(queryByText(i18n.t('compatibility_test'))).not.toBeTruthy();
  });

  it('should assert title exist', () => {
    const { getByTestId, getByText } = setup({ showTitle: true });
    expect(getByText(i18n.t('compatibility_test').toUpperCase())).toBeTruthy();
  });

  it('should assert title not exist', () => {
    const { getByTestId, queryByText } = setup({ showTitle: false });
    expect(
      queryByText(i18n.t('compatibility_test').toUpperCase())
    ).not.toBeTruthy();
  });

  it('should assert onBackPress', () => {
    const { getByTestId } = setup();
    fireEvent.press(getByTestId('close'));
    expect(mockedGoBack).toHaveBeenCalled();
  });

  it('should assert onBackPress with title', () => {
    const title = faker.lorem.word();
    const { getByTestId, getByText } = setup({ showTitle: true, title });

    fireEvent.press(getByTestId('close'));
    expect(mockedGoBack).toHaveBeenCalled();
    expect(getByText(title.toUpperCase())).toBeTruthy();
  });

  it('should update tint color with default color', () => {
    const { getByTestId } = setup({});
    expect(getByTestId('close').props.style[1].tintColor).toBe(colors.white);
  });

  it('should update tint color with custom color', () => {
    const { getByTestId } = setup({ color: colors.black });
    expect(getByTestId('close').props.style[1].tintColor).toBe(colors.black);
  });
});

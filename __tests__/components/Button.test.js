import 'react-native';
import React from 'react';
import faker from 'faker';
import Button from '../../src/components/Button';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';

const setup = props => render(<Button {...props} />);

const ButtonProps = {
  title: faker.lorem.sentence(),
  onPress: jest.fn(),
};

describe('<Button /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText } = setup(ButtonProps);
    expect(getByTestId('button-secondary')).toBeDefined();
    expect(getByText(ButtonProps.title)).toBeDefined();
  });

  it('should show loader when isLoading = true', () => {
    const { getByTestId } = setup({
      ...ButtonProps,
      isLoading: true,
    });
    expect(getByTestId('activity-indicator')).toBeDefined();
  });

  it('should accept different button types', () => {
    const { getByTestId } = setup({
      ...ButtonProps,
      type: Button.types.PRIMARY,
    });
    expect(getByTestId('button-primary')).toBeDefined();
  });

  it('should call onPress when button is pressed', () => {
    const { getByTestId } = setup(ButtonProps);
    fireEvent.press(getByTestId('button-secondary'));
    expect(ButtonProps.onPress).toHaveBeenCalledTimes(1);
  });

  it('should render text with underline when underline = true', () => {
    const { getByText } = setup({
      ...ButtonProps,
      underline: true,
    });
    expect(getByText(ButtonProps.title).props.style).toContainEqual({
      textDecorationLine: 'underline',
    });
  });

  it('should disable button when type = disabled', () => {
    const { getByTestId } = setup({
      ...ButtonProps,
      type: Button.types.DISABLED,
    });
    expect(getByTestId('button-disabled')).toBeDefined();
  });

  it('should not call onPress when button is disabled', () => {
    const { getByTestId } = setup({
      ...ButtonProps,
      type: Button.types.DISABLED,
    });
    fireEvent.press(getByTestId('button-disabled'));
    expect(ButtonProps.onPress).not.toHaveBeenCalled();
  });

  it('should disable button when isLoading = true', () => {
    const { getByTestId } = setup({
      ...ButtonProps,
      isLoading: true,
    });
    fireEvent.press(getByTestId('button-secondary'));
    expect(ButtonProps.onPress).not.toHaveBeenCalled();
  });
});

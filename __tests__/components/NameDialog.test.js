import 'react-native';
import React from 'react';
import faker from 'faker';
import NameDialog from '../../src/components/NameDialog';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import { mockedGoBack } from '../../jest.setup';
import i18n from 'i18n-js';
import { colors } from '../../src/common';
import { updateDevice } from '../../src/actions';

jest.mock('../../src/actions', () => ({
  updateDevice: jest.fn(({ id, name }) => {
    return {
      type: 'UPDATE_DEVICE',
      payload: {
        id,
        name,
      },
    };
  }),
}));

const setup = props => render(<NameDialog {...props} />);
const onClose = jest.fn();
const id = faker.random.uuid();
const name = faker.name.findName();
const Props = {
  visible: true,
  onClose,
  device: {
    id,
    name,
  },
};

describe('<NameDialog /> Test', () => {
  it('should assert main components exist', () => {
    const { getByText, getByPlaceholderText } = setup(Props);
    expect(getByText(i18n.t('pod_name'))).toBeDefined();
    expect(getByPlaceholderText(i18n.t('pod_name')).props.value).toBe(name);
  });

  it('should close when cancel button pressed', () => {
    const { getByText } = setup(Props);
    fireEvent.press(getByText(i18n.t('cancel')));
    expect(onClose).toHaveBeenCalled();
  });

  it('should update device name when update button pressed', () => {
    const { getByText, getByPlaceholderText } = setup(Props);
    fireEvent.changeText(getByPlaceholderText(i18n.t('pod_name')), 'new name');
    fireEvent.press(getByText(i18n.t('update')));
    expect(updateDevice).toHaveBeenCalledWith({ id, name: 'new name' });
    expect(onClose).toHaveBeenCalled();
  });
});

import 'react-native';
import React from 'react';
import faker from 'faker';
import Device from '../../src/components/Device';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import { mockedNavigate } from '../../jest.setup';

const deviceName = faker.lorem.sentence();
const initialState = {
  entities: {
    devices: {
      0: {
        name: deviceName,
      },
    },
  },
};

const setup = props => render(<Device {...props} />, { initialState });

describe('<Device /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText } = setup({
      entity_id: '0',
    });
    expect(getByText(deviceName)).toBeDefined();
    expect(getByTestId('arrow-icon')).toBeDefined();
  });

  it('should navigate to device details', () => {
    const { getByTestId } = setup({
      entity_id: '0',
    });
    fireEvent.press(getByTestId('arrow-icon'));
    expect(mockedNavigate).toHaveBeenCalledWith('DeviceDetails', {
      deviceId: '0',
    });
  });
});

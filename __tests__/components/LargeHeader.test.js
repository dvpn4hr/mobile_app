import 'react-native';
import React from 'react';
import LargeHeader from '../../src/components/LargeHeader';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent } from '@testing-library/react-native';
import { mockedGoBack } from '../../jest.setup';

const setup = props => render(<LargeHeader {...props} />);
const navigate = jest.fn();
const Props = {
  navigation: {
    navigate,
  },
  route: {
    name: 'test',
  },
  options: {
    showAvatar: false,
  },
  back: false,
};
describe('<LargeHeader /> Test', () => {
  it('should assert main components exist', () => {
    const { getByText } = setup(Props);
    expect(getByText('test')).toBeTruthy();
  });

  it('should show & call goBack when back is true', () => {
    const { getByTestId, getByText } = setup({
      ...Props,
      back: true,
    });
    expect(getByText(i18n.t('back'))).toBeTruthy();
    fireEvent.press(getByTestId('back-icon'));
    expect(mockedGoBack).toHaveBeenCalled();
  });

  it('should show & call navigate when showAvatar is true', () => {
    const { getByTestId } = setup({
      ...Props,
      options: {
        showAvatar: true,
      },
    });
    fireEvent.press(getByTestId('avatar'));
    expect(navigate).toHaveBeenCalled();
  });
});

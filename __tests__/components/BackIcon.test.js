import 'react-native';
import React from 'react';
import BackIcon from '../../src/components/BackIcon';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent } from '@testing-library/react-native';
import { mockedGoBack } from '../../jest.setup';

const setup = props => render(<BackIcon {...props} />);

describe('<BackIcon /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId } = setup({});

    expect(getByTestId('container')).toBeDefined();
    expect(getByTestId('back-icon')).toBeDefined();
    expect(getByTestId('back-icon').props.source).toBe(
      require('../../src/assets/arrowBack.png')
    );
  });

  it('should show back title when showTitle is true', () => {
    const { getByText } = setup({
      showTitle: true,
    });

    expect(getByText(i18n.t('back'))).toBeDefined();
  });

  it('should go back when the back button pressed', () => {
    const { getByTestId } = setup({});

    fireEvent.press(getByTestId('back-icon'));
    expect(mockedGoBack).toHaveBeenCalled();
  });

  it('should not go back when the back text pressed', () => {
    const { getByText } = setup({
      showTitle: true,
    });

    fireEvent.press(getByText(i18n.t('back')));
    expect(mockedGoBack).toHaveBeenCalled();
  });
});

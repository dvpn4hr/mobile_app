import 'react-native';
import React from 'react';
import faker from 'faker';
import EmptyView from '../../src/components/EmptyView';
import { render } from '../../custom-render';
import { fireEvent } from '@testing-library/react-native';
import Button from '../../src/components/Button';

const EmptyViewProps = {
  title: faker.lorem.sentence(),
  message: faker.lorem.sentence(),
  source: faker.image.imageUrl(),
  actions: [
    {
      title: faker.lorem.sentence(),
      onPress: jest.fn(),
    },
    {
      title: faker.lorem.sentence(),
      onPress: jest.fn(),
      type: Button.types.SECONDARY,
    },
  ],
};

const setup = props => render(<EmptyView {...props} />);

describe('<EmptyView /> Test', () => {
  it('should assert main components exist', () => {
    const { getByTestId, getByText } = setup(EmptyViewProps);
    expect(getByTestId('empty-view')).toBeDefined();
    expect(getByText(EmptyViewProps.title)).toBeDefined();
    expect(getByText(EmptyViewProps.message)).toBeDefined();
    expect(getByTestId('empty-image')).toBeDefined();
    expect(getByTestId('button-primary')).toBeDefined();
    expect(getByTestId('button-secondary')).toBeDefined();
  });

  it('should handle onPress for actions', () => {
    const { getByText } = setup(EmptyViewProps);
    fireEvent.press(getByText(EmptyViewProps.actions[0].title));
    expect(EmptyViewProps.actions[0].onPress).toHaveBeenCalled();

    fireEvent.press(getByText(EmptyViewProps.actions[1].title));
    expect(EmptyViewProps.actions[1].onPress).toHaveBeenCalled();
  });
});

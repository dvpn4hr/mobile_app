import 'react-native';
import React from 'react';
import ForgotPassword from '../../src/screens/ForgotPassword';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent, waitFor } from '@testing-library/react-native';
import { resetUserPassword } from '../../src/actions';
import faker from 'faker';

jest.mock('../../src/actions', () => ({
  resetUserPassword: jest.fn(() => ({
    type: 'RESET_USER_PASSWORD',
    success: true,
  })),
}));

const navigate = jest.fn();
const replace = jest.fn();
const dispatch = jest.fn();
const navigation = { navigate, replace, dispatch };

const setup = props => render(<ForgotPassword {...props} />);

describe('<ForgotPassword /> Test', () => {
  it('should validate inputs locally before submitting', () => {
    const { getByText } = setup({});

    fireEvent.press(getByText(i18n.t('submit')));
    expect(
      getByText('The field "Email" must be a valid email address.')
    ).toBeTruthy();
  });

  it('should reset user password when the form is valid', () => {
    const { getByPlaceholderText, getByText } = setup({ navigation });
    const email = faker.internet.email();

    fireEvent.changeText(getByPlaceholderText(i18n.t('email')), email);
    fireEvent.press(getByText(i18n.t('submit')));

    expect(resetUserPassword).toHaveBeenCalledWith({
      email,
    });

    waitFor(
      async () =>
        await expect(navigate).toHaveBeenCalledWith('ResetPassword', { email })
    );
  });
});

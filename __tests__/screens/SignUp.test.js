import 'react-native';
import React from 'react';
import SignUp from '../../src/screens/SignUp';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent, waitFor } from '@testing-library/react-native';
import { signUserUp } from '../../src/actions';
import faker from 'faker';

jest.mock('../../src/actions', () => ({
  signUserUp: jest.fn(() => ({
    type: 'SIGN_USER_UP',
    success: true,
  })),
}));

const navigate = jest.fn();
const replace = jest.fn();
const dispatch = jest.fn();
const navigation = { navigate, replace, dispatch };

const setup = props => render(<SignUp {...props} />);

describe('<SignUp /> Test', () => {
  it('should validate inputs locally before submitting', () => {
    const { getByText } = setup({});

    fireEvent.press(getByText(i18n.t('create_account')));
    expect(
      getByText('The field "Email" must be a valid email address.')
    ).toBeTruthy();
    expect(getByText('The field "Password" is mandatory.')).toBeTruthy();
    expect(
      getByText('The field "Password Confirmation" is mandatory.')
    ).toBeTruthy();
  });

  it('should sign user up when the form is valid', () => {
    const { getByPlaceholderText, getByText } = setup({ navigation });
    const email = faker.internet.email();
    const password = faker.internet.password() + '1$';
    const passwordConfirmation = password;

    fireEvent.changeText(getByPlaceholderText(i18n.t('email')), email);
    fireEvent.changeText(getByPlaceholderText(i18n.t('password')), password);
    fireEvent.changeText(
      getByPlaceholderText(i18n.t('password_confirmation')),
      passwordConfirmation
    );
    fireEvent.press(getByText(i18n.t('create_account')));

    expect(signUserUp).toHaveBeenCalledWith({
      username: email,
      email: email?.toLowerCase(),
      password,
      grant_type: 'password',
    });

    waitFor(() => expect(navigate).toHaveBeenCalledWith('Setup'));
  });

  it('should navigate to login screen', () => {
    const { getByText } = setup({ navigation });

    fireEvent.press(getByText(i18n.t('already_have_an_account')));

    expect(replace).toHaveBeenCalledWith('Login');
  });
});

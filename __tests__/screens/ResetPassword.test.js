import 'react-native';
import React from 'react';
import ResetPassword from '../../src/screens/ResetPassword';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent, waitFor } from '@testing-library/react-native';
import { setUserPassword } from '../../src/actions';
import faker from 'faker';

jest.mock('../../src/actions', () => ({
  setUserPassword: jest.fn(() => ({
    type: 'SET_USER_PASSWORD',
    success: true,
  })),
}));

const navigate = jest.fn();
const replace = jest.fn();
const dispatch = jest.fn();
const navigation = { navigate, replace, dispatch };

const setup = props => render(<ResetPassword {...props} />);

describe('<ResetPassword /> Test', () => {
  it('should validate inputs locally before submitting', () => {
    const { getByText } = setup({});

    fireEvent.press(getByText(i18n.t('submit')));
    expect(
      getByText('The field "Email" must be a valid email address.')
    ).toBeTruthy();

    expect(
      getByText('The field "One Time Password" is mandatory.')
    ).toBeTruthy();

    expect(getByText('The field "New Password" is mandatory.')).toBeTruthy();
  });

  it('should reset user password when the form is valid', () => {
    const { getByPlaceholderText, getByText } = setup({ navigation });
    const email = faker.internet.email();
    const one_time_password = faker.internet.password();
    const new_password = faker.internet.password() + '1$';

    fireEvent.changeText(getByPlaceholderText(i18n.t('email')), email);
    fireEvent.changeText(
      getByPlaceholderText(i18n.t('one_time_password')),
      one_time_password
    );
    fireEvent.changeText(
      getByPlaceholderText(i18n.t('new_password')),
      new_password
    );
    fireEvent.press(getByText(i18n.t('submit')));

    expect(setUserPassword).toHaveBeenCalledWith({
      email,
      new_password,
      one_time_password,
    });

    waitFor(() => expect(dispatch).toHaveBeenCalled());
  });
});

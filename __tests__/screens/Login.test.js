import 'react-native';
import React from 'react';
import Login from '../../src/screens/Login';
import { render } from '../../custom-render';
import i18n from 'i18n-js';
import { fireEvent, waitFor } from '@testing-library/react-native';
import { signUserIn } from '../../src/actions';
import faker from 'faker';

jest.mock('../../src/actions', () => ({
  signUserIn: jest.fn(() => ({
    type: 'SIGN_USER_IN',
    success: true,
  })),
}));

const navigate = jest.fn();
const replace = jest.fn();
const dispatch = jest.fn();
const navigation = { navigate, replace, dispatch };

const setup = props => render(<Login {...props} />);

describe('<Login /> Test', () => {
  it('should validate inputs locally before submitting', () => {
    const { getByText } = setup({});

    fireEvent.press(getByText(i18n.t('login')));
    expect(
      getByText('The field "Email" must be a valid email address.')
    ).toBeTruthy();
    expect(getByText('The field "Password" is mandatory.')).toBeTruthy();
  });

  it('should log user in when the form is valid for once', () => {
    const { getByPlaceholderText, getByText } = setup({ navigation });
    const email = faker.internet.email();
    const password = faker.internet.password();

    fireEvent.changeText(getByPlaceholderText(i18n.t('email')), email);
    fireEvent.changeText(getByPlaceholderText(i18n.t('password')), password);
    fireEvent.press(getByText(i18n.t('login')));

    expect(signUserIn).toHaveBeenCalledWith({
      username: email,
      password: password,
      grant_type: 'password',
      remember: false,
    });

    waitFor(() => expect(navigate).toHaveBeenCalledWith('Setup'));
  });

  it('should log user in when the form is valid with redirect', () => {
    const { getByPlaceholderText, getByText } = setup({
      navigation,
      route: { params: { isDirect: true } },
    });
    const email = faker.internet.email();
    const password = faker.internet.password();

    fireEvent.changeText(getByPlaceholderText(i18n.t('email')), email);
    fireEvent.changeText(getByPlaceholderText(i18n.t('password')), password);
    fireEvent.press(getByText(i18n.t('remember_password')));
    fireEvent.press(getByText(i18n.t('login')));

    expect(signUserIn).toHaveBeenCalledWith({
      username: email,
      password: password,
      grant_type: 'password',
      remember: true,
    });

    waitFor(() => expect(dispatch).toHaveBeenCalled());
  });

  it('should navigate to sign up', () => {
    const { getByText } = setup({ navigation });

    fireEvent.press(getByText(i18n.t('don_t_have_an_account')));

    expect(replace).toHaveBeenCalledWith('SignUp');
  });

  it('should navigate to forgot password', () => {
    const { getByText } = setup({ navigation });

    fireEvent.press(getByText(i18n.t('forgot_password')));

    expect(navigate).toHaveBeenCalledWith('ForgotPassword');
  });
});

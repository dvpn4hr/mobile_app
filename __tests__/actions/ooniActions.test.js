import {
  toggleOONISetup,
  toggleOONIWorking,
} from '../../src/actions/ooniActions';
import actionTypes from '../../src/actionTypes';

describe('ooniActions Test', () => {
  it('should toggleOONISetup action correctly', () => {
    const state = true;
    const expectedAction = {
      type: actionTypes.TOGGLE_OONI_SETUP,
      state,
    };
    expect(toggleOONISetup(state)).toEqual(expectedAction);
  });

  it('should toggleOONIWorking action correctly', () => {
    const state = true;
    const expectedAction = {
      type: actionTypes.TOGGLE_OONI_WORKING,
      state,
    };
    expect(toggleOONIWorking(state)).toEqual(expectedAction);
  });
});

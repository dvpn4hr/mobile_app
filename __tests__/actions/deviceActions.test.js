import actionTypes from '../../src/actionTypes';
import {
  getDiagnosticInfo,
  setMainDevice,
} from '../../src/actions/deviceActions';

jest.mock('../../src/services/APIServices', () => {
  return jest.fn().mockImplementation(() => {
    return {
      checkService: jest.fn(() =>
        Promise.resolve({
          success: true,
        })
      ),
    };
  });
});

jest.mock('../../src/services/RPIServices', () => {
  return jest.fn().mockImplementation(() => {
    return {
      getErrorLog: jest.fn(() => Promise.resolve({})),
    };
  });
});

describe('deviceActions Test', () => {
  it('should setMainDevice action correctly', () => {
    const id = 1;
    const expectedAction = {
      type: actionTypes.SET_MAIN_DEVICE,
      id,
    };
    expect(setMainDevice(id)).toEqual(expectedAction);
  });

  it('should getDiagnostic action correctly', async () => {
    const device = {
      id: 1,
      local_ip_address: '192.168.1.1',
      local_token: 'token',
    };

    expect(await getDiagnosticInfo(device)).toEqual({
      isWEPNAccessible: true,
      isDeviceAccessible: true,
      deviceError: undefined,
      error_logs: undefined,
    });
  });
});

import actionTypes from '../../src/actionTypes';
import {
  addFriendHash,
  removeFriendHash,
} from '../../src/actions/friendActions';

describe('friendActions Test', () => {
  it('should addFriendHash action correctly', () => {
    const device = {
      id: 'id',
      name: 'name',
      hash: 'hash',
    };
    const expectedAction = {
      type: actionTypes.ADD_FRIEND_HASH,
      device,
    };
    expect(addFriendHash(device)).toEqual(expectedAction);
  });

  it('should removeFriendHash action correctly', () => {
    const friend_id = 'friend_id';
    const expectedAction = {
      type: actionTypes.REMOVE_FRIEND_HASH,
      friend_id,
    };
    expect(removeFriendHash(friend_id)).toEqual(expectedAction);
  });
});

import actionTypes from '../../src/actionTypes';
import {
  setFirstTime,
  setAccessData,
  resetAccessData,
} from '../../src/actions/commonActions';

describe('commonActions Test', () => {
  it('should setFirstTime action correctly', () => {
    const state = true;
    const expectedAction = {
      type: actionTypes.SET_FIRST_TIME,
      state,
    };
    expect(setFirstTime(state)).toEqual(expectedAction);
  });

  it('should setAccessData action correctly', () => {
    const data = {
      access_token: 'access_token',
      refresh_token: 'refresh_token',
    };
    const expectedAction = {
      type: actionTypes.SET_ACCESS_DATA,
      data,
    };
    expect(setAccessData(data)).toEqual(expectedAction);
  });

  it('should resetAccessData action correctly', () => {
    const expectedAction = {
      type: actionTypes.RESET_ACCESS_DATA,
    };
    expect(resetAccessData()).toEqual(expectedAction);
  });
});

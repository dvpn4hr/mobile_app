import actionTypes from '../../src/actionTypes';
import {
  setUserData,
  setDeviceKey,
  setUserToken,
  refreshToken,
  onStartUp,
  onSignIn,
} from '../../src/actions/userActions';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { identifyAnonymous, initialize } from 'react-native-zendesk/lib';

const initialState = {
  auth: {
    userToken: {
      refresh_token: '',
      access_token: '',
      expires_in: 0,
      remember: false,
    },
  },
};

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

const store = mockStore(initialState);

describe('userActions Test', () => {
  beforeEach(() => {
    store.clearActions();
  });

  it('should setUserData action correctly', () => {
    const data = {
      id: 1,
    };
    const expectedAction = {
      type: actionTypes.SET_USER_DATA,
      data,
    };
    expect(setUserData(data)).toEqual(expectedAction);
  });

  it('should setDeviceKey action correctly', () => {
    const key = 'key';
    const expectedAction = {
      type: actionTypes.SET_DEVICE_KEY,
      key,
    };
    expect(setDeviceKey(key)).toEqual(expectedAction);
  });

  it('should setUserToken action correctly', () => {
    const data = {
      access_token: 'access_token',
    };
    const expectedAction = {
      type: actionTypes.SET_USER_TOKEN,
      data,
    };
    expect(setUserToken(data)).toEqual(expectedAction);
  });

  it('should refresh token action correctly', async () => {
    await store.dispatch(refreshToken());
    const actions = store.getActions();
    expect(actions[0].type).toEqual(actionTypes.SIGN_USER_IN);
  });

  it('should dispatch on start up action correctly', async () => {
    await store.dispatch(onStartUp());
    expect(initialize).toHaveBeenCalled();
  });

  it('should dispatch on sign in action correctly', async () => {
    await store.dispatch(onSignIn());
    expect(identifyAnonymous).toHaveBeenCalled();
    expect(store.getActions()[0].type).toEqual(actionTypes.SET_USER_DATA);
  });
});

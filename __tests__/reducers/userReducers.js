import actionTypes from '../../src/actionTypes';
import {
  userToken,
  userData,
  deviceKey,
} from '../../src/reducers/userReducers';

describe('userReducers', () => {
  describe('userToken', () => {
    it('should return the initial state', () => {
      const initialState = {};
      expect(userToken(undefined, {})).toEqual(initialState);
    });

    it('should handle SET_USER_TOKEN', () => {
      const action = {
        type: actionTypes.SET_USER_TOKEN,
        data: {
          access_token: 'access_token',
        },
      };
      const expectedState = {
        access_token: 'access_token',
      };
      expect(userToken({}, action)).toEqual(expectedState);
    });

    it('should handle SIGN_USER_OUT', () => {
      const action = {
        type: actionTypes.SIGN_USER_OUT,
      };
      const expectedState = {};
      expect(userToken({ access_token: 'access_token' }, action)).toEqual(
        expectedState
      );
    });
  });

  describe('userData', () => {
    it('should return the initial state', () => {
      const initialState = {};
      expect(userData(undefined, {})).toEqual(initialState);
    });
    it('should handle SET_USER_DATA', () => {
      const action = {
        type: actionTypes.SET_USER_DATA,
        data: {
          id: 'id',
          name: 'name',
        },
      };
      const expectedState = {
        id: 'id',
        name: 'name',
      };
      expect(userData({}, action)).toEqual(expectedState);
    });
    it('should handle SIGN_USER_OUT', () => {
      const action = {
        type: actionTypes.SIGN_USER_OUT,
      };
      const expectedState = {};
      expect(userData({ id: 'id', name: 'name' }, action)).toEqual(
        expectedState
      );
    });
  });

  describe('deviceKey', () => {
    it('should return the initial state', () => {
      const initialState = null;
      expect(deviceKey(undefined, {})).toEqual(initialState);
    });

    it('should handle SET_DEVICE_KEY', () => {
      const action = {
        type: actionTypes.SET_DEVICE_KEY,
        key: 'device_key',
      };
      const expectedState = 'device_key';
      expect(deviceKey({}, action)).toEqual(expectedState);
    });

    it('should handle SIGN_USER_OUT', () => {
      const action = {
        type: actionTypes.SIGN_USER_OUT,
      };
      const expectedState = null;
      expect(deviceKey({ device_key: 'device_key' }, action)).toEqual(
        expectedState
      );
    });
  });
});

import actionTypes from '../../src/actionTypes';
import { mainDeviceId } from '../../src/reducers/deviceReducers';

describe('deviceReducers', () => {
  describe('mainDeviceId', () => {
    it('should return the initial state', () => {
      const initialState = null;
      expect(mainDeviceId(undefined, {})).toEqual(initialState);
    });

    it('should handle SET_MAIN_DEVICE', () => {
      const action = {
        type: actionTypes.SET_MAIN_DEVICE,
        id: 'id',
      };
      const expectedState = 'id';
      expect(mainDeviceId({}, action)).toEqual(expectedState);
    });

    it('should handle SIGN_USER_OUT', () => {
      const action = {
        type: actionTypes.SIGN_USER_OUT,
      };
      const expectedState = null;
      expect(mainDeviceId({ mainDeviceId: 'id' }, action)).toEqual(
        expectedState
      );
    });
  });
});

import actionTypes from '../../src/actionTypes';
import { entities, accessData } from '../../src/reducers/commonReducers';

describe('commonReducers', () => {
  describe('entities', () => {
    it('should return the initial state', () => {
      const initialState = {};
      expect(entities(undefined, {})).toEqual(initialState);
    });

    it('should handle add entity', () => {
      const action = {
        entities: {
          users: { 1: { id: 1, name: 'name' } },
        },
      };

      const expectedState = {
        users: { 1: { id: 1, name: 'name' } },
      };

      expect(entities({}, action)).toEqual(expectedState);
    });

    it('should handle remove entity on user sign out', () => {
      const action = { type: actionTypes.SIGN_USER_OUT };

      const expectedState = {};

      expect(
        entities({ users: { 1: { id: 1, name: 'name' } } }, action)
      ).toEqual(expectedState);
    });
  });

  describe('accessData', () => {
    it('should return the initial state', () => {
      const initialState = {
        language: 'en',
        config: {
          tunnel: 'shadowsocks',
        },
      };
      expect(accessData(undefined, {})).toEqual(initialState);
    });

    it('should handle SET_ACCESS_DATA', () => {
      const action = {
        type: actionTypes.SET_ACCESS_DATA,
        data: {
          language: 'ar',
        },
      };
      const expectedState = {
        language: 'ar',
      };

      expect(accessData({}, action)).toEqual(expectedState);
    });

    it('should handle SIGN_USER_OUT', () => {
      const action = {
        type: actionTypes.RESET_ACCESS_DATA,
      };
      const expectedState = {
        language: 'en',
        config: {
          tunnel: 'shadowsocks',
        },
      };
      expect(
        accessData(
          {
            language: 'ar',
            config: {
              tunnel: 'shadowsocks',
            },
          },
          action
        )
      ).toEqual(expectedState);
    });
  });
});

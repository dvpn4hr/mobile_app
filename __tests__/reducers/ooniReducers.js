import actionTypes from '../../src/actionTypes';
import { OONISetup, OONIWorking } from '../../src/reducers/ooniReducers';

describe('ooniReducers', () => {
  describe('OONISetup', () => {
    it('should return the initial state', () => {
      const initialState = false;
      expect(OONISetup(undefined, {})).toEqual(initialState);
    });

    it('should handle OONI_SETUP', () => {
      const action = {
        type: actionTypes.TOGGLE_OONI_SETUP,
        state: true,
      };
      const expectedState = true;

      expect(OONISetup({ OONISetup: false }, action)).toEqual(expectedState);
    });
  });
  describe('OONIWorking', () => {
    it('should return the initial state', () => {
      const initialState = false;
      expect(OONIWorking(undefined, {})).toEqual(initialState);
    });

    it('should handle OONI_WORKING', () => {
      const action = {
        type: actionTypes.TOGGLE_OONI_WORKING,
        state: true,
      };
      const expectedState = true;

      expect(OONIWorking({ OONIWorking: false }, action)).toEqual(
        expectedState
      );
    });
  });
});

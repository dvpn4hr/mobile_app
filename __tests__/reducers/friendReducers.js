import actionTypes from '../../src/actionTypes';
import { friendHash } from '../../src/reducers/friendReducers';

describe('friendReducers', () => {
  describe('friendHash', () => {
    it('should return the initial state', () => {
      const initialState = [];
      expect(friendHash(undefined, {})).toEqual(initialState);
    });

    it('should handle ADD_FRIEND_HASH', () => {
      const action = {
        type: actionTypes.ADD_FRIEND_HASH,
        device: {
          id: 'friend_id',
        },
      };

      const expectedState = [
        {
          id: 'friend_id',
        },
      ];

      expect(friendHash([], action)).toEqual(expectedState);
    });

    it('should handle REMOVE_FRIEND_HASH', () => {
      const action = {
        type: actionTypes.REMOVE_FRIEND_HASH,
        friend_id: 'friend_id',
      };

      const expectedState = [];

      expect(
        friendHash(
          [
            {
              id: 'friend_id',
            },
          ],
          action
        )
      ).toEqual(expectedState);
    });
  });
});

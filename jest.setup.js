import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';
import mockRNCNetInfo from '@react-native-community/netinfo/jest/netinfo-mock';

jest.mock('rn-fetch-blob', () => ({
  DocumentDir: jest.fn(),
  polyfill: jest.fn(),
  config: jest.fn(() => ({
    fetch: jest.fn(),
  })),
}));

jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock')
);

jest.mock('react-native-zendesk', () => {
  return {
    initialize: jest.fn(),
    showArticle: jest.fn(),
    identifyAnonymous: jest.fn(),
  };
});
jest.mock('react-native-ping', () => ({
  start: jest.fn(),
}));

export const mockedNavigate = jest.fn();
export const mockedGoBack = jest.fn();

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: mockedNavigate,
      goBack: mockedGoBack,
    }),
  };
});

jest.mock('@react-navigation/stack', () => {
  return {
    createStackNavigator: jest.fn().mockImplementation(nav => ({})),
    CardStyleInterpolators: {},
  };
});

jest.mock('react-native-gesture-handler', () => {
  return {
    ...jest.requireActual('react-native-gesture-handler'),
    Swipeable: jest.fn().mockImplementation(({ children }) => children),
  };
});

jest.mock('react-native-safe-area-context', () => {
  const SafeAreaView = ({ children }) => children;
  const SafeAreaProvider = ({ children }) => children;
  return { SafeAreaView, SafeAreaProvider };
});
jest.mock('react-native-iphone-x-helper', () => {});
jest.mock('react-native-keyboard-aware-scroll-view', () => {});
jest.mock('react-native-share', () => {});
jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);

jest.mock('@react-navigation/native');
jest.mock('@react-navigation/compat', () => {});
jest.mock('react-native-snackbar', () => {});
jest.mock('@yz1311/react-native-http-cache', () => ({
  clearHttpCache: jest.fn(),
  clearCache: jest.fn(),
}));
jest.mock('react-native-keyboard-aware-scroll-view', () => {
  const KeyboardAwareScrollView = ({ children }) => children;
  return { KeyboardAwareScrollView };
});
jest.mock('react-native-tcp-socket', () => {});
jest.mock('react-native-permissions', () =>
  require('react-native-permissions/mock')
);
jest.mock('@react-native-community/netinfo', () => mockRNCNetInfo);

jest.mock('@react-native-clipboard/clipboard', () => ({}));

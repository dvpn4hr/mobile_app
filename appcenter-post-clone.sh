#!/usr/bin/env bash
CUR_COCOAPODS_VER=`sed -n -e 's/^COCOAPODS: \([0-9.]*\)/\1/p' ios/Podfile.lock`
ENV_COCOAPODS_VER=`pod --version`

# check if not the same version, reinstall cocoapods version to current project's
if [ $CUR_COCOAPODS_VER != $ENV_COCOAPODS_VER ];
then
    echo "Uninstalling all CocoaPods versions"
    sudo gem uninstall cocoapods --all --executables
    echo "Installing CocoaPods version $CUR_COCOAPODS_VER"
    sudo gem install cocoapods -v $CUR_COCOAPODS_VER
else 
    echo "CocoaPods version is suitable for the project"
fi;

if [[ ! -z "${CLIENT_ID}" ]]; then
	cat << EOF  > .env
CLIENT_ID=${CLIENT_ID}
CLIENT_SECRET=${CLIENT_SECRET}
URL=${URL}
ZENDESK_APP_ID=${ZENDESK_APP_ID}
ZENDESK_APP_URL=${ZENDESK_APP_URL}
ZENDESK_CLIENT_ID=${ZENDESK_CLIENT_ID}
DEV_URL=${DEV_URL}
EOF
	echo ".env file generated"
fi

BUILD_GRADLE=$APPCENTER_SOURCE_DIRECTORY/android/app/build.gradle
INFO_PLIST=$APPCENTER_SOURCE_DIRECTORY/ios/WEPN/Info.plist

if [ ! -z "$VERSION_NAME" ]
then


	echo "starting to set"
	if [ -e $BUILD_GRADLE ]
	then
	    echo "Updating: versionName = $VERSION_NAME versionCode= $VERSION_CODE"
	    sed -i '' 's/versionName "[0-9.]*"/versionName "'$VERSION_NAME'"/' $BUILD_GRADLE
	    if [ ! -z "$VERSION_CODE" ]
	    then
		    sed -i '' 's/versionCode [0-9.]*/versionCode '$VERSION_CODE'/' $BUILD_GRADLE
	    fi
	else
		echo "File not found: $BUILD_GRADLE"
	fi

	if [ -e "$INFO_PLIST" ]
	then
	    echo "Updating: CFBundleShortVersionString= $VERSION_NAME"
	    plutil -replace CFBundleShortVersionString -string $VERSION_NAME $INFO_PLIST
	    if [ ! -z "$VERSION_CODE" ]
	    then
		    plutil -replace CFBundleVersion -string $VERSION_CODE $INFO_PLIST
	    fi
	else
		echo "File not found: $INFO_PLIST"
	fi
else
	echo "VERSION_NAME not set"
fi

echo "versions should be corrected below:"
cat $INFO_PLIST | grep version
cat $BUILD_GRADLE | grep version

# run cache clean [fix appcenter caching issue]
echo "cleaning old builds cache"
yarn cache clean
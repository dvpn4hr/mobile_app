module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: true,
  bracketSpacing: true,
  singleQuote: true,
  useTabs: false,
  printWidth: 80,
  tabWidth: 2,
  trailingComma: 'es5',
  noSemi: true,
  rcVerbose: true,
  parser: 'babel',
};

# WE-PN

## SETUP

- Create .env file with contents in .env.example file
- Create .env.staging / .env.production if you need to use multiple environments.
- Install dependencies:

```sh
yarn install

cd ios && pod install
```

## RUNNING
if you want to run different environment like **Staging** or **Production** use:

```sh
ENVFILE=.env.staging react-native run-ios 
```
or
```sh
ENVFILE=.env.staging react-native run-ios 
```


## Development Branch Builds:

* Android build status: ![Android BuildStatus](https://build.appcenter.ms/v0.1/apps/38b18869-f847-4065-8c51-c8047f3c7c6f/branches/develop/badge)
* iOS build status:     ![iOS BuildStatus](https://build.appcenter.ms/v0.1/apps/a866b395-8769-4de5-8546-bf7ee6023275/branches/develop/badge)

import Config from 'react-native-config';

const Environment = {
  CLIENT_ID: Config.CLIENT_ID,
  CLIENT_SECRET: Config.CLIENT_SECRET,
  URL: Config.URL,
  DEV_URL: Config.DEV_URL,
  ZENDESK_APP_ID: Config.ZENDESK_APP_ID,
  ZENDESK_APP_URL: Config.ZENDESK_APP_URL,
  ZENDESK_CLIENT_ID: Config.ZENDESK_CLIENT_ID,
};

console.log({ Environment });
module.exports = Environment;

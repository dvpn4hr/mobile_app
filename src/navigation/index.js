import React, { useEffect, useState } from 'react';
import { StatusBar, View, UIManager, Linking } from 'react-native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { LoadingView } from '../components';
import { colors } from '../common';
import { persistStore } from 'redux-persist';
import { store } from '../reducers';
import { Provider } from 'react-redux';
import { onStartUp, getDevices, refreshToken } from '../actions';
import { getShouldRefreshToken, getUserToken } from '../selectors';
import { APIServices } from '../services';
import { navigate, navigationRef } from '../RootNavigation';
import { parse } from 'search-params';
import AuthStackNavigator from './auth';
import LandingStackNavigator from './landing';
import DevicesStackNavigator from './devices';
import CompatibilityStack from './compatibility';
import SetupStack from './setup';
import ClaimStack from './claim';
import OONIStack from './ooni';
import GrantAccessStack from './grantAccess';
import TicketStack from './ticket';
import TutorialStack from './tutorial';
import SwitchStack from './switch';

APIServices.setStore(store);

const config = {
  screens: {
    SwitchEnv: 'switch',
  },
};

const linking = {
  prefixes: ['wepn://', 'https://*.we-pn.com'],
  config,
};

const Stack = createStackNavigator();

const getInitialRoute = store => {
  const state = store.getState();
  const authenticated = getUserToken(state)?.access_token;

  return authenticated ? 'App' : 'Landing';
};

const handleOpenURL = ({ url }) => {
  if (!url) return;

  const authenticated = getUserToken(store.getState())?.access_token;
  if (!authenticated) {
    navigate('Auth', {
      screen: 'Login',
    });
    return;
  }

  const data = parse(url);

  const { k, s, pk } = data;
  if (k && s && pk) {
    navigate('Setup', {
      screen: 'QRScanner',
      params: { k, s, pk },
    });
  }
};

const MainApp = () => {
  let URLListener;
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    persistStore(store, {}, async () => {
      if (getShouldRefreshToken(store.getState())) {
        await store.dispatch(refreshToken());
      }
      const authenticated = getUserToken(store.getState())?.access_token;
      Promise.all([
        store.dispatch(onStartUp()),
        authenticated && store.dispatch(getDevices()),
      ]).finally(onReady);
    });
    UIManager?.setLayoutAnimationEnabledExperimental?.(true);
    return () => {
      URLListener?.remove();
    };
  }, []);

  const onReady = async () => {
    setIsLoading(false);
    Linking.getInitialURL().then(url => handleOpenURL({ url }));
    URLListener = Linking.addEventListener('url', handleOpenURL);
  };

  return (
    <LoadingView
      isLoading={isLoading}
      unmount
      indicatorColor={colors.main_color}
      backgroundColor={colors.sub_bg}>
      <StatusBar
        barStyle={'light-content'}
        animated
        translucent
        showHideTransition={'slide'}
        backgroundColor={colors.sub_bg}
      />
      <Provider store={store}>
        <NavigationContainer
          ref={navigationRef}
          linking={linking}
          fallback={
            <LoadingView
              isLoading
              unmount
              indicatorColor={colors.main_color}
              backgroundColor={colors.sub_bg}
            />
          }>
          <Stack.Navigator
            initialRouteName={getInitialRoute(store)}
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen name={'Landing'} component={LandingStackNavigator} />
            <Stack.Screen
              name={'Auth'}
              component={AuthStackNavigator}
              options={{
                cardStyleInterpolator:
                  CardStyleInterpolators.forModalPresentationIOS,
              }}
            />
            <Stack.Screen name={'App'} component={DevicesStackNavigator} />
            <Stack.Screen
              name={'Compatibility'}
              component={CompatibilityStack}
              options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <Stack.Screen
              name={'Setup'}
              component={SetupStack}
              options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <Stack.Screen
              name={'ClaimStack'}
              component={ClaimStack}
              options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                headerLeft: () => <View />,
              }}
            />
            <Stack.Screen
              name={'OONIStack'}
              component={OONIStack}
              options={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                headerLeft: () => <View />,
              }}
            />
            <Stack.Screen
              name={'GrantAccess'}
              component={GrantAccessStack}
              options={{
                cardStyleInterpolator:
                  CardStyleInterpolators.forModalPresentationIOS,
              }}
            />
            <Stack.Screen
              name={'Ticket'}
              component={TicketStack}
              options={{
                cardStyleInterpolator:
                  CardStyleInterpolators.forModalPresentationIOS,
              }}
            />
            <Stack.Screen
              name={'Tutorial'}
              component={TutorialStack}
              options={{
                cardStyleInterpolator:
                  CardStyleInterpolators.forModalPresentationIOS,
              }}
            />
            <Stack.Screen
              name={'SwitchEnv'}
              component={SwitchStack}
              options={{
                cardStyleInterpolator:
                  CardStyleInterpolators.forModalPresentationIOS,
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    </LoadingView>
  );
};

export default MainApp;

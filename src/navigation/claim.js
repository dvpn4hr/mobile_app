import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { RunSetup } from "../screens";
import { stackOptions } from "./common";

const Stack = createStackNavigator();

const ClaimStack = () => (
    <Stack.Navigator
        name={'ClaimStack'}
        screenOptions={{ ...stackOptions, headerMode: 'screen' }}>
        <Stack.Screen name={'RunSetup'} component={RunSetup} />
    </Stack.Navigator>
);

export default ClaimStack;
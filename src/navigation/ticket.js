import React from "react";
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import { Ticket } from "../screens";
import { NavBar } from "../components";
import i18n from "../i18n";

const Stack = createStackNavigator();

const TicketStack = () => (
    <Stack.Navigator>
        <Stack.Screen
            name={'Ticket'}
            component={Ticket}
            options={{
                cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
                header: props => <NavBar {...props} title={i18n.t('support')} />,
            }}
        />
    </Stack.Navigator>
);

export default TicketStack;

import React from 'react';
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import { LargeHeader } from "../components";
import i18n from "../i18n";
import { AccessKey, DeviceDetails, Devices, Diagnostics, Friend, ManageAccess, QRScanner, Settings, Device } from "../screens";
import { stackOptions } from "./common";

const Stack = createStackNavigator();

const DevicesStackNavigator = () => (
    <Stack.Navigator
        name={'Devices'}
        screenOptions={{
            ...stackOptions,
            header: props => <LargeHeader {...props} />,
        }}>
        <Stack.Screen
            name={'Devices'}
            component={Devices}
            options={{
                headerTitle: i18n.t('wepn_pod'),
                initialParams: {
                    showAvatar: true,
                },
            }}
        />

        <Stack.Screen
            name={'Device'}
            component={Device}
            options={{
                headerTitle: i18n.t('VPN'),
            }}
        />

        <Stack.Screen name={'QRScanner'} component={QRScanner} />
        <Stack.Screen name={'ManageAccess'} component={ManageAccess} />
        <Stack.Screen name={'Friend'} component={Friend} />
        <Stack.Screen
            name={'AccessKey'}
            component={AccessKey}
            options={{
                cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
            }}
        />
        <Stack.Screen
            name={'Settings'}
            component={Settings}
            options={{ headerTitle: i18n.t('settings') }}
        />
        <Stack.Screen name={'DeviceDetails'} component={DeviceDetails} />
        <Stack.Screen name={'Diagnostics'} component={Diagnostics} />
    </Stack.Navigator>
);


export default DevicesStackNavigator;
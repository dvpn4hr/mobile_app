import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import { View, StyleSheet } from 'react-native';
import { BackAction, BackIcon } from '../components';
import { CompatibilityTest, RunTest } from '../screens';
import { stackOptions } from './common';
import { moderateVerticalScale } from 'react-native-size-matters/extend';

const Stack = createStackNavigator();

const CompatibilityStack = () => (
  <Stack.Navigator
    name={'Compatibility'}
    screenOptions={{
      ...stackOptions,
      headerTransparent: true,
      headerMode: 'float',
      header: props => (
        <View style={styles.customHeader}>
          <BackIcon {...props} type="close" />
        </View>
      ),
    }}>
    <Stack.Screen name={'CompatibilityTest'} component={CompatibilityTest} />
    <Stack.Screen name={'RunTest'} component={RunTest} />
  </Stack.Navigator>
);

export default CompatibilityStack;

const styles = StyleSheet.create({
  customHeader: {
    height: moderateVerticalScale(50),
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});

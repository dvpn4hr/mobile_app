import React from "react";
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import { NavBar } from '../components';
import { Tutorial } from '../screens';
import i18n from "../i18n";

const Stack = createStackNavigator();

const TutorialStack = () => (
    <Stack.Navigator>
        <Stack.Screen
            name={'Tutorial'}
            component={Tutorial}
            options={{
                cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
                header: props => <NavBar {...props} title={i18n.t('tutorials')} />,
            }}
        />
    </Stack.Navigator>
);

export default TutorialStack;
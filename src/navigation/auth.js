import React from 'react';
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import { View } from "react-native";
import { BackAction } from "../components";
import {
    Login,
    ForgotPassword,
    SignUp,
    ResetPassword,
} from '../screens';
import i18n from '../i18n';

const Stack = createStackNavigator();

const AuthStackNavigator = () => (
    <Stack.Navigator
        name={'Login'}
        screenOptions={{
            headerTransparent: true,
            headerTitle: '',
            headerLeft: () => <View />,
            headerRight: () => <BackAction title={i18n.t('cancel')} />,
            headerMode: 'float',
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
            name={'SignUp'}
            component={SignUp}
            options={{
                headerRight: () => <BackAction title={i18n.t('cancel')} />,
            }}
        />
        <Stack.Screen name={'Login'} component={Login} />
        <Stack.Screen name={'ForgotPassword'} component={ForgotPassword} />
        <Stack.Screen name={'ResetPassword'} component={ResetPassword} />
    </Stack.Navigator>
);

export default AuthStackNavigator;
import { CardStyleInterpolators } from "@react-navigation/stack";
import { colors, typo } from "../common";
import { BackIcon } from "../components";
import i18n from "../i18n";
import React from 'react';

export const headerStyles = {
    headerStyle: {
        backgroundColor: colors.sub_bg,
        shadowRadius: 0,
        shadowOffset: {
            height: 0,
        },
        elevation: 0,
    },
    headerTitleStyle: {
        color: colors.white,
    },
    headerBackTitleStyle: {
        ...typo.secondary,
        fontSize: 14,
        color: colors.white,
    },
};

export const stackOptions = {
    ...headerStyles,
    headerBackImage: props => <BackIcon {...props} />,
    headerBackTitleVisible: true,
    headerBackTitle: i18n.t('back'),
    headerTitle: '',
    headerMode: 'float',
    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};


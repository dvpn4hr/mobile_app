import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { Image, StyleSheet } from 'react-native';
import { GrantAccess, ContactInfo, AccessKey, ChooseLanguage, ChangeTunnel, EmailFriend, ImageSlider } from '../screens';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';

const Stack = createStackNavigator();

const GrantAccessStack = () => (
    <Stack.Navigator
        screenOptions={{
            headerBackTitle: i18n.t('back'),
            headerStyle: styles.header,
            headerBackTitleStyle: styles.backStyle,
            headerTitle: '',
            headerBackImage: () => (
                <Image
                    style={styles.backImage}
                    source={require('../assets/arrowBack.png')}
                />
            ),
            cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen name={'GrantAccess'} component={GrantAccess} />
        <Stack.Screen name={'ContactInfo'} component={ContactInfo} />
        <Stack.Screen name={'AccessKey'} component={AccessKey} />
        <Stack.Screen name={'ChooseLanguage'} component={ChooseLanguage} />
        <Stack.Screen name={'ChangeTunnel'} component={ChangeTunnel} />
        <Stack.Screen name={'EmailFriend'} component={EmailFriend} />
        <Stack.Screen name={'ImageSlider'} component={ImageSlider} />
    </Stack.Navigator>
);


export default GrantAccessStack;


const styles = StyleSheet.create({
    backImage: {
        width: 8,
        height: 14,
        resizeMode: 'contain',
        marginHorizontal: constants.PADDING,
    },
    backStyle: {
        ...typo.secondary,
        fontSize: 14,
        color: colors.white,
    },
    header: {
        backgroundColor: colors.charade,
        borderBottomColor: colors.chradeLight,
    },
});

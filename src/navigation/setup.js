import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { BackIcon, NavBar } from "../components";
import { ClaimDevice, QRScanner, Setup } from "../screens";
import { stackOptions } from "./common";
import { StyleSheet, View } from 'react-native';
import { moderateVerticalScale } from 'react-native-size-matters/extend';
import i18n from '../i18n';

const Stack = createStackNavigator();

const SetupStack = () => (
    <Stack.Navigator
        name={'Setup'}
        screenOptions={{
            ...stackOptions,
            headerTransparent: true,
            headerMode: 'float',
            header: props => (
                <View style={styles.customHeader}>
                    <BackIcon {...props} showTitle />
                </View>
            ),
        }}>
        <Stack.Screen name={'Setup'} component={Setup} />
        <Stack.Screen name={'ClaimDevice'} component={ClaimDevice} />
        <Stack.Screen
            name={'QRScanner'}
            component={QRScanner}
            options={{
                header: props => <NavBar {...props} title={i18n.t('scan_qr')} />,
            }}
        />
    </Stack.Navigator>
);

export default SetupStack;

const styles = StyleSheet.create({
    customHeader: {
        height: moderateVerticalScale(50),
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
});

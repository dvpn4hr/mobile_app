import React from "react";
import { CardStyleInterpolators, createStackNavigator } from "@react-navigation/stack";
import i18n from "../i18n";
import { NavBar } from "../components";
import { SwitchEnv } from "../screens";

const Stack = createStackNavigator();

const SwitchStack = () => (
    <Stack.Navigator>
        <Stack.Screen
            name={'SwitchEnv'}
            component={SwitchEnv}
            options={{
                cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
                header: props => <NavBar {...props} title={i18n.t('switch_env')} />,
            }}
        />
    </Stack.Navigator>
);

export default SwitchStack;
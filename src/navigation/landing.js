import React from 'react';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
import { View } from 'react-native';
import { BackAction } from '../components';
import { Intro, Landing, ProviderTips } from '../screens';
import { stackOptions } from './common';
import { useSelector } from 'react-redux';
import { getIsFirstTime } from '../selectors';

const Stack = createStackNavigator();

const LandingStackNavigator = () => {
  const freshStart = useSelector(getIsFirstTime);
  return (
    <Stack.Navigator
      name={'LandingStack'}
      initialRouteName={freshStart ? 'Landing' : 'Intro'}
      screenOptions={{
        ...stackOptions,
        headerTransparent: true,
      }}>
      <Stack.Screen name={'Landing'} component={Landing} />
      <Stack.Screen
        name={'Intro'}
        component={Intro}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name={'ProviderTips'}
        component={ProviderTips}
        options={{
          cardStyleInterpolator: CardStyleInterpolators.forModalPresentationIOS,
          headerRight: () => <BackAction />,
          headerTitle: '',
          headerLeft: () => <View />,
        }}
      />
    </Stack.Navigator>
  );
};

export default LandingStackNavigator;

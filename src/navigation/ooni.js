import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { LargeHeader } from '../components';
import { stackOptions } from './common';
import { IntroOONI, OONI } from '../screens';
import i18n from '../i18n';

const Stack = createStackNavigator();

const OONIStack = () => (
    <Stack.Navigator
        name={'OONIStack'}
        screenOptions={{
            ...stackOptions,
            header: props => <LargeHeader {...props} />,
        }}>
        <Stack.Screen
            name={'IntroOONI'}
            component={IntroOONI}
            options={{
                headerTitle: i18n.t('contribute_to_ooni'),
            }}
        />
        <Stack.Screen
            name={'OONI'}
            component={OONI}
            options={{
                headerTitle: i18n.t('ooni'),
            }}
        />
    </Stack.Navigator>
);

export default OONIStack;
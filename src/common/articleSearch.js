import _ from 'lodash';

const articles = [
  {
    title: ['No UPnP Device Found', 'UPnP is not available'],
    id: 11653723607309,
  },
  {
    title: ["Can't forward port while UPnP is disabled."],
    id: 360004782992,
  },
  {
    title: ['port is already mapped', 'Could not Open Port'],
    id: 17937365370765,
  },
  {
    title: ['UPnP port forwarding failed'],
    id: 360004782992,
  },
  {
    title: ['A server with the specified hostname could not be found'],
    id: 17937249357069,
  },
  {
    title: ['Network error'],
    id: 17937266099597,
  },
  {
    title: [
      'Port forwarding test failed',
      'ISP assigned IP is behind NAT (CG-NAT)',
    ],
    id: 11653768708877,
  },
  {
    title: [
      'port is already mapped (on Android)',
      'Could not Open Port (on iOS)',
    ],
    id: 17937365370765,
  },
  {
    title: ['Local Network Permission Isn’t allowed'],
    id: 17936934595213,
  },
];

const calculateLevenshteinDistance = (str1, str2) => {
  const dp = Array.from(Array(str1.length + 1), () =>
    Array(str2.length + 1).fill(0)
  );

  for (let i = 0; i <= str1.length; i++) {
    for (let j = 0; j <= str2.length; j++) {
      if (i === 0) dp[i][j] = j;
      else if (j === 0) dp[i][j] = i;
      else if (str1[i - 1] === str2[j - 1]) dp[i][j] = dp[i - 1][j - 1];
      else
        dp[i][j] = 1 + Math.min(dp[i - 1][j], dp[i][j - 1], dp[i - 1][j - 1]);
    }
  }

  return dp[str1.length][str2.length];
};

export const getArticle = query => {
  const allTitles = _.flatMap(articles, 'title');
  const bestMatchTitle = _.minBy(allTitles, title =>
    calculateLevenshteinDistance(query, title)
  );
  const bestMatchArticle = articles.find(article =>
    article.title.includes(bestMatchTitle)
  );

  return bestMatchArticle;
};

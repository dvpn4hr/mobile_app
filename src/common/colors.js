const APP_COLORS = {
  blue_chill: 'rgba(19, 156, 154, 1)',
  blue_light: 'rgba(129, 222, 221, 1)',
  white: 'rgba(255, 255, 255, 1)',
  black: 'rgba(0, 0, 0, 1)',
  alizarin_crimson: 'rgba(237, 47, 47, 1)',
  green: 'rgba(0, 128, 0, 1)',
  yellow: 'rgba(255, 255, 0, 1)',
  doveGray: 'rgba(102, 102, 102, 1)',
  pearl_Bush: 'rgba(228, 218, 206, 1)',
  blue: 'rgba(52, 152, 219, 1)',
  charade: 'rgba(36, 39, 48, 1)',
  mirage: 'rgba(24, 25, 36, 1)',
  silver: 'rgba(196, 196, 196, 1)',
  tuna: 'rgba(54, 54, 68, 1)',
  gray: 'rgba(140, 140, 140, 1)',
  ecstasy: 'rgba(253, 126, 20, 1)',
  pampas: 'rgba(250, 249, 248, 1)',
  sliverChalice: 'rgba(159, 159, 159, 1)',
  abbey: 'rgba(70, 72, 77, 1)',
  punch: 'rgba(220, 53, 69, 1)',
  nobel: 'rgba(182, 182, 182, 1)',
  chradeLight: 'rgba(46, 46, 59, 1)',
  gun_powder: 'rgba(69, 69, 83, 1)',
  manatee: 'rgba(148, 148, 150, 1)',
  transparent: 'rgba(0, 0, 0, 0)',
  mercurySolid: 'rgba(229, 229, 229, 1)',
  casablanca: 'rgba(245, 185, 67, 1)',
};

const THEME_COLORS = {
  ...APP_COLORS,
  main_color: APP_COLORS.blue_chill,
  background_color: APP_COLORS.white,
  text_color: APP_COLORS.charade,
  sub_text: APP_COLORS.silver,
  sub_bg: APP_COLORS.mirage,
  error: APP_COLORS.alizarin_crimson,
  success: APP_COLORS.green,
  warn: APP_COLORS.yellow,
  disabled: APP_COLORS.tuna,
};

const colorWithAlpha = (name = 'white', opacity = 1) => {
  if (!THEME_COLORS[name]) {
    name = 'denim';
  }
  return THEME_COLORS[name].split(', 1)').join(`, ${opacity})`);
};

const colors = {
  ...THEME_COLORS,
  colorWithAlpha,
};

export default colors;

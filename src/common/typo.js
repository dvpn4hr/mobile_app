const typo = {
  primary: {
    fontFamily: 'Rubik-Bold',
  },
  secondary: {
    fontFamily: 'Inter-SemiBold',
  },
  ancillary: {
    fontFamily: 'Inter-Regular',
  },
};

export default typo;

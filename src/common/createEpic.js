import { normalize } from 'normalizr';
import union from 'lodash/union';
import * as CacheManager from '@yz1311/react-native-http-cache';

const initialState = {
  loading: false,
};

const createEpic = ({ type, epic, schema = null }) => {
  const [SUCCESS, FAILED, RESET] = [
    `${type}_SUCCESS`,
    `${type}_FAILED`,
    `${type}_RESET`,
  ];
  return {
    actionCreator: payload => {
      return (dispatch, getState) => {
        if (payload?.reset) {
          return dispatch({ type: RESET });
        }
        dispatch({ type, payload });
        return epic(payload, dispatch, getState)
          .then(result => {
            const { entities = {}, result: ids = [] } = schema
              ? normalize(result, schema)
              : {};
            return dispatch({
              type: SUCCESS,
              result,
              ids,
              entities,
              success: true,
            });
          })
          .catch(error => {
            return dispatch({
              type: FAILED,
              result: null,
              error,
              success: false,
            });
          })
          .finally(() => {
            CacheManager.clearCache();
          });
      };
    },
    reducer: (state = initialState, action) => {
      switch (action.type) {
        case type:
          return {
            ...state,
            loading: true,
            ids: [],
          };
        case SUCCESS:
          let ids = state?.ids || [];
          return {
            ...state,
            success: true,
            loading: false,
            error: null,
            result: action.result,
            ids: union(ids, action.ids),
          };
        case FAILED:
          return {
            ...state,
            success: false,
            error: action.error,
            loading: false,
          };
        case RESET:
          return {
            loading: false,
          };
        default:
          return state;
      }
    },
  };
};

export default createEpic;

import { XMLParser } from 'fast-xml-parser';
import isEmpty from 'lodash/isEmpty';

const parser = new XMLParser();

export const extractPort = url => {
  const regex = /(?:http:\/\/|https:\/\/)([0-9.]+):([0-9]+)/;
  const port = url ? regex.exec(url)[2] : '';

  return port;
};

export const extractData = xml => {
  const parsed = parser.parse(xml);
  const device = parsed?.root?.device;
  const action = extractWANAction(device);

  return {
    path: action?.controlURL,
    action: action?.serviceType,
    device,
  };
};

export const extractWanIP = xml => {
  const regex = /(?:GetExternalIPAddress|NewExternalIPAddress)>([0-9.]+)</;
  let wan_ip = xml ? regex.exec(xml)[1] : '';

  return wan_ip;
};

export const extractMapping = xml => {
  console.log({ xml });
  const port = /(?:NewInternalPort)>([0-9.]+)</;
  const name = /(?:NewPortMappingDescription)>(.*?)<\//;
  const protocol = /(?:NewProtocol)>(.*?)<\//;
  return {
    port: xml ? port.exec(xml)[1] : '',
    name: xml ? name.exec(xml)[1] : '',
    protocol: xml ? protocol.exec(xml)[1] : '',
  };
};

export const checkName = name => {
  const names = ['wepn', 'shadowsocks'];

  return names.some(n => name.toLowerCase().includes(n)) ? name : 'Unknown';
};

const extractWANAction = devices => {
  if (isEmpty(devices)) return '';

  const services = [].concat(
    ...[].concat(devices).map(d => d?.serviceList?.service)
  );

  const action = services.find(service =>
    service?.serviceType?.includes('WANIPConnection')
  );

  if (action) return action;

  return extractWANAction(
    [].concat(...[].concat(devices).map(d => d?.deviceList?.device))
  );
};

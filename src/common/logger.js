export default class Logger {
  constructor() {
    this.logs = [];
  }

  log(logEntry) {
    this.logs.push(logEntry);
  }

  getLogs() {
    return this.logs;
  }

  deleteLogs() {
    this.logs = [];
  }
}

import RNSecureStorage, { ACCESSIBLE } from 'react-native-secure-key-store';

const config = {
  accessible: ACCESSIBLE.WHEN_PASSCODE_SET_THIS_DEVICE_ONLY,
};

export default {
  async getAllKeys() {
    return await RNSecureStorage.getAllKeys();
  },
  async getItem(key) {
    return await RNSecureStorage.get(key);
  },
  async setItem(key, value) {
    return await RNSecureStorage.set(key, value, config);
  },
  async removeItem(key) {
    return await RNSecureStorage.remove(key);
  },
  async clear() {
    return await RNSecureStorage.clear();
  },
  async exists(key) {
    return await RNSecureStorage.exists(key);
  },
};

import i18n from 'i18n-js';
import { colors } from '.';
import { PERFECT_DIAG_CODE } from '../Constants';

export const statusMap = {
  0: _ => ({
    title: i18n.t('off'),
    description: i18n.t('device_is_off'),
    message: i18n.t('unavailable_when_off'),
    color: colors.gray,
    error: false,
    canGrant: false,
  }),
  1: _ => ({
    title: i18n.t('disabled'),
    description: i18n.t('service_is_off'),
    message: i18n.t('turn_on_service'),
    color: colors.gray,
    error: false,
    canGrant: false,
  }),
  3: _ => ({
    title: i18n.t('starting'),
    description: i18n.t('sync_can_take_up_to'),
    message: i18n.t('sync_can_take_up_to'),
    color: colors.white,
    error: false,
    canGrant: false,
  }),
  2: diagCode =>
    (diagCode | 32) === PERFECT_DIAG_CODE
      ? {
          title: i18n.t('working'),
          description: i18n.t('ready_for_people'),
          message: i18n.t('turn_off_device'),
          color: colors.green,
          error: false,
          canGrant: true,
        }
      : {
          title: i18n.t('needs_attention'),
          description: i18n.t('review_pod_health'),
          message: i18n.t('unAvailable_due_to_errors'),
          color: colors.error,
          error: true,
          canGrant: false,
        },
  4: _ => ({
    title: i18n.t('warming'),
    description: i18n.t('device_is_starting_up'),
    message: i18n.t('unavailable_when_warming'),
    color: colors.green,
    error: false,
    canGrant: false,
  }),
};

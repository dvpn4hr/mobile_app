const links = {
  terms_of_service: 'https://we-pn.com/terms_of_service.html',
  privacy_policy: 'https://we-pn.com/privacy_policy.html',
  support: 'https://support.we-pn.com',
  report_issue: 'https://wepn.zendesk.com/hc/en-us/requests/new',
  twitter: 'https://twitter.com/getwepn',
  web_site: 'https://we-pn.com',
};

export default links;

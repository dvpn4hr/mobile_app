import { schema } from 'normalizr';

const DeviceSchema = new schema.Entity('devices');
const DiagnosticsSchema = new schema.Entity('diagnostics');
const FriendSchema = new schema.Entity('friends');

module.exports = {
  device: DeviceSchema,
  devices: [DeviceSchema],
  diagnostic: DiagnosticsSchema,
  diagnostics: [DiagnosticsSchema],
  friend: FriendSchema,
  friends: [FriendSchema],
};

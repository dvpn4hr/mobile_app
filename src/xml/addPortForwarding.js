export const addPortForwardingXML = (params) => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:AddPortMapping
            xmlns:u="${params.action}">
            <NewRemoteHost></NewRemoteHost>
            <NewExternalPort>${params.forwardingPort}</NewExternalPort>
            <NewProtocol>TCP</NewProtocol>
            <NewInternalPort>${params.forwardingPort}</NewInternalPort>
            <NewInternalClient>${params.host}</NewInternalClient>
            <NewEnabled>1</NewEnabled>
            <NewPortMappingDescription>WEPN</NewPortMappingDescription>
            <NewLeaseDuration>0</NewLeaseDuration>
        </u:AddPortMapping>
    </s:Body>
</s:Envelope>`;
    return xml;
};
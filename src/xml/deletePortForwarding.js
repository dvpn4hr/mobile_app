export const deletePortForwardingXML = (params) => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:DeletePortMapping
            xmlns:u="${params.action}">
            <NewRemoteHost>${params.host}</NewRemoteHost>
            <NewExternalPort>${params.forwardingPort}</NewExternalPort>
            <NewProtocol>TCP</NewProtocol>
        </u:DeletePortMapping>
    </s:Body>
</s:Envelope>`;
    return xml;
};
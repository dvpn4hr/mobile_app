export const getGenericPortMappingXML = (params) => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>
<s:Envelope
	xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
    s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
	<s:Body>
		<u:GetGenericPortMappingEntry
			xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">
			<NewPortMappingIndex>${params.index}</NewPortMappingIndex>
		</u:GetGenericPortMappingEntry>
	</s:Body>
</s:Envelope>`;
    return xml;
};
export const GetExternalIPAddressXML = (params) => {
    const xml = `<?xml version="1.0" encoding="utf-8"?>
<s:Envelope
    xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
    <s:Body>
        <u:GetExternalIPAddress
            xmlns:u="${params.action}">
        </u:GetExternalIPAddress>
    </s:Body>
</s:Envelope>`;
    return xml;
};
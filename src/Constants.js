export const languages = [
  { label: 'English', value: 'en' },
  { label: 'Persian', value: 'fa' },
  { label: 'Chinese', value: 'cn' },
];

export const tunnels = [
  { label: 'ShadowSocks', value: 'shadowsocks' },
  { label: 'Wireguard', value: 'wireguard' },
  { label: 'Tor', value: 'tor' },
  { label: 'VMess (in development)', value: 'vmess', disabled: true },
  { label: 'VLESS (in development)', value: 'vless', disabled: true },
  { label: 'Trojan (in development)', value: 'trojan', disabled: true },
  { label: 'Choose for me (in development)', value: 'choose', disabled: true },
];

export const PERFECT_DIAG_CODE = 127;

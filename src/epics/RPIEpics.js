import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { RPIServices } from '../services';

export const searchDevice = createEpic({
  type: actionTypes.SEARCH_FOR_DEVICE,
  epic: (payload, dispatch, getState) => {
    return RPIServices.searchDevice();
  },
});

export const getErrorLog = createEpic({
  type: actionTypes.GET_ERROR_LOG,
  epic: (payload, dispatch, getState) => {
    return RPIServices.getErrorLog(payload);
  },
});

export const getAccessLink = createEpic({
  type: actionTypes.GET_ACCESS_LINK,
  epic: (payload, dispatch, getState) => {
    return RPIServices.getAccessLink(payload);
  },
});

export const getProgress = createEpic({
  type: actionTypes.GET_PROGRESS,
  epic: (payload, dispatch, getState) => {
    return RPIServices.getProgress(payload);
  },
});

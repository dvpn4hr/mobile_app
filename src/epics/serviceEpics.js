import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { APIServices } from '../services';

export const checkService = createEpic({
  type: actionTypes.CHECK_SERVICE,
  epic: (payload, dispatch, getState) => {
    return APIServices.checkService(payload);
  },
});

import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { ModemServices } from '../services';

export const getWanIP = createEpic({
    type: actionTypes.GET_WAN_IP,
    epic: (payload, dispatch, getState) => {
        return ModemServices.getWanIP(payload);
    }
});

export const addPortForwarding = createEpic({
    type: actionTypes.ADD_PORT_FORWARDING,
    epic: (payload, dispatch, getState) => {
        return ModemServices.addPortForwarding(payload);
    }
});

export const deletePortForwarding = createEpic({
    type: actionTypes.DELETE_PORT_FORWARDING,
    epic: (payload, dispatch, getState) => {
        return ModemServices.deletePortForwarding(payload);
    }
});

export const getGenericPortMapping = createEpic({
    type: actionTypes.GET_GENERIC_PORT_MAPPING,
    epic: (payload, dispatch, getState) => {
        return ModemServices.getGenericPortMapping(payload);
    }
})
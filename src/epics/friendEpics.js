import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { friends, friend } from '../models';
import { APIServices } from '../services';

export const getFriends = createEpic({
  type: actionTypes.LOAD_FRIENDS,
  schema: friends,
  epic: (payload, dispatch, getState) => {
    return APIServices.getFriends(payload);
  },
});

export const getFriend = createEpic({
  type: actionTypes.LOAD_FRIEND,
  schema: friend,
  epic: (payload, dispatch, getState) => {
    return APIServices.getFriend(payload);
  },
});

export const addFriend = createEpic({
  type: actionTypes.ADD_FRIEND,
  schema: friend,
  epic: (payload, dispatch, getState) => {
    return APIServices.addFriend(payload).then(response => {
      dispatch(getFriends.actionCreator());
      return response;
    });
  },
});

export const updateFriend = createEpic({
  type: actionTypes.UPDATE_FRIEND,
  schema: friend,
  epic: (payload, dispatch, getState) => {
    return APIServices.updateFriend(payload);
  },
});

export const deleteFriend = createEpic({
  type: actionTypes.DELETE_FRIEND,
  epic: (payload, dispatch, getState) => {
    return APIServices.deleteFriend(payload).then(response => {
      dispatch(getFriends.actionCreator());
      return response;
    });
  },
});

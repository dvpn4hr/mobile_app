import Snackbar from 'react-native-snackbar';
import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { devices, device, diagnostics } from '../models';
import { APIServices } from '../services';
import { setMainDevice } from '../actions';
import { makeGetEntity } from '../selectors';
export const getDevices = createEpic({
  type: actionTypes.LOAD_DEVICES,
  schema: devices,
  epic: (payload, dispatch, getState) => {
    return APIServices.getDevices(payload).then(devices => {
      const mainDevice = getState().mainDeviceId;
      if (!mainDevice && devices.length > 0) {
        dispatch(setMainDevice(devices[0].id));
      }
      return devices;
    });
  },
});

export const getDevice = createEpic({
  type: actionTypes.LOAD_DEVICE,
  schema: device,
  epic: (payload, dispatch, getState) => {
    return APIServices.getDevice(payload);
  },
});

export const updateDevice = createEpic({
  type: actionTypes.UPDATE_DEVICE,
  schema: device,
  epic: (payload, dispatch, getState) => {
    return APIServices.updateDevice(payload);
  },
});

export const updateDeviceStatus = createEpic({
  type: actionTypes.UPDATE_DEVICE_STATUS,
  schema: device,
  epic: (payload, dispatch, getState) => {
    return APIServices.getDevice(payload).then(async result => {
      Snackbar.show({
        text: result?.Done,
        duration: Snackbar.LENGTH_SHORT,
      });
      const commandMap = {
        start: 2,
        stop: 1,
        reboot: 3,
      };

      return {
        id: payload.id,
        status: commandMap[payload.command],
      };
    });
  },
});

export const getDeviceDiagnostics = createEpic({
  type: actionTypes.LOAD_DIAGNOSTICS,
  schema: diagnostics,
  epic: (payload, dispatch, getState) => {
    return APIServices.getDeviceDiagnosis(payload).then(diagnostics => {
      return diagnostics.map((diag, index) => ({ id: index, ...diag }));
    });
  },
});

export const addDevice = createEpic({
  type: actionTypes.ADD_DEVICE,
  schema: device,
  epic: (payload, dispatch, getState) => {
    return APIServices.claimDevice(payload).then(device => {
      dispatch(getDevices.actionCreator());
      return device;
    });
  },
});

export const removeDevice = createEpic({
  type: actionTypes.DELETE_DEVICE,
  epic: (payload, dispatch, getState) => {
    return APIServices.unclaimDevice(payload);
  },
});

export const shareAccessLink = createEpic({
  type: actionTypes.SHARE_ACCESS_LINK,
  epic: (payload, dispatch, getState) => {
    return APIServices.shareAccessLink();
  },
});

export const testPortForwarding = createEpic({
  type: actionTypes.TEST_PORT_FORWARDING,
  epic: (payload, dispatch, getState) => {
    return APIServices.testPortForwarding(payload);
  }
});

export const getExperimentResults = createEpic({
  type: actionTypes.GET_EXPERIMENT_RESULTS,
  epic: (payload, dispatch, getState) => {
    return APIServices.getExperimentResults(payload);
  }
});
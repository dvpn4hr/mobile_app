import moment from 'moment';
import actionTypes from '../actionTypes';
import { createEpic } from '../common';
import { APIServices } from '../services';
import { onSignIn, setUserToken } from '../actions';
import { getDevices } from '.';

export const signUserIn = createEpic({
  type: actionTypes.SIGN_USER_IN,
  epic: (payload, dispatch, getState) => {
    return APIServices.login(payload).then(data => {
      data['expired_at'] = moment(new Date())
        .add(data.expires_in, 's')
        .toDate();
      dispatch(
        setUserToken({
          ...data,
          remember: payload.remember,
        })
      );
      if (payload.username) {
        dispatch(
          onSignIn({
            email: payload.username,
          })
        );
      }
      dispatch(getDevices.actionCreator());
      return data;
    });
  },
});

export const signUserUp = createEpic({
  type: actionTypes.SIGN_USER_UP,
  epic: (payload, dispatch, getState) => {
    return APIServices.signup(payload).then(async result => {
      await dispatch(
        signUserIn.actionCreator({
          username: payload.email,
          password: payload.password,
          grant_type: 'password',
        })
      );
      return result;
    });
  },
});

export const resetUserPassword = createEpic({
  type: actionTypes.RESET_USER_PASSWORD,
  epic: (payload, dispatch, getState) => {
    return APIServices.forgotPassword(payload);
  },
});

export const setUserPassword = createEpic({
  type: actionTypes.SET_USER_PASSWORD,
  epic: (payload, dispatch, getState) => {
    return APIServices.resetPassword(payload);
  },
});

export const signUserOut = createEpic({
  type: actionTypes.SIGN_USER_OUT,
  epic: (payload, dispatch, getState) => {
    return APIServices.logout(payload);
  },
});

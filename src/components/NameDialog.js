import i18n from 'i18n-js';
import React, { useState } from 'react';
import { StyleSheet, Text, View, Modal } from 'react-native';
import { useDispatch } from 'react-redux';
import { Button, InputText } from '.';
import { colors, constants, typo } from '../common';
import { updateDevice } from '../actions';

const NameDialog = ({ visible, device, onClose }) => {
  const [name, setName] = useState(device?.name);
  const dispatch = useDispatch();

  const onSubmit = () => {
    dispatch(
      updateDevice({
        id: device?.id,
        name,
      })
    );
    onClose();
  };

  return (
    <Modal
      transparent
      visible={visible}
      animationType={'fade'}
      textID={'name-dialog'}
    >
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.title}>{i18n.t('pod_name')}</Text>
          <InputText
            placeholder={i18n.t('pod_name')}
            defaultValue={name}
            onChangeText={setName}
            leftIcon={'desktop-mac'}
            containerStyle={styles.input}
            inputStyle={styles.inputStyle}
            autoComplete={'name'}
            textContentType={'name'}
            autoCapitalize={'none'}
          />
          <View style={styles.footer}>
            <Button
              title={i18n.t('cancel')}
              onPress={onClose}
              style={styles.button}
            />
            <Button
              title={i18n.t('update')}
              type={Button.types.PRIMARY}
              style={styles.button}
              onPress={onSubmit}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default NameDialog;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: colors.colorWithAlpha('white', 0.1),
    padding: constants.PADDING,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    width: '100%',
    height: 212,
    borderRadius: 18,
    backgroundColor: colors.sub_bg,
    padding: constants.PADDING * 1.5,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  title: {
    ...typo.primary,
    fontSize: 21,
    color: colors.white,
  },
  input: {
    width: '100%',
    height: 48,
    marginTop: constants.PADDING,
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  button: {
    width: '45%',
  },
});

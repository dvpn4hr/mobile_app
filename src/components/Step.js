import React, { useEffect, useState } from 'react';
import {
  Animated,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Collapsible from 'react-native-collapsible';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { showArticle, showHelpCenter } from 'react-native-zendesk';
import { colors, getArticle, typo } from '../common';
import i18n from '../i18n';
import Button from './Button';

const Step = ({ text, index, log, status }) => {
  const [collapsed, setCollapsed] = useState(true);
  const fadeAnim = new Animated.Value(1);

  useEffect(() => {
    if (!status === 'inProgress') return;

    const flickerAnimation = Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim, {
          toValue: 0,
          duration: 350,
          useNativeDriver: true,
        }),
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 350,
          useNativeDriver: true,
        }),
      ]),
      { iterations: null }
    );

    flickerAnimation.start();

    return () => {
      flickerAnimation.stop();
    };
  }, [fadeAnim, status]);

  const getDotStyle = () => {
    if (status === 'inProgress') return styles.progressDot;
    if (status !== 'Done') return styles.dot;
    if (log?.error) {
      return styles.dotError;
    } else if (!log?.error) {
      return styles.dotSuccess;
    } else {
      return styles.dot;
    }
  };

  const article = log?.error && getArticle(log?.error);

  const handlePress = () => {
    showArticle({
      articleId: article?.id?.toString(),
      hideContactSupport: true,
    });
  };

  return (
    <Animatable.View
      key={index}
      animation="fadeInUpBig"
      style={styles.container}>
      <TouchableOpacity
        activeOpacity={1}
        disabled={!log?.error}
        style={[styles.step, !collapsed && styles.expanded]}
        onPress={() => setCollapsed(!collapsed)}>
        <Text style={styles.stepText}>{text}</Text>
        <Animated.View
          style={[
            getDotStyle(),
            { opacity: status === 'inProgress' ? fadeAnim : 1 },
          ]}
        />
      </TouchableOpacity>
      <Collapsible collapsed={collapsed} align="center">
        <View style={styles.details}>
          <Text style={styles.errorTitle}>{log?.errorTitle}</Text>
          <Text style={styles.detailsText}>{log?.error}</Text>
          <View style={styles.footer}>
            {article?.id ? (
              <Button
                title={i18n.t('learn_why')}
                onPress={handlePress}
                style={styles.button}
                type={Button.types.PRIMARY}
              />
            ) : (
              <Button
                title={i18n.t('contact_support')}
                onPress={() =>
                  showHelpCenter({
                    hideContactSupport: false,
                  })
                }
                style={styles.button}
                type={Button.types.PRIMARY}
              />
            )}
          </View>
        </View>
      </Collapsible>
    </Animatable.View>
  );
};

export default Step;

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  step: {
    width: '100%',
    height: moderateVerticalScale(60),
    backgroundColor: colors.chradeLight,
    borderRadius: moderateScale(8),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(24),
  },
  expanded: {
    borderBottomStartRadius: moderateScale(0),
    borderBottomEndRadius: moderateScale(0),
  },
  stepText: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: moderateScale(14),
  },
  dot: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.gray,
  },
  progressDot: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.gray,
    backgroundColor: colors.yellow,
  },
  dotSuccess: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.green,
  },
  dotError: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.punch,
  },
  details: {
    width: '100%',
    backgroundColor: colors.chradeLight,
    borderBottomStartRadius: moderateScale(8),
    borderBottomEndRadius: moderateScale(8),
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: moderateScale(24),
    paddingBottom: moderateVerticalScale(16),
  },
  detailsText: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: moderateScale(14),
  },
  errorTitle: {
    ...typo.primary,
    color: colors.white,
    fontSize: moderateScale(16),
    fontWeight: 'bold',
    marginBottom: moderateVerticalScale(8),
  },
  footer: {
    width: '100%',
    paddingVertical: moderateVerticalScale(8),
  },
  button: {
    width: 'auto',
    height: moderateVerticalScale(35),
    alignSelf: 'flex-end',
    paddingHorizontal: moderateScale(16),
    marginTop: moderateVerticalScale(8),
  },
});

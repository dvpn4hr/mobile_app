import React from 'react';
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { colors, constants, typo } from '../common';
import Button from './Button';
import PropTypes from 'prop-types';
const EmptyView = ({ actions, source, message, title }) => {
  return (
    <View style={styles.empty} testID={'empty-view'}>
      <View style={styles.imgContainer}>
        <Image source={source} style={styles.img} testID={'empty-image'} />
      </View>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.msg}>{message}</Text>
      <View style={styles.actions}>
        {actions?.map(action => (
          <Button
            key={action.title}
            title={action.title}
            onPress={action.onPress}
            type={action.type ?? Button.types.PRIMARY}
            style={styles.button}
            underline={action.underline}
          />
        ))}
      </View>
    </View>
  );
};

EmptyView.propTypes = {
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      onPress: PropTypes.func.isRequired,
      type: PropTypes.oneOf(Object.values(Button.types)),
      underline: PropTypes.bool,
    }),
  ),
  source: PropTypes.number.isRequired,
  message: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default EmptyView;

const styles = StyleSheet.create({
  empty: {
    width: '100%',
    height: Dimensions.get('window').height * 0.6,
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: moderateScale(constants.PADDING),
  },
  msg: {
    fontSize: moderateScale(18),
    color: colors.white,
    textAlign: 'center',
  },
  actions: {
    width: '100%',
    height: Dimensions.get('window').height * 0.15,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  button: {
    width: '50%',
    height: moderateVerticalScale(50),
    borderRadius: moderateScale(25),
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...typo.primary,
    fontSize: moderateScale(20),
    color: colors.white,
  },
  imgContainer: {
    width: moderateScale(104),
    height: moderateScale(104),
    borderRadius: moderateScale(104),
    backgroundColor: colors.chradeLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    width: moderateScale(57),
    height: moderateVerticalScale(47),
  },
});

import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
const NavBar = ({
  showTitle = true,
  color = colors.white,
  title = i18n.t('compatibility_test'),
}) => {
  const navigation = useNavigation();

  const onBackPress = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      {showTitle ? (
        <Text style={styles.title}>{title?.toUpperCase()}</Text>
      ) : (
        <View />
      )}
      <TouchableOpacity onPress={onBackPress} activeOpacity={0.8}>
        <Image
          style={[styles.close, { tintColor: color }]}
          source={require('../assets/close.png')}
          testID={'close'}
        />
      </TouchableOpacity>
    </View>
  );
};

export default NavBar;

const styles = StyleSheet.create({
  close: {
    width: 24,
    height: 24,
  },
  container: {
    padding: constants.PADDING,
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.sub_bg,
  },
  title: {
    ...typo.primary,
    color: colors.white,
  },
});

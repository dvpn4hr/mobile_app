import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { BackIcon } from '.';
import { colors, constants, typo } from '../common';
import { getHeaderTitle } from '@react-navigation/elements';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

const LargeHeader = ({ navigation, route, options, back }) => {
  const title = getHeaderTitle(options, route.name);
  return (
    <View style={styles.container}>
      <View style={styles.section}>{back && <BackIcon showTitle />}</View>
      <View style={[styles.section, { padding: constants.PADDING }]}>
        <Text style={styles.title}>{title}</Text>
        {options?.showAvatar && (
          <TouchableOpacity onPress={() => navigation.navigate('Settings')}>
            <Image
              source={require('../assets/ic_settings.png')}
              style={styles.user}
              resizeMode={'contain'}
              testID={'avatar'}
            />
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

export default LargeHeader;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: moderateVerticalScale(120),
    backgroundColor: colors.mirage,
    borderBottomWidth: moderateScale(1),
    borderBottomColor: colors.gun_powder,
  },
  section: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  title: {
    ...typo.primary,
    fontSize: moderateScale(34),
    color: colors.white,
  },
  user: {
    width: moderateScale(28),
    height: moderateScale(28),
  },
});

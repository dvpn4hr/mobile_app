import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import Lottie from 'lottie-react-native';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

const LoadingIndicator = () => {
  return (
    <Lottie
      source={require('../animations/infinity.json')}
      autoPlay
      loop
      style={styles.animation}
      speed={2}
    />
  );
};

export default LoadingIndicator;

const styles = StyleSheet.create({
  animation: {
    width: moderateScale(75),
    height: moderateVerticalScale(75),
  },
});

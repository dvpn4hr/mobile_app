import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  Image,
  TouchableOpacity,
} from 'react-native';
import { colors, constants, typo } from '../common';
import PropTypes from 'prop-types';
import i18n from '../i18n';
import { moderateVerticalScale } from 'react-native-size-matters/extend';

const AlertDialog = ({
  visible = false,
  iconSrc,
  title,
  description,
  actions,
  showMore = false,
  onPressMore,
}) => {
  return visible ? (
    <View style={StyleSheet.absoluteFillObject}>
      <View style={styles.alert}>
        <View style={styles.content}>
          <View />
          <Image style={styles.icon} source={iconSrc} testID={'icon-src'} />
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.descriptionCont}>
            <Text style={styles.description}>{description} </Text>
            {showMore ? (
              <Text style={styles.learnMore} onPress={onPressMore}>
                {i18n.t('learn_more')}
              </Text>
            ) : null}
          </Text>
          <View style={styles.actions}>
            {actions?.map(action => (
              <TouchableOpacity
                key={action.title}
                onPress={action.onPress}
                activeOpacity={0.8}
                style={styles.action}>
                <Text style={styles.actionTxt}>{action.title}</Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </View>
    </View>
  ) : null;
};

AlertDialog.types = {
  visible: PropTypes.bool,
  iconSrc: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  actions: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      onPress: PropTypes.func,
    })
  ),
};

export default AlertDialog;

const styles = StyleSheet.create({
  alert: {
    flex: 1,
    padding: constants.PADDING,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.colorWithAlpha('white', 0.1),
  },
  content: {
    width: '100%',
    height: moderateVerticalScale(320),
    borderRadius: 16,
    backgroundColor: colors.charade,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icon: {
    width: 32,
    height: 32,
    marginVertical: constants.PADDING,
  },
  title: {
    ...typo.primary,
    color: colors.white,
    fontSize: 21,
  },
  description: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
    padding: constants.PADDING,
    marginVertical: constants.PADDING,
  },
  descriptionCont: {
    lineHeight: 24,
  },
  actions: {
    height: 53,
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: colors.chradeLight,
    flexDirection: 'row',
  },
  action: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionTxt: {
    ...typo.secondary,
    fontSize: 14,
    color: colors.white,
  },
  learnMore: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
    textDecorationLine: 'underline',
  },
});

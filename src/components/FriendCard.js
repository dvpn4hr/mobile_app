import React, { useMemo } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { colors } from '../common';
import { makeGetEntity } from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

const FriendCard = ({ entity_id }) => {
  const getEntity = useMemo(makeGetEntity, []);
  const friend = useSelector(state =>
    getEntity(state, { entity: 'friends', entity_id })
  );
  const navigation = useNavigation();

  const navigateToFriend = () => {
    navigation.navigate('Friend', {
      entity_id,
    });
  };

  return (
    <TouchableOpacity onPress={navigateToFriend} style={styles.container}>
      <Text style={styles.email}>{friend?.name}</Text>
      <Icon
        name={'chevron-right'}
        size={24}
        color={colors.white}
        testID={'arrow-icon'}
      />
    </TouchableOpacity>
  );
};

export default FriendCard;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 55,
  },
  email: {
    color: colors.white,
    fontWeight: 'bold',
  },
});

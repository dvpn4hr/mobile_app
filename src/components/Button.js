import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { moderateScale } from 'react-native-size-matters/extend';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { colors, typo } from '../common';

function Button({
  title,
  onPress,
  type = 'secondary',
  isLoading,
  style = {},
  textStyle = {},
  underline = false,
  iconName,
  iconColor = colors.white,
  iconSize = moderateScale(20),
}) {
  return (
    <TouchableOpacity
      style={[styles[type], style]}
      onPress={onPress}
      disabled={isLoading || type === 'disabled'}
      testID={`button-${type}`}>
      {isLoading ? (
        <ActivityIndicator
          size={'small'}
          color={colors.white}
          testID={'activity-indicator'}
        />
      ) : (
        <View style={styles.row}>
          {iconName && (
            <Icon
              name={iconName}
              size={iconSize}
              color={iconColor}
              style={styles.icon}
            />
          )}
          <Text
            style={[
              styles[`${type}Txt`] || styles.btnTxt,
              textStyle,
              underline && styles.underline,
            ]}>
            {title}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
}

Button.types = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  TERTIARY: 'tertiary',
  DISABLED: 'disabled',
};

export default Button;

const styles = StyleSheet.create({
  primary: {
    width: '100%',
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.main_color,
    alignSelf: 'center',
  },
  secondary: {
    width: '100%',
    height: 50,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  tertiary: {
    height: 50,
    borderColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  disabled: {
    width: '100%',
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.tuna,
    alignSelf: 'center',
  },
  btnTxt: {
    ...typo.secondary,
    fontSize: 14,
    color: colors.white,
  },
  disabledTxt: {
    ...typo.secondary,
    fontSize: 14,
    color: colors.gray,
  },
  underline: {
    textDecorationLine: 'underline',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    marginEnd: 5,
  },
});

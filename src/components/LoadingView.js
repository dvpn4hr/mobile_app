import React from 'react';
import LoadingIndicator from './LoadingIndicator';
import RNLoadingView from '@mahmoudaliibrahim/react-native-loading-view';
const LoadingView = props => {
  return <RNLoadingView renderLoader={() => <LoadingIndicator />} {...props} />;
};

export default LoadingView;

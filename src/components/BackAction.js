import React from "react";
import { useNavigation } from "@react-navigation/core";
import I18n from "i18n-js";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { colors, constants, typo } from "../common";

const BackAction = ({ title = I18n.t('done') }) => {
    const navigation = useNavigation();
    return (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            activeOpacity={0.8}
            style={styles.action}>
            <Text style={styles.backStyle}>{title}</Text>
        </TouchableOpacity>
    );
};

export default BackAction;

const styles = StyleSheet.create({
    backStyle: {
        ...typo.secondary,
        fontSize: 14,
        color: colors.white,
    },
    action: {
        paddingHorizontal: constants.PADDING,
        paddingVertical: constants.PADDING / 2,
    },
});
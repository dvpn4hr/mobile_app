import React, { useMemo } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { makeGetEntity } from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { colors, constants } from '../common';
import { showArticle } from 'react-native-zendesk';

const DiagnosticCard = ({ entity_id }) => {
  const getEntity = useMemo(makeGetEntity, []);
  const diag = useSelector(state =>
    getEntity(state, { entity: 'diagnostics', entity_id })
  );
  const openURL = () => {
    showArticle({
      articleId: diag?.url?.match(/([\d]+)/)?.[0],
      hideContactSupport: true,
    });
  };
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={openURL}
      testID={'diagnostic-card'}
    >
      <View style={styles.row}>
        <Icon
          name={diag?.state ? 'check-circle' : 'information-outline'}
          color={diag?.state ? colors.main_color : colors.error}
          size={18}
          testID={'diagnostic-card-icon'}
        />
        <Text style={styles.description}>{diag?.description}</Text>
      </View>
      <Icon
        name={'chevron-right'}
        size={18}
        color={colors.silver}
        testID={'chevron-right'}
      />
    </TouchableOpacity>
  );
};

export default DiagnosticCard;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    flexDirection: 'row',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  description: {
    marginStart: constants.PADDING / 2,
    color: colors.white,
  },
});

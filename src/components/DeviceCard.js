import React, { useEffect, useMemo } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { colors, constants, statusMap, typo } from '../common';
import { getDeviceIssues, makeGetEntity, makeGetEpicIds } from '../selectors';
import { Button } from '../components';
import i18n from '../i18n';
import { useNavigation } from '@react-navigation/native';
import { getDeviceDiagnostics, getFriends, getDevice } from '../actions';
import { PERFECT_DIAG_CODE } from '../Constants';

const friendEpic = { epicName: 'getFriends' };

let updateRef = null;

const DeviceCard = ({ entity_id }) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const getEntity = useMemo(makeGetEntity, []);
  const getEpicIds = useMemo(makeGetEpicIds, []);
  const device = useSelector(state =>
    getEntity(state, { entity: 'devices', entity_id })
  );
  const friends = useSelector(state => getEpicIds(state, friendEpic));
  const issues = useSelector(getDeviceIssues);
  const isMultipleIssues = issues?.length > 1;
  const deviceStatus = useMemo(
    () => statusMap?.[device?.status]?.(device?.diag_code),
    [device?.status, device?.diag_code]
  );
  const isStarting = device?.status === 3;

  useEffect(() => {
    if (deviceStatus?.error) {
      dispatch(getDeviceDiagnostics({ device_code: device?.diag_code }));
    } else {
      dispatch(getFriends());
    }
  }, [deviceStatus]);

  useEffect(() => {
    if (isStarting && !updateRef) {
      updateRef = setInterval(() => {
        dispatch(getDevice({ id: device?.id }));
      }, 5000);
    } else {
      clearInterval(updateRef);
      updateRef = null;
    }
    return () => {
      clearInterval(updateRef);
      updateRef = null;
    };
  }, [deviceStatus]);

  const navigate = screen => {
    navigation.navigate(screen, { deviceId: device?.id });
  };

  const getDescription = () => {
    if (device?.status !== 2) {
      return deviceStatus?.description;
    } else {
      if ((device?.diag_code | 32) === PERFECT_DIAG_CODE) {
        return i18n.t(
          friends?.length > 0 ? 'access_key_granted' : 'ready_for_people',
          { count: friends?.length }
        );
      } else {
        return isMultipleIssues
          ? i18n.t('review_pod_health')
          : issues?.[0]?.description;
      }
    }
  };

  const renderState = () => (
    <>
      <Image
        style={styles.success}
        source={require('../assets/shield.png')}
        testID={'shield'}
      />
      <Text style={styles.title}>{i18n.t('VPN')}</Text>
      <Text style={styles.description}>{getDescription()}</Text>
    </>
  );

  const renderGrant = () => (
    <Button
      title={i18n.t('grant_access')}
      onPress={() => navigate('GrantAccess')}
      type={Button.types.PRIMARY}
      style={styles.btn}
    />
  );

  const getDeviceStyle = () => {
    if (deviceStatus?.canGrant) {
      return styles.full;
    } else {
      return styles.collapsed;
    }
  };

  return (
    <TouchableOpacity
      style={[styles.container, getDeviceStyle()]}
      onPress={() => navigate('Device')}>
      <View style={styles.row}>
        <View
          style={[styles.circle, { backgroundColor: deviceStatus?.color }]}
          testID={'circle'}
        />
        <Text style={styles.status}>{deviceStatus?.title}</Text>
      </View>
      {renderState()}
      {deviceStatus?.canGrant && renderGrant()}
    </TouchableOpacity>
  );
};

export default DeviceCard;

const styles = StyleSheet.create({
  success: {
    width: 36,
    height: 36,
  },
  container: {
    backgroundColor: colors.charade,
    marginTop: constants.PADDING,
    borderRadius: 8,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    margin: constants.PADDING,
    padding: constants.PADDING,
  },
  full: {
    height: 245,
  },
  collapsed: {
    height: 164,
  },
  title: {
    ...typo.primary,
    color: colors.white,
    fontSize: 15,
  },
  description: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: 14,
    lineHeight: 24,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circle: {
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  status: {
    ...typo.ancillary,
    color: colors.white,
    marginStart: constants.PADDING / 2,
  },
  btn: {
    width: 144,
    height: 38,
    alignSelf: 'flex-start',
  },
});

import React, { useMemo } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { colors, constants } from '../common';
import { makeGetEntity } from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';

const Device = ({ entity_id }) => {
  const navigation = useNavigation();
  const getEntity = useMemo(makeGetEntity, []);
  const device = useSelector(state =>
    getEntity(state, { entity: 'devices', entity_id })
  );

  const navigate = () => {
    navigation.navigate('DeviceDetails', { deviceId: entity_id });
  };

  return (
    <TouchableOpacity onPress={navigate} style={styles.container}>
      <Text style={styles.name}>{device?.name}</Text>
      <Icon
        name={'chevron-right'}
        size={24}
        color={colors.white}
        testID={'arrow-icon'}
      />
    </TouchableOpacity>
  );
};

export default Device;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: 55,
    backgroundColor: colors.chradeLight,
    paddingVertical: constants.PADDING / 2,
    paddingHorizontal: constants.PADDING,
  },
  name: {
    color: colors.white,
    fontWeight: 'bold',
  },
});

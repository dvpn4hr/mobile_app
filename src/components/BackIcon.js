import React from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { colors, constants, typo } from '../common';
import i18n from 'i18n-js';

const BackIcon = ({ showTitle, type = 'back' }) => {
  const navigation = useNavigation();

  const onBackPress = () => {
    navigation.goBack();
  };

  return (
    <TouchableOpacity
      testID="container"
      style={styles.container}
      onPress={onBackPress}>
      <Image
        style={type === 'close' ? styles.close : styles.backImage}
        source={
          type === 'close'
            ? require('../assets/back_x.png')
            : require('../assets/arrowBack.png')
        }
        testID="back-icon"
      />
      {showTitle && <Text style={styles.back}>{i18n.t('back')}</Text>}
    </TouchableOpacity>
  );
};

export default BackIcon;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  backImage: {
    width: 8,
    height: 14,
    resizeMode: 'contain',
    marginHorizontal: constants.PADDING,
  },
  back: {
    ...typo.secondary,
    fontSize: 16,
    color: colors.white,
  },
  close: {
    width: 28,
    height: 28,
    resizeMode: 'contain',
    marginHorizontal: constants.PADDING,
  },
});

import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';
import { colors, constants } from '../common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

const InputText = React.forwardRef((props, ref) => {
  const {
    leftIcon,
    placeholder,
    onChangeText,
    defaultValue = '',
    secureTextEntry,
    error,
    containerStyle = {},
    showBottomBorder = false,
    source,
    imgStyle,
    ...otherProps
  } = props;
  const [text, setText] = useState(defaultValue);
  const [show, setShow] = useState(secureTextEntry);
  const [focused, setFocused] = useState(false);
  useEffect(() => {
    onChangeText(text);
  }, [text]);

  const onFocus = () => {
    setFocused(true);
  };

  const onBlur = () => {
    setFocused(false);
  };

  const getBorderColor = () => {
    if (error) {
      return colors.error;
    } else if (focused) {
      return colors.main_color;
    } else {
      return colors.colorWithAlpha('white', 0.2);
    }
  };

  return (
    <>
      <View
        style={[
          styles.container,
          containerStyle,
          { borderColor: getBorderColor() },
          showBottomBorder && styles.borderBottom,
        ]}
        testID={'inputContainer'}>
        {source ? (
          <Image
            source={source}
            style={imgStyle || styles.img}
            testID={'img'}
          />
        ) : (
          <Icon
            name={leftIcon}
            size={24}
            color={colors.colorWithAlpha('white', 0.8)}
            testID={'leftIcon'}
          />
        )}
        <TextInput
          ref={ref}
          style={styles.textInput}
          placeholder={placeholder}
          placeholderTextColor={colors.colorWithAlpha('white', 0.8)}
          onChangeText={text => setText(text)}
          value={text}
          secureTextEntry={show}
          testID={'textInput'}
          onFocus={onFocus}
          onBlur={onBlur}
          {...otherProps}
        />
        {text.length === 0 ? (
          <View />
        ) : secureTextEntry ? (
          <TouchableOpacity onPress={() => setShow(!show)}>
            <Icon
              name={show ? 'eye' : 'eye-off'}
              size={24}
              color={colors.colorWithAlpha('white', 0.8)}
              testID={'passwordIcon'}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => setText('')}>
            <Icon
              name={'close'}
              size={24}
              color={colors.colorWithAlpha('white', 0.8)}
              testID={'clearIcon'}
            />
          </TouchableOpacity>
        )}
      </View>
      <Text style={styles.errorTxt}>{error}</Text>
    </>
  );
});

export default InputText;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: moderateVerticalScale(50),
    paddingHorizontal: constants.PADDING,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: colors.chradeLight,
  },
  textInput: {
    flex: 1,
    paddingHorizontal: constants.PADDING,
    fontSize: moderateScale(16),
    color: colors.white,
    height: '100%',
  },
  errorTxt: {
    fontSize: moderateScale(14),
    paddingVertical: constants.PADDING / 2,
    color: colors.error,
    alignSelf: 'flex-start',
  },
  borderBottom: {
    borderWidth: 0,
    borderBottomWidth: moderateScale(1),
  },
  img: {
    width: moderateScale(20),
    height: moderateVerticalScale(12),
    resizeMode: 'contain',
  },
});

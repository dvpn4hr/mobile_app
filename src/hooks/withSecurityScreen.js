import React from 'react';
import { AppState, Platform, View, StyleSheet } from 'react-native';
import { BlurView } from '@react-native-community/blur';

const showSecurityScreenFromAppState = appState =>
  ['background', 'inactive'].includes(appState);

const withSecurityScreenIOS = Wrapped => {
  return class WithSecurityScreen extends React.Component {
    state = {
      showSecurityScreen: showSecurityScreenFromAppState(AppState.currentState),
    };

    componentDidMount() {
      this.appState = AppState.addEventListener('change', this.onChangeAppState);
    }

    componentWillUnmount() {
      this.appState.remove();
    }

    onChangeAppState = nextAppState => {
      const showSecurityScreen = showSecurityScreenFromAppState(nextAppState);

      this.setState({ showSecurityScreen });
    };

    render() {
      const { showSecurityScreen } = this.state;
      return (
        <View style={{ flex: 1 }}>
          <Wrapped {...this.props} />
          {showSecurityScreen && (
            <BlurView
              style={StyleSheet.absoluteFillObject}
              blurType="light"
              blurAmount={30}
              reducedTransparencyFallbackColor="white"
            />
          )}
        </View>
      );
    }
  };
};

const withSecurityScreenAndroid = Wrapped => Wrapped;

export const withSecurityScreen =
  Platform.OS === 'ios' ? withSecurityScreenIOS : withSecurityScreenAndroid;

import { withSecurityScreen } from './withSecurityScreen';
import useCompatibilityProcess from './useCompatibilityProcess';

export { useCompatibilityProcess, withSecurityScreen };

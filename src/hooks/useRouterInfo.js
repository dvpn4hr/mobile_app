import { useEffect, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';
import { extractData, extractPort, extractWanIP, Logger } from '../common';
import { getWanIP } from '../actions';
import i18n from '../i18n';

const Client = require('react-native-ssdp').Client;
const logger = new Logger();

function useRouterInfo() {
  const dispatch = useDispatch();
  const [info, setInfo] = useState(null);
  const [error, setError] = useState(null);
  const clientRef = useRef(null);
  const timeoutRef = useRef(null);
  useEffect(() => {
    search();
    timeoutRef.current = setTimeout(() => {
      onError(new Error(i18n.t('no_upnp_devices_found')));
    }, 15000);

    return () => {
      clientRef.current?.stop();
      clientRef.current?.removeAllListeners();
      clearTimeout(timeoutRef.current);
    };
  }, []);

  const search = () => {
    clientRef.current = new Client({
      explicitSocketBind: true,
      reuseAddr: false,
    });

    clientRef.current.on('response', onResponse);
    clientRef.current.search('ssdp:all');
  };

  const onResponse = (headers, statusCode, rinfo) => {
    if (
      statusCode === 200 &&
      headers?.ST?.includes('urn:schemas-upnp-org:service:WANIPConnection')
    ) {
      clientRef.current.stop();
      clearTimeout(timeoutRef.current);
      getRouterInfo(headers, rinfo);
    }
  };

  const getRouterInfo = async (headers, rInfo) => {
    const { LOCATION } = headers;
    const { address: ipAddress } = rInfo;

    try {
      const response = await fetch(LOCATION);
      const detailsXML = await response.text();
      const { device, path, action } = extractData(detailsXML);
      const port = extractPort(LOCATION);
      const params = { ipAddress, path, action, port };
      const { result } = await dispatch(getWanIP(params));
      logger.log({
        request: 'getWanIP',
        params,
        result,
      });
      const wanIp = extractWanIP(result?.data);
      setInfo({
        wanIp,
        device,
        ipAddress,
        path,
        action,
        port,
      });
    } catch (error) {
      onError(error);
    }
  };

  const onError = error => {
    clientRef.current?.stop();
    setError(error);
  };

  return [info, error];
}

export default useRouterInfo;

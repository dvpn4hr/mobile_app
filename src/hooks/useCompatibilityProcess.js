import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  testPortForwarding,
  getExperimentResults,
  addPortForwarding,
  deletePortForwarding,
  getGenericPortMapping,
} from '../actions';
import i18n from '../i18n';
import NetInfo, { NetInfoStateType } from '@react-native-community/netinfo';
import { SpeedTest } from '../services';
import random from 'lodash/random';
import TcpSocket from 'react-native-tcp-socket';
import { getUserId } from '../selectors';
import useRouterInfo from './useRouterInfo';
import { Logger, checkName, extractMapping } from '../common';
import pick from 'lodash/pick';

const mappingLogger = new Logger();
const errorLogger = new Logger();
const report = new Logger();

const initialStep = { step: null, error: null };
const deviceInfo = {
  serial_number: 'COMPATIBLE',
  device_key: 'CWGWNFAL2F',
};

function useCompatibilityProcess() {
  const dispatch = useDispatch();
  const user_id = useSelector(getUserId);
  const [info, error] = useRouterInfo();
  const [step, setStep] = useState(initialStep);

  useEffect(() => {
    return () => {
      report.deleteLogs();
      errorLogger.deleteLogs();
      mappingLogger.deleteLogs();
    };
  }, []);

  useEffect(() => {
    setStep({ step: 0 });
    if (info || error) {
      checkForwarding();
      report.log({
        step: 0,
        error: error?.message,
        info,
        errorTitle: i18n.t('no_upnp_device_found'),
      });
    }
  }, [info, error]);

  const getLocalIpAddress = async () => {
    const netInfo = await NetInfo.fetch(NetInfoStateType.wifi);
    const { details: { ipAddress: host } = {} } = netInfo;

    return host;
  };

  const checkForwarding = async () => {
    setStep({ step: 1 });
    const forwardingPort = random(1024, 10000);
    const host = await getLocalIpAddress();

    const { ipAddress, path, action, port, wanIp, device } = info || {};
    const params = {
      ipAddress,
      path,
      action,
      port,
      forwardingPort,
      host,
    };

    try {
      const response = await dispatch(addPortForwarding(params));
      createSocket(wanIp, device, forwardingPort);
      if (response?.error) {
        if (!ipAddress) {
          throw new Error(i18n.t('can_t_forward_port_while_upnp_is_disabled'));
        } else {
          throw new Error(response.error);
        }
      }
      report.log({
        step: 1,
        params,
      });
    } catch (error) {
      errorLogger.log({
        request: 'addPortForwarding',
        endpoint: `${ipAddress}:${port}${path}`,
        error: error.message,
      });
      report.log({
        step: 1,
        error: error?.message,
        errorTitle: i18n.t('port_forwarding_failed'),
      });
    }
  };

  const createSocket = async (wan_ip, device, port) => {
    setStep({ step: 2 });
    const host = await getLocalIpAddress();

    TcpSocket.createServer(_ => {})
      .listen({ port, host })
      .on('error', onSocketError);
    report.log({
      step: 2,
      params: {
        wan_ip,
        device,
        port,
        host,
      },
    });
    runForwardingTest(port, wan_ip);
  };

  const getGenericPortMappings = async () => {
    let index = 0;
    let success = true;

    while (success) {
      const params = {
        ...info,
        index,
      };
      const { ipAddress, path, port } = info || {};
      const { result } = await dispatch(getGenericPortMapping(params));
      success = result?.respInfo?.status === 200;

      if (success) {
        const { port, name, protocol } = extractMapping(result?.data);

        mappingLogger.log({
          [port]: checkName(name),
          transport: protocol,
        });
      } else {
        success = false;
        if (result?.error) {
          errorLogger.log({
            request: 'getGenericPortMapping',
            endpoint: `${ipAddress}:${port}${path}`,
            error: result?.error,
          });
        }
      }
      index++;
    }
  };

  const getPublicIp = async () => {
    try {
      const response = await fetch('https://api.ipify.org?format=json');
      const { ip } = await response.json();

      return ip;
    } catch (error) {
      errorLogger.log({
        request: 'getPublicIp',
        endpoint: 'https://api.ipify.org?format=json',
        error,
      });

      return null;
    }
  };

  const getNetworkSpeed = async () => {
    try {
      const { speed, metric } = await SpeedTest();
      return `${speed} ${metric}`;
    } catch (error) {
      errorLogger.log({
        request: 'getNetworkSpeed',
        error,
      });
    }
  };

  const runForwardingTest = async (port, wan_ip) => {
    setStep({ step: 3 });

    const [publicIp, _, networkSpeed] = await Promise.all([
      getPublicIp(),
      getGenericPortMappings(),
      getNetworkSpeed(),
    ]);

    try {
      const {
        result: { id },
      } = await dispatch(
        testPortForwarding({
          ...deviceInfo,
          input: {
            experiment_name: 'port_test',
            port,
            ip_address: publicIp,
            router_info: pick(info, [
              'device.adminPanelURL',
              'device.presentationURL',
              'port',
              'device.friendlyName',
              'device.manufacturer',
              'device.manufacturerURL',
              'device.modelDescription',
              'device.modelName',
            ]),
            errorLogs: errorLogger.getLogs(),
            networkSpeed,
            mappingLogs: mappingLogger.getLogs(),
            wan_ip,
          },
          user_id,
        })
      );
      report.log({
        step: 3,
        params: {
          publicIp,
          networkSpeed,
          id,
        },
      });

      setTimeout(() => checkResult(id, port), 10000);
    } catch (error) {
      logError(error.message, false);
      report.log({
        step: 3,
        error,
      });
    }
  };

  const checkResult = async (id, port) => {
    setStep({ step: 4 });

    try {
      const {
        result: {
          result: { experiment_result = 'False' },
        },
      } = await dispatch(getExperimentResults({ id, body: deviceInfo }));

      if (experiment_result === 'True') {
        report.log({
          step: 4,
          params: {
            experiment_result,
          },
        });
      } else {
        logError(i18n.t('port_forwarding_test_failed'), false);
        report.log({
          step: 4,
          error: i18n.t('port_forwarding_test_failed'),
          errorTitle: i18n.t('port_forwarding_test_failed'),
        });
      }
    } catch (error) {
      logError(error.message, false);
      report.log({
        step: 4,
        error: error.message,
        errorTitle: i18n.t('port_forwarding_test_failed'),
      });
    }
    setStep({ step: 5 });
    mappingLogger.deleteLogs();
    errorLogger.deleteLogs();
    dispatch(
      deletePortForwarding({
        ...info,
        forwardingPort: port,
        host: await getLocalIpAddress(),
      })
    );
  };

  const onSocketError = () => {
    logError(i18n.t('socket_error'));
    report.log({
      step: 2,
      error: i18n.t('socket_error'),
      errorTitle: i18n.t('socket_error'),
    });
  };

  const logError = (error, runTest = true) => {
    const currentStep = step.step;

    if (runTest) {
      dispatch(
        testPortForwarding({
          ...deviceInfo,
          input: {
            error,
            step: currentStep,
          },
          user_id,
        })
      );
    }
  };

  return { ...step, ...report };
}

export default useCompatibilityProcess;

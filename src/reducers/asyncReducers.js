import * as asyncReducers from '../epics';
import each from 'lodash/each';
let reducers = {};

each(asyncReducers, (action, name) => {
  reducers[name] = action.reducer;
});

module.exports = reducers;

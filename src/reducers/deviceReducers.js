import actionTypes from '../actionTypes';

export const mainDeviceId = (state = null, action) => {
  switch (action.type) {
    case actionTypes.SET_MAIN_DEVICE:
      return action.id;
    case actionTypes.SIGN_USER_OUT:
      return null;
    default:
      return state;
  }
};

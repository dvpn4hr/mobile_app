import actionTypes from '../actionTypes';
import merge from 'lodash/merge';
export const firstTime = (state = true, action) => {
  switch (action.type) {
    case actionTypes.SET_FIRST_TIME:
      return action.state;
    default:
      return state;
  }
};

export const entities = (state = {}, action) => {
  const newEntities = action?.entities;
  if (newEntities) {
    return merge({}, state, newEntities);
  }
  switch (action.type) {
    case actionTypes.SIGN_USER_OUT:
      return {};
    default:
      return state;
  }
};

const initialState = {
  language: 'en',
  config: {
    tunnel: 'shadowsocks',
  },
};

export const accessData = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_ACCESS_DATA:
      return { ...state, ...action.data };
    case actionTypes.RESET_ACCESS_DATA:
      return initialState;
    default:
      return state;
  }
};

export const isProduction = (state = true, action) => {
  switch (action.type) {
    case actionTypes.SWITCH_ENVIRONMENT:
      return !state;
    default:
      return state;
  }
};

import actionTypes from '../actionTypes';

export const userToken = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SET_USER_TOKEN:
      return action.data;
    case actionTypes.SIGN_USER_OUT:
      return {};
    default:
      return state;
  }
};

export const userData = (state = {}, action) => {
  switch (action.type) {
    case actionTypes.SET_USER_DATA:
      return action.data;
    case actionTypes.SIGN_USER_OUT:
      return {};
    default:
      return state;
  }
};

export const deviceKey = (state = null, action) => {
  switch (action.type) {
    case actionTypes.SET_DEVICE_KEY:
      return action.key;
    case actionTypes.SIGN_USER_OUT:
      return null;
    default:
      return state;
  }
};

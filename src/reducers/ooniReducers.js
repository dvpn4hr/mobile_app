import actionTypes from '../actionTypes';

export const OONISetup = (state = false, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_OONI_SETUP:
      return action.state;
    default:
      return state;
  }
};

export const OONIWorking = (state = false, action) => {
  switch (action.type) {
    case actionTypes.TOGGLE_OONI_WORKING:
      return action.state;
    default:
      return state;
  }
};

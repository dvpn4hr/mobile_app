import actionTypes from '../actionTypes';
import filter from 'lodash/filter';

export const friendHash = (state = [], action) => {
  switch (action.type) {
    case actionTypes.ADD_FRIEND_HASH:
      return [...state, action.device];
    case actionTypes.REMOVE_FRIEND_HASH:
      let oldFriends = state;
      let friendsHash = filter(
        oldFriends,
        friend =>
          friend.id !== action.friend_id &&
          friend.device_id !== action.device_id
      );
      return friendsHash;
    default:
      return state;
  }
};

import AsyncStorage from '@react-native-async-storage/async-storage';
import pick from 'lodash/pick';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import logger from 'redux-logger';
import { createTransform, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import * as asyncReducers from './asyncReducers';
import * as commonReducers from './commonReducers';
import * as friendReducers from './friendReducers';
import * as userReducers from './userReducers';
import * as deviceReducers from './deviceReducers';
import * as ooniReducers from './ooniReducers';
import { auth } from '../middlewares';
import { SecureStorage } from '../common';

let middleWares = [thunk, auth];
if (__DEV__) {
  middleWares.push(logger);
}

const whiteListTransform = createTransform((inboundState, key) => {
  switch (key) {
    case 'userToken':
      return inboundState.remember ? inboundState : {};
    default:
      return inboundState;
  }
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: [
    'firstTime',
    'mainDeviceId',
    'OONISetup',
    'OONIWorking',
    'isProduction',
  ],
  throttle: 30,
};

const authPersistConfig = {
  key: 'auth',
  storage: SecureStorage,
  whitelist: ['userToken', 'userData', 'deviceKey', 'friendHash'],
  transforms: [whiteListTransform],
  throttle: 30,
};

const appReducer = combineReducers({
  ...commonReducers,
  ...deviceReducers,
  ...ooniReducers,
  auth: persistReducer(
    authPersistConfig,
    combineReducers({ ...userReducers, ...friendReducers })
  ),
  epics: combineReducers({ ...asyncReducers }),
});

const persistedReducer = persistReducer(persistConfig, appReducer);

const store = createStore(
  persistedReducer,
  compose(applyMiddleware(...middleWares))
);

const reducer = (state, action) => {
  return appReducer(state, action);
};

module.exports = {
  store,
  reducer,
};

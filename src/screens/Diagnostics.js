import i18n from 'i18n-js';
import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  TouchableOpacity,
} from 'react-native';
import ValidationComponent from 'react-native-form-validator';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { getDeviceDiagnostics } from '../actions';
import { colors, constants, typo } from '../common';
import { DiagnosticCard, InputText, LoadingView } from '../components';
import {
  makeGetEpicError,
  makeGetEpicIds,
  makeGetIsEpicLoading,
} from '../selectors';

class Diagnostics extends ValidationComponent {
  labels = {
    diag_code: i18n.t('diag_code'),
  };
  constructor(props) {
    super(props);
    const { route } = props;
    this.state = {
      diag_code: route?.params?.diag_code || '',
    };
  }
  componentDidMount() {
    const { route, navigation } = this.props;
    const diag_code = route?.params?.diag_code;
    if (diag_code) {
      this.getDeviceDiagnostics(diag_code);
    }
    navigation.setOptions({ headerTitle: i18n.t('pod_health') });
  }

  getDeviceDiagnostics = diag_code => {
    const { getDeviceDiagnostics } = this.props;
    const isValid = this.validate({
      diag_code: { required: true, minLength: 3, maxLength: 8, numbers: true },
    });
    if (isValid) {
      getDeviceDiagnostics({ device_code: Number(diag_code) });
    }
  };

  navigateToTicket = () => {
    const { navigation } = this.props;

    navigation.navigate('Ticket');
  };

  renderItem = ({ item, index }) => <DiagnosticCard entity_id={item} />;

  renderSeparator = () => <View style={styles.separator} />;

  renderFooter = () => (
    <>
      {this.renderSeparator()}
      <View style={styles.footer}>
        {this.renderSeparator()}
        <TouchableOpacity
          style={styles.contactSupport}
          onPress={this.navigateToTicket}>
          <Text style={styles.contactSupportText}>
            {i18n.t('contact_support')}
          </Text>
          <Icon name={'chevron-right'} size={24} color={colors.silver} />
        </TouchableOpacity>
        {this.renderSeparator()}
      </View>
    </>
  );

  render() {
    const { isLoading, diags, error } = this.props;
    const { diag_code } = this.state;

    return (
      <LoadingView
        isLoading={isLoading}
        backgroundColor={colors.sub_bg}
        indicatorColor={colors.white}
        containerStyle={styles.container}>
        <InputText
          leftIcon={'magnify'}
          placeholder={i18n.t('look_up_a_diagnostic')}
          showClear
          onChangeText={diag_code => this.setState({ diag_code })}
          error={
            this.getErrorsInField('diag_code')[0] || error?.error_description
          }
          autoComplete={'off'}
          keyboardType={'number-pad'}
          maxLength={8}
          numeric
          defaultValue={diag_code.toString()}
          containerStyle={styles.input}
          inputStyle={styles.inputStyle}
          onSubmitEditing={() => this.getDeviceDiagnostics(diag_code)}
        />
        <Text style={styles.sectionTitle}>
          {i18n.t('health_checks').toUpperCase()}
        </Text>
        <FlatList
          data={diags}
          renderItem={this.renderItem}
          style={styles.list}
          showsVerticalScrollIndicator={false}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderSeparator}
          ListFooterComponent={this.renderFooter}
        />
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getIds = makeGetEpicIds();
  const getIsLoading = makeGetIsEpicLoading();
  const getError = makeGetEpicError();
  const mapStateToProps = state => ({
    diags: getIds(state, { epicName: 'getDeviceDiagnostics' }),
    isLoading: getIsLoading(state, { epicName: 'getDeviceDiagnostics' }),
    error: getError(state, { epicName: 'getDeviceDiagnostics' }),
  });
  return mapStateToProps;
};

const mapDispatchToProps = {
  getDeviceDiagnostics,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(Diagnostics);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  list: {
    marginVertical: constants.PADDING,
  },
  separator: {
    width: '98%',
    height: 0.4,
    backgroundColor: colors.manatee,
    alignSelf: 'center',
  },
  input: {
    width: '100%',
    height: 55,
    borderBottomWidth: 0,
    backgroundColor: colors.chradeLight,
    marginTop: constants.PADDING,
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: 12,
    color: colors.main_color,
    fontWeight: 'bold',
    paddingHorizontal: constants.PADDING,
    paddingBottom: constants.PADDING / 2,
  },
  contactSupport: {
    width: '100%',
    height: 50,
    paddingHorizontal: constants.PADDING,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: colors.charade,
  },
  footer: {
    marginVertical: constants.PADDING,
  },
  contactSupportText: {
    color: colors.white,
    fontSize: 14,
  },
});

import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Image, StatusBar } from 'react-native';
import { useDispatch } from 'react-redux';
import { colors, typo } from '../common';
import { Button } from '../components';
import {
  moderateScale,
  moderateVerticalScale,
  verticalScale,
} from 'react-native-size-matters/extend';
import { setFirstTime } from '../actions';
import i18n from 'i18n-js';
import { identifyAnonymous } from 'react-native-zendesk';

const Intro = ({ navigation }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    identifyAnonymous();
    dispatch(setFirstTime(false));
  }, []);

  const navigate = (screen, props = {}) => {
    navigation.navigate('Auth', {
      screen: screen,
      params: props,
    });
  };

  const navigateToTest = () => {
    navigation.navigate('Compatibility', {
      screen: 'CompatibilityTest',
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle={'light-content'} backgroundColor={colors.sub_bg} />
      <Image source={require('../assets/wepn-mark.png')} style={styles.mark} />
      <Text style={styles.pod}>{i18n.t('wepn_pod')}</Text>
      <View style={styles.body}>
        <Text style={styles.intro}>{i18n.t('a_custom_vpn_made_easy')}</Text>
        <Text style={styles.sub}>{i18n.t('before_ordering')}</Text>
        <Button
          title={i18n.t('check_compatibility')}
          type={Button.types.PRIMARY}
          onPress={navigateToTest}
        />

        <View style={styles.row}>
          <View style={styles.line} />
          <Text style={styles.OR}>{i18n.t('or')}</Text>
          <View style={styles.line} />
        </View>
        <Button
          title={i18n.t('login')}
          onPress={() => navigate('Login', { isDirect: true })}
          style={styles.bottomMargin}
        />
        <Button
          title={i18n.t('create_account')}
          type={Button.types.SECONDARY}
          onPress={() => navigate('SignUp')}
        />
      </View>
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.sub_bg,
    padding: moderateScale(32),
  },
  pod: {
    ...typo.secondary,
    fontSize: moderateScale(16),
    paddingTop: moderateVerticalScale(16),
    paddingBottom: moderateVerticalScale(42),
    color: colors.white,
  },
  intro: {
    ...typo.primary,
    fontSize: moderateScale(28),
    paddingBottom: moderateScale(9),
    textAlign: 'center',
    color: colors.white,
  },
  sub: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    paddingBottom: moderateVerticalScale(64),
    color: colors.white,
    textAlign: 'center',
  },
  body: {
    width: '100%',
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: moderateVerticalScale(14),
  },
  line: {
    width: '40%',
    height: verticalScale(0.5),
    backgroundColor: colors.gray,
  },
  OR: {
    color: colors.white,
  },
  mark: {
    width: moderateScale(112),
    height: moderateScale(43),
    resizeMode: 'contain',
  },
  bottomMargin: {
    marginBottom: moderateVerticalScale(12),
  },
});

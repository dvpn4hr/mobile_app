import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React, { useEffect } from 'react';
import { colors, constants } from '../common';
import Swiper from 'react-native-swiper';
import { Manuals } from '../assets/Manuals';
import { useNavigation } from '@react-navigation/core';
import RNShare from 'react-native-share';
import i18n from '../i18n';

const ImageSlider = () => {
  const navigation = useNavigation();

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={shareManual}>
          <Image source={require('../assets/share.png')} style={styles.share} />
        </TouchableOpacity>
      ),
    });
  }, []);

  const shareManual = () => {
    RNShare.open({
      title: i18n.t('share_manuals'),
      urls: Manuals,
      failOnCancel: false,
    });
  };

  return (
    <View style={styles.container}>
      <Swiper style={styles.wrapper} showsButtons={false} loop>
        {Manuals.map(manual => (
          <View style={styles.slide1}>
            <Image style={styles.image} source={{ uri: manual }} />
          </View>
        ))}
      </Swiper>
    </View>
  );
};

export default ImageSlider;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {},
  slide1: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: constants.PADDING,
  },
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
  },
  share: {
    width: 23,
    height: 25,
    resizeMode: 'contain',
    marginHorizontal: constants.PADDING,
  },
});

import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, Text, View, Switch } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { toggleOONIWorking } from '../actions';
import { colors, constants, typo } from '../common';
import { getOONIWorking } from '../selectors';

const OONI = () => {
  const dispatch = useDispatch();
  const working = useSelector(getOONIWorking);

  const setWorking = value => {
    dispatch(toggleOONIWorking(value));
  };

  return (
    <View style={styles.container}>
      <View style={styles.mainContent}>
        <View style={styles.row}>
          <Text style={styles.title}>{i18n.t('status')}</Text>
          <View style={styles.sub_row}>
            <View
              style={[styles.point, working ? styles.ok : styles.disabled]}
            />
            <Text style={styles.status}>
              {working ? i18n.t('working') : i18n.t('disabled')}
            </Text>
          </View>
        </View>
        <View style={styles.separator} />
        <View style={styles.row}>
          <Text style={styles.title}>{i18n.t('automatic_testing')}</Text>
          <Switch
            value={working}
            onValueChange={setWorking}
            trackColor={{ true: colors.main_color }}
          />
        </View>
        <Text style={styles.status}>{i18n.t('ooni_will_run_tests')}</Text>
      </View>
    </View>
  );
};

export default OONI;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mirage,
    padding: constants.PADDING,
  },
  mainContent: {
    width: '100%',
    borderRadius: 8,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: constants.PADDING / 2,
  },
  sub_row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...typo.secondary,
    color: colors.white,
    fontSize: 17,
  },
  point: {
    marginEnd: constants.PADDING / 2,
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  ok: {
    backgroundColor: colors.green,
  },
  disabled: {
    backgroundColor: colors.gray,
  },
  status: {
    ...typo.ancillary,
    color: colors.manatee,
    fontSize: 14,
  },
  separator: {
    width: '100%',
    height: 0.5,
    backgroundColor: colors.manatee,
    marginVertical: constants.PADDING / 2,
  },
});

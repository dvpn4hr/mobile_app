import { NavigationActions } from '@react-navigation/compat';
import { CommonActions } from '@react-navigation/native';
import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
import { setFirstTime } from '../actions';
import { colors, constants } from '../common';
import i18n from '../i18n';
import { getIsFirstTime } from '../selectors';

const slides = [
  {
    text: i18n.t('walk_through.1'),
    image: require('../assets/Walkthrough/1.png'),
  },
  {
    text: i18n.t('walk_through.2'),
    image: require('../assets/Walkthrough/2.png'),
  },
  {
    text: i18n.t('walk_through.3'),
    image: require('../assets/Walkthrough/3.png'),
  },
  {
    text: i18n.t('walk_through.4'),
    image: require('../assets/Walkthrough/4.png'),
  },
  {
    text: i18n.t('walk_through.5'),
    image: require('../assets/Walkthrough/5.png'),
  },
  {
    text: i18n.t('walk_through.6'),
    image: require('../assets/Walkthrough/6.png'),
  },
  {
    text: i18n.t('walk_through.7'),
    image: require('../assets/Walkthrough/7.png'),
  },
  {
    text: i18n.t('walk_through.8'),
    image: require('../assets/Walkthrough/8.png'),
  },
  {
    text: i18n.t('walk_through.9'),
    image: require('../assets/Walkthrough/9.png'),
  },
];

class WalkThrough extends Component {
  next = () => {
    this.swiper.scrollBy(1, true);
  };

  continue = () => {
    const { setFirstTime, firstTime, navigation } = this.props;
    if (!firstTime) {
      navigation.dispatch(
        NavigationActions.navigate({
          routeName: 'Help',
        })
      );
    } else {
      setFirstTime(false);
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: 'Auth' }],
        })
      );
    }
  };

  renderFooter = () => (
    <View style={styles.footer}>
      <TouchableOpacity style={styles.cancelBtn} onPress={this.continue}>
        <Text style={styles.cancelTxt}>{i18n.t('cancel')}</Text>
      </TouchableOpacity>
    </View>
  );

  onIndexChanged = index => {
    this.setState({ index });
  };

  render() {
    return (
      <View style={styles.container}>
        <Swiper
          ref={ref => (this.swiper = ref)}
          style={styles.wrapper}
          onIndexChanged={this.onIndexChanged}
          showsButtons={false}
          loop={false}
          scrollEnabled
          activeDotColor={colors.white}
          removeClippedSubviews={false}
          paginationStyle={styles.paginationStyle}
          showsPagination
        >
          {slides.map((slide, index) => {
            const isLast = index === slides.length - 1;
            return (
              <View style={styles.slide}>
                <View style={styles.slideContent}>
                  <Image
                    style={styles.image}
                    source={slide.image}
                    resizeMode={'contain'}
                  />
                  <Text numberOfLines={2} style={styles.text}>
                    {slide.text}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={isLast ? this.continue : this.next}
                  style={styles.button}
                >
                  <Text style={styles.btnTxt}>
                    {i18n.t(isLast ? 'continue_to_the_app' : 'next')}
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
        </Swiper>
        {this.renderFooter()}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  firstTime: getIsFirstTime(state),
});

const mapDispatchToProps = {
  setFirstTime,
};

export default connect(mapStateToProps, mapDispatchToProps)(WalkThrough);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  paginationStyle: {
    position: 'absolute',
    top: constants.PADDING * 2,
    left: 0,
    right: 0,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: colors.main_color,
    height: '100%',
    width: '100%',
    paddingVertical: '20%',
  },
  slideContent: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  image: {
    width: 250,
    height: 250,
  },
  text: {
    fontSize: constants.PADDING,
    padding: constants.PADDING * 2,
    textAlign: 'center',
    fontWeight: 'bold',
    lineHeight: 24,
    color: colors.white,
  },
  button: {
    height: 50,
    paddingHorizontal: constants.PADDING,
    borderRadius: 25,
    backgroundColor: colors.colorWithAlpha('black', 0.25),
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: '30%',
  },
  btnTxt: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 16,
  },
  footer: {
    height: 50,
    backgroundColor: colors.main_color,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingHorizontal: constants.PADDING,
    shadowColor: colors.black,
    shadowOffset: {
      width: 5,
      height: 0,
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,

    elevation: 2,
  },
  cancelBtn: {
    padding: constants.PADDING,
  },
  cancelTxt: {
    color: colors.white,
    fontWeight: 'bold',
  },
});

import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import { Button } from '../components';
import i18n from '../i18n';
import { setAccessData } from '../actions';
import { tunnels } from '../Constants';

class ChangeTunnel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tunnel: 'shadowsocks',
    };
  }

  componentDidMount() {
    const { navigation } = this.props;

    navigation.setOptions({
      headerRight: () => (
        <Button
          title={i18n.t('done')}
          type={Button.types.TERTIARY}
          style={{
            width: 50,
            alignSelf: 'flex-end',
            marginHorizontal: constants.PADDING,
          }}
          onPress={this.setData}
        />
      ),
    });
  }

  setData = () => {
    const { navigation, setAccessData } = this.props;
    const { tunnel } = this.state;

    setAccessData({ tunnel });
    navigation.goBack();
  };

  setTunnel = tunnel => {
    this.setState({ tunnel }, () => {
      const { setAccessData } = this.props;

      setAccessData({ config: { tunnel } });
    });
  };

  render() {
    const { tunnel } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.grantAccess}>{i18n.t('tunnel_access')}</Text>
        {tunnels?.map(({ label, value, disabled = false }) => (
          <TouchableOpacity
            style={styles.language}
            onPress={() => this.setTunnel(value)}
            activeOpacity={1}
            disabled={disabled}>
            <Text
              style={[styles.title, disabled ? { color: colors.gray } : {}]}>
              {label}
            </Text>
            {tunnel === value && (
              <Image
                style={styles.check}
                source={require('../assets/check.png')}
              />
            )}
          </TouchableOpacity>
        ))}
        <Text style={styles.desc}>
          {i18n.t('use_tor_if_you_are_concerned')}
        </Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  setAccessData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangeTunnel);

const styles = StyleSheet.create({
  check: { width: 18, height: 13 },
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    paddingTop: constants.PADDING * 3,
  },
  grantAccess: {
    ...typo.primary,
    fontSize: 28,
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
  title: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
  },
  language: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: colors.abbey,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  desc: {
    width: '80%',
    fontSize: 16,
    color: colors.nobel,
    lineHeight: 24,
    paddingTop: constants.PADDING,
  },
});

import i18n from 'i18n-js';
import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import { colors, constants, typo } from '../common';
import { Button } from '../components';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { toggleOONISetup } from '../actions';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

const CheckItem = ({ title, onCheck, checked }) => (
  <View style={styles.checkItem}>
    <Text style={styles.body}>{title}</Text>
    <CheckBox
      value={checked}
      style={styles.checkbox}
      tintColor={colors.white}
      onCheckColor={colors.white}
      onTintColor={colors.white}
      onFillColor={colors.main_color}
      boxType={'square'}
      onValueChange={onCheck}
    />
  </View>
);

const IntroOONI = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [values, setValues] = useState(Array(3).fill(false));

  const onCheck = (checked, i) => {
    let newValues = [...values];
    newValues[i - 1] = checked;
    setValues(newValues);
  };

  const checked = values.every(v => v);

  const navigateToOONI = () => {
    dispatch(toggleOONISetup(true));
    navigation.replace('OONI');
  };

  return (
    <View style={styles.container}>
      {[1, 2, 3].map(i => (
        <CheckItem
          title={i18n.t(`ooni_description.${i}`)}
          onCheck={checked => onCheck(checked, i)}
          checked={values[i - 1]}
        />
      ))}
      <Text style={styles.subTitle}>{i18n.t('check_the_boxes')}</Text>
      <Button
        title={i18n.t('continue')}
        onPress={navigateToOONI}
        type={checked ? Button.types.PRIMARY : Button.types.DISABLED}
      />
    </View>
  );
};

export default IntroOONI;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.mirage,
    padding: moderateScale(constants.PADDING),
  },
  checkItem: {
    borderRadius: moderateScale(8),
    backgroundColor: colors.charade,
    width: '100%',
    padding: moderateScale(constants.PADDING),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: moderateVerticalScale(constants.PADDING),
  },
  body: {
    ...typo.ancillary,
    flex: 1,
    color: colors.white,
    paddingEnd: moderateScale(constants.PADDING),
    fontSize: moderateScale(14),
  },
  subTitle: {
    ...typo.ancillary,
    color: colors.silver,
    fontSize: moderateScale(14),
    paddingBottom: moderateScale(constants.PADDING * 2),
  },
});

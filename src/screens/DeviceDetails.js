import React, { useMemo, useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Switch,
  RefreshControl,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import {
  getMainDeviceId,
  makeGetEntity,
  makeGetEpicIds,
  makeGetIsEpicLoading,
} from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation, CommonActions } from '@react-navigation/native';
import {
  updateDevice,
  updateDeviceStatus,
  setMainDevice,
  getDevice,
  removeDevice,
  getDevices,
} from '../actions';
import moment from 'moment';
import { NameDialog, LoadingView } from '../components';
import { PERFECT_DIAG_CODE } from '../Constants';

const ActionButton = ({
  source,
  onPress,
  title,
  disabled,
  color = colors.main_color,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.action} disabled={disabled}>
    <View style={[styles.imgCont, { backgroundColor: color }]}>
      <Image style={styles.icon} source={source} />
    </View>
    <Text style={[styles.actionTitle, { color }]}>{title}</Text>
  </TouchableOpacity>
);

const SectionItem = ({ title, body, subTitle, onPress, renderRight }) => (
  <TouchableOpacity
    style={styles.mainSection}
    disabled={!onPress}
    onPress={onPress}>
    <View style={styles.row}>
      <Text style={styles.title}>{title}</Text>
      {renderRight?.() ?? (
        <View style={styles.subRow}>
          <Text style={styles.body}>{body}</Text>
          {onPress && (
            <Icon name={'chevron-right'} size={24} color={colors.silver} />
          )}
        </View>
      )}
    </View>
    {subTitle && (
      <Text style={[styles.body, { paddingBottom: constants.PADDING }]}>
        {subTitle}
      </Text>
    )}
  </TouchableOpacity>
);

const DeviceDetails = ({ route }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const entity_id = route?.params?.deviceId;
  const getEntity = useMemo(makeGetEntity, []);
  const getIsLoading = useMemo(makeGetIsEpicLoading, []);
  const device = useSelector(state =>
    getEntity(state, { entity: 'devices', entity_id })
  );
  const isLoading = useSelector(state =>
    getIsLoading(state, { epicName: 'getDevice' })
  );

  const isRemoving = useSelector(state =>
    getIsLoading(state, { epicName: 'removeDevice' })
  );

  useEffect(() => {
    navigation.setOptions({ headerTitle: device?.name });
  }, []);

  const [notify, setNotify] = useState(device?.permission_to_notify_owner);
  const [show, setShow] = useState(false);
  const mainDeviceId = useSelector(getMainDeviceId);
  const [main, setMain] = useState(mainDeviceId === device?.id);
  const isDeviceUp = device?.status !== 0;

  const navigate = (screen, params = {}) => {
    navigation.navigate(screen, params);
  };

  const onRefresh = () => {
    dispatch(getDevice({ id: entity_id }));
  };

  const onValueChange = status => {
    setNotify(status);
    dispatch(
      updateDevice({
        id: device?.id,
        permission_to_notify_owner: status,
      })
    );
  };

  const onChangeDevice = () => {
    setMain(!main);
    dispatch(setMainDevice(device?.id));
  };

  const reboot = () => {
    dispatch(
      updateDeviceStatus({
        id: device?.id,
        command: 'reboot',
      })
    );
  };

  const unclaimDevice = () => {
    Alert.alert(
      i18n.t('remove_device'),
      i18n.t('are_you_sure_you_want_to_remove_this_device'),
      [
        {
          text: i18n.t('cancel'),
        },
        {
          text: i18n.t('remove_device'),
          onPress: async () => {
            const resp = await dispatch(removeDevice({ id: device?.id }));
            if (resp.success) {
              dispatch(getDevices());
              if (entity_id === mainDeviceId) {
                dispatch(setMainDevice(null));
              }
              navigation.goBack();
            }
          },
          style: 'destructive',
        },
      ]
    );
  };

  const renderSeparator = () => <View style={styles.separator} />;

  const renderRestart = () => (
    <>
      {renderSeparator()}
      <ActionButton
        title={i18n.t('restart_the_pod')}
        source={require('../assets/restart.png')}
        onPress={reboot}
      />
    </>
  );

  const renderRemove = () => (
    <>
      {renderSeparator()}
      <ActionButton
        title={i18n.t('remove_device')}
        source={require('../assets/key.png')}
        onPress={unclaimDevice}
        color={colors.error}
      />
      {renderSeparator()}
    </>
  );
  return (
    <LoadingView
      isLoading={isRemoving}
      containerStyle={styles.container}
      backgroundColor={colors.sub_bg}
      indicatorColor={colors.main_color}>
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>
            {i18n.t('about').toUpperCase()}
          </Text>
          {renderSeparator()}
          <SectionItem
            title={i18n.t('name')}
            body={device?.name}
            onPress={() => setShow(true)}
          />
          {renderSeparator()}
          <SectionItem
            title={i18n.t('last_heartbeat')}
            body={moment(device?.last_seen).fromNow()}
            subTitle={i18n.t('the_last_time_app_pod')}
          />
          {renderSeparator()}
          <SectionItem
            title={i18n.t('software_version')}
            body={device?.software_version}
          />
          {renderSeparator()}
          <SectionItem
            title={i18n.t('set_as_main_display')}
            subTitle={i18n.t('show_information')}
            renderRight={() => (
              <Switch
                value={main}
                onValueChange={onChangeDevice}
                trackColor={{ true: colors.main_color }}
                disabled={main}
              />
            )}
          />
          {renderSeparator()}
        </View>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>
            {i18n.t('notifications').toUpperCase()}
          </Text>
          {renderSeparator()}
          <SectionItem
            title={i18n.t('email_me_when_offline')}
            subTitle={i18n.t('when_the_pod_is_not')}
            renderRight={() => (
              <Switch
                value={notify}
                onValueChange={onValueChange}
                trackColor={{ true: colors.main_color }}
              />
            )}
          />
          {renderSeparator()}
        </View>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>
            {i18n.t('health').toUpperCase()}
          </Text>
          {renderSeparator()}
          <SectionItem
            title={i18n.t('pod_health')}
            body={
              (device?.diag_code | 32) === PERFECT_DIAG_CODE
                ? i18n.t('good')
                : i18n.t('needs_attention')
            }
            onPress={() =>
              navigate('Diagnostics', { diag_code: device?.diag_code })
            }
          />
          {isDeviceUp && renderRestart()}
          {renderRemove()}
        </View>
        <NameDialog
          device={device}
          visible={show}
          onClose={() => setShow(false)}
        />
      </ScrollView>
    </LoadingView>
  );
};

export default DeviceDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  section: {
    paddingTop: constants.PADDING,
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: 12,
    color: colors.main_color,
    fontWeight: 'bold',
    paddingHorizontal: constants.PADDING,
    marginBottom: constants.PADDING,
  },
  separator: {
    width: '98%',
    height: 0.4,
    backgroundColor: colors.manatee,
    alignSelf: 'center',
  },
  action: {
    flexDirection: 'row',
    paddingHorizontal: constants.PADDING,
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.chradeLight,
  },
  actionTitle: {
    ...typo.primary,
    fontSize: 16,
    color: colors.main_color,
    paddingStart: constants.PADDING,
  },
  imgCont: {
    width: 30,
    height: 30,
    borderRadius: 8,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
  },
  mainSection: {
    backgroundColor: colors.chradeLight,
    paddingHorizontal: constants.PADDING,
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    minHeight: 55,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    ...typo.secondary,
    color: colors.white,
    fontWeight: 'bold',
  },
  body: {
    ...typo.ancillary,
    color: colors.manatee,
  },
  subRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

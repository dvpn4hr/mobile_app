import i18n from 'i18n-js';
import React, { useMemo, useState, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Switch,
  FlatList,
  TouchableOpacity,
  Image,
  RefreshControl,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { colors, constants, statusMap, typo } from '../common';
import {
  getDeviceIssues,
  getMainDeviceId,
  makeGetEntity,
  makeGetEpicIds,
  makeGetIsEpicLoading,
} from '../selectors';
import {
  updateDeviceStatus,
  getFriends,
  getDeviceDiagnostics,
  getDevice,
} from '../actions';
import { FriendCard, LoadingView } from '../components';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import { showArticle } from 'react-native-zendesk';

const ActionButton = ({
  source,
  onPress,
  title,
  disabled,
  color = colors.main_color,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.action} disabled={disabled}>
    <View style={[styles.imgCont, { backgroundColor: color }]}>
      <Image style={styles.icon} source={source} />
    </View>
    <Text style={[styles.actionTitle, { color }]}>{title}</Text>
  </TouchableOpacity>
);

const friendEpic = { epicName: 'getFriends' };
const diagnosticsEpic = { epicName: 'getDeviceDiagnostics' };
const removeDeviceEpic = { epicName: 'removeDevice' };
const Device = ({ route }) => {
  const entity_id = route?.params?.deviceId;
  const getEntity = useMemo(makeGetEntity, []);
  const getEpicIds = useMemo(makeGetEpicIds, []);
  const getIsLoading = useMemo(makeGetIsEpicLoading, []);
  const navigation = useNavigation();
  const friends = useSelector(state => getEpicIds(state, friendEpic));
  const isLoading = useSelector(
    state =>
      getIsLoading(state, friendEpic) ||
      getIsLoading(state, diagnosticsEpic) ||
      getIsLoading(state, removeDeviceEpic)
  );
  const dispatch = useDispatch();
  const device = useSelector(state =>
    getEntity(state, { entity: 'devices', entity_id })
  );
  const deviceStatus = useMemo(
    () => statusMap?.[device?.status]?.(device?.diag_code),
    [device?.status, device?.diag_code]
  );
  const issues = useSelector(getDeviceIssues);
  const isMultipleIssues = issues?.length > 1;
  const isDeviceUp = device?.status !== 0;
  const isPaused = device?.status === 1;
  const mainDeviceId = useSelector(getMainDeviceId);
  const isAvailable = isDeviceUp && !isPaused && deviceStatus?.canGrant;
  const [available, setAvailable] = useState(isAvailable);

  useEffect(() => {
    dispatch(getFriends());
  }, []);

  useEffect(() => {
    if (deviceStatus?.error) {
      dispatch(getDeviceDiagnostics({ device_code: device?.diag_code }));
    }
  }, [deviceStatus?.error]);

  const onValueChange = status => {
    setAvailable(status);
    dispatch(
      updateDeviceStatus({
        id: device?.id,
        command: status ? 'start' : 'stop',
      })
    );
  };

  const navigate = screen => {
    navigation.navigate(screen, { deviceId: device?.id });
  };

  const onRefresh = () => {
    dispatch(getFriends());
    dispatch(
      getDeviceDiagnostics({
        device_code: device?.diag_code,
      })
    );
    dispatch(getDevice({ id: entity_id }));
  };

  const renderItem = ({ item }) => <FriendCard entity_id={item} />;

  const renderSeparator = () => <View style={styles.separator} />;

  const renderFooter = () => (
    <>
      <View style={styles.separator} />
      <ActionButton
        title={i18n.t('grant_access')}
        source={require('../assets/key.png')}
        onPress={() => navigate('GrantAccess')}
      />
      <View style={styles.separator} />
    </>
  );

  const renderHeader = () => (
    <>
      <View style={styles.mainContent}>
        <View style={styles.row}>
          <Text style={styles.title}>{i18n.t('status')}</Text>
          <View style={styles.sub_row}>
            <View
              style={[styles.point, { backgroundColor: deviceStatus?.color }]}
            />
            <Text style={styles.status}>{deviceStatus?.title}</Text>
          </View>
        </View>
        {deviceStatus?.error && renderError()}
        <View style={styles.separator} />
        <View style={styles.row}>
          <Text style={styles.title}>{i18n.t('vpn_availability')}</Text>
          <Switch
            value={available}
            onValueChange={onValueChange}
            trackColor={{ true: colors.main_color }}
            disabled={device?.status === 0}
          />
        </View>
        <View style={[styles.row, { justifyContent: 'flex-start' }]}>
          <Text style={styles.status}>{deviceStatus?.message} </Text>
          {deviceStatus?.error && (
            <Text onPress={onRefresh}>
              <Text style={styles.retry}>{i18n.t('tap_to_retry')}</Text>
            </Text>
          )}
        </View>
      </View>
      {friends?.length > 0 && (
        <View style={styles.access}>
          <Text style={styles.sectionTitle}>
            {i18n.t('people_with_access').toUpperCase()}
          </Text>
          {renderSeparator()}
        </View>
      )}
    </>
  );

  const navigateToDiagnostics = () => {
    if (isMultipleIssues) {
      navigation.navigate('Diagnostics', {
        diag_code: device?.diag_code,
      });
    } else {
      const diag = issues?.[0];
      showArticle({
        articleId: diag?.url?.match(/([\d]+)/)?.[0],
        hideContactSupport: false,
      });
    }
  };

  const renderError = () => (
    <TouchableOpacity
      onPress={navigateToDiagnostics}
      style={styles.errorContainer}>
      <View style={styles.details}>
        <Text style={styles.sectionTitle}>
          {i18n.t(isMultipleIssues ? 'multiple_issues' : 'help_article')}
        </Text>
        <Text style={styles.desc} numberOfLines={1} ellipsizeMode="tail">
          {isMultipleIssues
            ? i18n.t('review_pod_health')
            : issues?.[0]?.description}
        </Text>
      </View>
      <Icon
        name={isMultipleIssues ? 'chevron-forward' : 'share-outline'}
        size={24}
        color={colors.white}
      />
    </TouchableOpacity>
  );

  return (
    <LoadingView
      isLoading={isLoading}
      containerStyle={styles.container}
      backgroundColor={colors.sub_bg}
      indicatorColor={colors.main_color}>
      <FlatList
        data={friends}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
        ListHeaderComponent={renderHeader}
        ListFooterComponent={renderFooter}
        showsVerticalScrollIndicator={false}
        style={styles.section}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
      />
    </LoadingView>
  );
};

export default Device;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    paddingHorizontal: constants.PADDING,
  },
  mainContent: {
    width: '100%',
    borderRadius: 8,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: constants.PADDING / 2,
  },
  sub_row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...typo.secondary,
    color: colors.white,
    fontSize: 17,
  },
  point: {
    marginEnd: constants.PADDING / 2,
    width: 10,
    height: 10,
    borderRadius: 10,
  },
  status: {
    ...typo.ancillary,
    color: colors.manatee,
    fontSize: 16,
    lineHeight: 24,
  },
  separator: {
    width: '100%',
    height: 0.5,
    backgroundColor: colors.manatee,
    marginVertical: constants.PADDING / 2,
  },
  section: {
    paddingTop: constants.PADDING,
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: 12,
    color: colors.main_color,
    fontWeight: 'bold',
  },
  action: {
    flexDirection: 'row',
    paddingVertical: 14,
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  actionTitle: {
    ...typo.primary,
    fontSize: 16,
    color: colors.main_color,
    paddingStart: constants.PADDING,
  },
  imgCont: {
    width: 30,
    height: 30,
    borderRadius: 8,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 20,
    height: 10,
  },
  errorContainer: {
    width: '100%',
    height: 70,
    backgroundColor: colors.chradeLight,
    marginVertical: constants.PADDING,
    borderRadius: 8,
    padding: constants.PADDING,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  details: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingEnd: 8,
  },
  desc: {
    ...typo.ancillary,
    paddingTop: 8,
    color: colors.white,
  },
  access: {
    paddingTop: constants.PADDING / 2,
  },
  retry: {
    ...typo.primary,
    color: colors.main_color,
    fontWeight: 'bold',
  },
});

import i18n from '../i18n';
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  RefreshControl,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { getDevices } from '../actions';
import { colors, constants, typo } from '../common';
import { DeviceCard, EmptyView, Button, LoadingView } from '../components';
import {
  makeGetEpicIds,
  getOONISetup,
  makeGetIsEpicLoading,
  getOONIWorking,
  makeGetMainDevice,
} from '../selectors';

const Tile = ({ name, description, disabled, onPress, source }) => (
  <TouchableOpacity style={styles.tile} onPress={onPress}>
    <View style={styles.status}>
      <View style={disabled ? styles.disabled : styles.working} />
      <Text style={styles.disabledText}>
        {i18n.t(disabled ? 'disabled' : 'working')}
      </Text>
    </View>
    <Image source={source} style={styles.image} />
    <View>
      <Text style={styles.name}>{name}</Text>
      <Text style={styles.description}>{description}</Text>
    </View>
  </TouchableOpacity>
);
class Devices extends Component {
  componentDidMount() {
    const { mainDevice, navigation } = this.props;
    navigation.setOptions({
      showAvatar: true,
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { mainDevice, navigation } = this.props;
    if (mainDevice?.name) {
      navigation.setOptions({
        headerTitle: mainDevice?.name,
        showAvatar: true,
      });
    }
  }

  getDevices = () => {
    const { getDevices } = this.props;
    getDevices();
  };

  claimDevice = () => {
    const { navigation } = this.props;
    navigation.navigate('Setup');
  };

  renderSeparator = () => <View style={styles.separator} />;

  renderEmpty = () => {
    const { navigation } = this.props;
    return (
      <EmptyView
        source={require('../assets/wepn_device.png')}
        title={i18n.t('hello')}
        message={i18n.t('ready_to_connect_wepn')}
        actions={[
          {
            title: i18n.t('connect_a_pod'),
            onPress: this.claimDevice,
          },
          {
            title: i18n.t('check_compatibility'),
            onPress: () => navigation.navigate('Compatibility'),
            type: Button.types.TERTIARY,
            underline: true,
          },
        ]}
      />
    );
  };

  navigateToOONI = () => {
    const { navigation, isOONISetup } = this.props;
    if (isOONISetup) {
      navigation.navigate('OONIStack', {
        screen: 'OONI',
      });
    } else {
      navigation.navigate('OONIStack');
    }
  };

  render() {
    const { isLoading, devices, mainDevice, isOONIWorking } = this.props;
    return (
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={this.getDevices} />
        }
        contentContainerStyle={styles.contentContainer}>
        <LoadingView
          isLoading={isLoading}
          containerStyle={styles.container}
          indicatorColor={colors.main_color}
          backgroundColor={colors.sub_bg}>
          {devices?.length > 0 ? (
            <>
              <DeviceCard
                entity_id={mainDevice?.id}
                navigation={this.props.navigation}
              />
              <Tile
                name={i18n.t('ooni')}
                source={require('../assets/ooni.png')}
                onPress={this.navigateToOONI}
                title={i18n.t('ooni')}
                description={i18n.t(
                  isOONIWorking ? 'tap_to_stop' : 'tap_to_start'
                )}
                disabled={!isOONIWorking}
              />
            </>
          ) : (
            this.renderEmpty()
          )}
        </LoadingView>
      </ScrollView>
    );
  }
}

const makeMapStateToProps = () => {
  const getIsLoading = makeGetIsEpicLoading();
  const getIds = makeGetEpicIds();
  const epicName = 'getDevices';
  const getMainDevice = makeGetMainDevice();
  return state => ({
    devices: getIds(state, { epicName }),
    isLoading:
      getIsLoading(state, { epicName }) ||
      getIsLoading(state, { epicName: 'getFriends' }) ||
      getIsLoading(state, { epicName: 'getDiagnostics' }),
    isOONISetup: getOONISetup(state),
    isOONIWorking: getOONIWorking(state),
    mainDevice: getMainDevice(state),
  });
};

const mapDispatchToProps = {
  getDevices,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(Devices);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  contentContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },
  separator: {
    height: 0.5,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: colors.gray,
  },
  tile: {
    width: '48%',
    height: 164,
    backgroundColor: colors.charade,
    marginVertical: constants.PADDING / 2,
    marginHorizontal: constants.PADDING,
    borderRadius: 12,
    padding: constants.PADDING,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  status: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabled: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: colors.gray,
    marginEnd: constants.PADDING / 2,
  },
  working: {
    width: 10,
    height: 10,
    borderRadius: 10,
    backgroundColor: colors.green,
    marginEnd: constants.PADDING / 2,
  },
  disabledText: {
    ...typo.ancillary,
    color: colors.white,
  },
  image: {
    width: 36,
    height: 36,
    resizeMode: 'contain',
  },
  name: {
    ...typo.primary,
    color: colors.white,
  },
  description: {
    ...typo.ancillary,
    color: colors.white,
    lineHeight: 24,
  },
});

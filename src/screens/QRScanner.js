import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { colors, typo } from '../common';
import Snackbar from 'react-native-snackbar';
import { useIsFocused } from '@react-navigation/core';
import BarcodeMask from 'react-native-barcode-mask';
import { parse } from 'search-params';
import i18n from '../i18n';
import { connect } from 'react-redux';
import { addDevice, setDeviceKey } from '../actions';
import { LoadingView } from '../components';
import { useNavigation } from '@react-navigation/native';

const QRScanner = props => {
  const isFocused = useIsFocused();
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const { k, s, pk } = props?.route?.params || {};

  const showError = message => {
    Snackbar.show({
      text: message,
      duration: Snackbar.LENGTH_SHORT,
      backgroundColor: colors.error,
    });
  };

  useEffect(() => {
    if (k && s && pk) {
      setIsLoading(true);
      onQRRead(k, s, pk);
    }
  }, [k, s, pk]);

  const onQRRead = (device_key, serial_number, PK) => {
    const { addDevice, setDeviceKey } = props;
    if (PK) {
      setDeviceKey(PK);
    } else {
      showError(i18n.t('please_check_qr_code'));
      navigation.goBack();
      return;
    }

    addDevice({
      device_name: 'WEPN Device',
      device_key,
      serial_number,
    })
      .then(res => {
        if (res.success) {
          navigation.navigate('ClaimStack');
        } else {
          showError(res?.error?.error_description || res?.error?.detail);
          navigation.goBack();
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onBarCodeRead = event => {
    setIsLoading(true);
    if (event.data) {
      const data = parse(event.data);
      if (!data.k || !data.s) {
        showError(i18n.t('this_qr_code_is_invalid'));
        setIsLoading(false);
        return;
      }
      onQRRead(data.k, data.s, data.pk);
    } else {
      setIsLoading(false);
      showError(i18n.t('this_qr_code_is_invalid'));
    }
  };

  return isFocused ? (
    <LoadingView isLoading={isLoading} unmount>
      <RNCamera
        style={styles.camera}
        aspect={RNCamera.Constants.AutoFocus}
        onBarCodeRead={onBarCodeRead}
        captureAudio={false}>
        <BarcodeMask showAnimatedLine={false} useNativeDriver />
        <View style={styles.holder}>
          <Text style={styles.hold}>{i18n.t('hold_camera_over')}</Text>
        </View>
      </RNCamera>
    </LoadingView>
  ) : (
    <View />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  addDevice,
  setDeviceKey,
};

export default connect(mapStateToProps, mapDispatchToProps)(QRScanner);

const styles = StyleSheet.create({
  camera: {
    flex: 1,
  },
  holder: {
    position: 'absolute',
    top: '75%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hold: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
    width: '65%',
    textAlign: 'center',
  },
});

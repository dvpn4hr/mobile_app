import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import { Button } from '../components';
import i18n from '../i18n';
import { setAccessData } from '../actions';
import { languages } from '../Constants';
import RNShare from 'react-native-share';
import { Manuals } from '../assets/Manuals';

const ActionButton = ({
  source,
  onPress,
  title,
  disabled,
  color = colors.main_color,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.action} disabled={disabled}>
    <View style={[styles.imgCont, { backgroundColor: color }]}>
      <Image style={styles.icon} source={source} />
    </View>
    <Text style={[styles.actionTitle, { color }]}>{title}</Text>
  </TouchableOpacity>
);

class ChooseLanguage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      language: 'en',
    };
  }

  componentDidMount() {
    const { navigation } = this.props;

    navigation.setOptions({
      headerRight: () => (
        <Button
          title={i18n.t('done')}
          type={Button.types.TERTIARY}
          style={{
            width: 50,
            alignSelf: 'flex-end',
            marginHorizontal: constants.PADDING,
          }}
          onPress={this.setData}
        />
      ),
    });
  }

  setData = () => {
    const { navigation, setAccessData } = this.props;
    const { language } = this.state;

    setAccessData({ language });
    navigation.goBack();
  };

  setLanguage = language => {
    this.setState({ language });
  };

  shareManual = () => {
    RNShare.open({
      title: i18n.t('share_manuals'),
      urls: Manuals,
      failOnCancel: false,
    });
  };

  render() {
    const { language } = this.state;
    return (
      <View style={styles.container}>
        <Text style={styles.grantAccess}>{i18n.t('choose_language')}</Text>
        <Text style={styles.subText}>{i18n.t('we_will_send_instruction')}</Text>
        {languages?.map(({ label, value }) => (
          <TouchableOpacity
            style={styles.language}
            onPress={() => this.setLanguage(value)}
            activeOpacity={1}>
            <Text style={styles.title}>{label}</Text>
            {language === value && (
              <Image
                style={styles.check}
                source={require('../assets/check.png')}
              />
            )}
          </TouchableOpacity>
        ))}
        <View style={styles.actions}>
          <ActionButton
            source={require('../assets/eye.png')}
            title={i18n.t('preview_instructions')}
            type={Button.types.TERTIARY}
            textStyle={{ color: colors.main_color }}
            style={{ alignSelf: 'flex-start' }}
            onPress={this.shareManual}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  setAccessData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ChooseLanguage);

const styles = StyleSheet.create({
  check: { width: 18, height: 13 },
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    paddingTop: constants.PADDING * 3,
  },
  grantAccess: {
    ...typo.primary,
    fontSize: 28,
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
  subText: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: 16,
  },
  title: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
  },
  language: {
    flexDirection: 'row',
    width: '100%',
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: colors.abbey,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actions: {
    borderBottomWidth: 1,
    borderBottomColor: colors.abbey,
  },
  action: {
    flexDirection: 'row',
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  actionTitle: {
    ...typo.primary,
    fontSize: 16,
    color: colors.main_color,
    paddingStart: constants.PADDING,
  },
  imgCont: {
    width: 30,
    height: 30,
    borderRadius: 8,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 25,
    height: 15,
    resizeMode: 'contain',
  },
});

import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useDispatch } from 'react-redux';
import { switchEnvironment, signUserOut } from '../actions';
import { useSelector } from 'react-redux';
import { getIsProduction, getUserData } from '../selectors';
import { useEffect } from 'react';
import { colors } from '../common';
import RNRestart from 'react-native-restart';
import isEmpty from 'lodash/isEmpty';

const SwitchEnv = () => {
  const dispatch = useDispatch();
  const userData = useSelector(getUserData);
  const isProduction = useSelector(getIsProduction);

  useEffect(() => {
    dispatch(switchEnvironment());
    if (!isEmpty(userData)) {
      dispatch(
        signUserOut({
          token: userData?.access_token,
        })
      );
    }
    setTimeout(() => {
      RNRestart.Restart();
    }, 5000);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        Switching to {isProduction ? 'Production' : 'Staging'} ...
      </Text>
    </View>
  );
};

export default SwitchEnv;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 20,
    color: colors.white,
  },
});

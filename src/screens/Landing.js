import { Image, StatusBar, StyleSheet, Text, View } from 'react-native';
import React, { useCallback, useState } from 'react';
import { colors, typo } from '../common';
import Swiper from 'react-native-swiper';
import i18n from '../i18n';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { Button } from '../components';
import { useNavigation } from '@react-navigation/core';

const slides = [
  {
    title: i18n.t('slides.1.title'),
    description: i18n.t('slides.1.description'),
    image: require('../assets/slide_1.png'),
  },
  {
    title: i18n.t('slides.2.title'),
    description: i18n.t('slides.2.description'),
    image: require('../assets/slide_2.png'),
  },
  {
    title: i18n.t('slides.3.title'),
    description: i18n.t('slides.3.description'),
    image: require('../assets/slide_3.png'),
  },
  {
    title: i18n.t('slides.4.title'),
    description: i18n.t('slides.4.description'),
    image: require('../assets/slide_4.png'),
  },
];

const Landing = () => {
  const ref = React.useRef(null);
  const { reset } = useNavigation();

  const [index, setIndex] = useState(0);

  onIndexChanged = useCallback(index => {
    setIndex(index);
  }, []);

  const updateDescription = description => {
    if (Array.isArray(description)) {
      return description.map((item, index) => `• ${item} `).join('\n');
    }
    return description;
  };

  const goToNextSlide = () => {
    if (index === 3) {
      goToIntro();
      return;
    }
    ref.current.scrollBy(1);
  };

  goToIntro = () => {
    reset({
      index: 0,
      routes: [{ name: 'Intro' }],
    });
  };

  const renderSlide = (slide, index) => {
    return (
      <View style={styles.slide}>
        <Image source={slide.image} style={styles.image} />
        <Text style={styles.title}>{slide.title}</Text>
        <Text style={styles.description}>
          {updateDescription(slide.description)}
        </Text>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor={colors.white} />
      <Swiper
        ref={ref}
        index={index}
        loadMinimal
        loop={false}
        style={styles.wrapper}
        dotStyle={styles.dot}
        activeDotStyle={styles.activeDot}
        onIndexChanged={onIndexChanged}
        removeClippedSubviews
        automaticallyAdjustContentInsets>
        {slides.map(renderSlide)}
      </Swiper>
      <View style={styles.actions}>
        <Button
          title={i18n.t('continue')}
          type={Button.types.PRIMARY}
          style={styles.continue}
          onPress={goToNextSlide}
        />
        {index > 2 ? (
          <View style={styles.placeholder} />
        ) : (
          <Button
            title={i18n.t('skip')}
            type={Button.types.TERTIARY}
            textStyle={styles.skip}
            onPress={goToIntro}
          />
        )}
      </View>
    </View>
  );
};

export default Landing;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
    paddingVertical: moderateVerticalScale(40),
  },
  wrapper: {},
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: moderateScale(288),
    height: moderateScale(288),
    resizeMode: 'contain',
  },
  title: {
    ...typo.primary,
    fontSize: moderateScale(28),
    fontWeight: 'bold',
    color: colors.chradeLight,
    marginVertical: moderateScale(12),
  },
  description: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.chradeLight,
    textAlign: 'left',
    paddingHorizontal: moderateScale(38),
  },
  actions: {
    paddingHorizontal: moderateScale(38),
    width: '100%',
  },
  continue: {
    backgroundColor: colors.mirage,
    marginHorizontal: moderateVerticalScale(12),
  },
  skip: {
    marginTop: moderateVerticalScale(12),
    color: colors.mirage,
  },
  dot: {
    backgroundColor: colors.mercurySolid,
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(4),
  },
  activeDot: {
    backgroundColor: colors.casablanca,
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(4),
  },
  actionText: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.chradeLight,
    textAlign: 'left',
    paddingHorizontal: moderateScale(30),
    textDecorationLine: 'underline',
    alignSelf: 'flex-start',
  },
  placeholder: {
    height: 50,
  },
});

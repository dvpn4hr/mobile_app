import i18n from 'i18n-js';
import React, { Component } from 'react';
import { Linking, View, Text, StyleSheet } from 'react-native';
import { showHelpCenter } from 'react-native-zendesk';
import { colors, constants, links } from '../common';
import { Button } from '../components';

class Help extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Button
          onPress={() => navigation.navigate('Tutorial')}
          title={i18n.t('watch_video_tutorial')}
        />
        <Button
          onPress={() => navigation.navigate('WalkThrough')}
          title={i18n.t('tutorial')}
        />
        <Button
          onPress={() =>
            showHelpCenter({
              hideContactSupport: true,
              tags: [],
            })
          }
          title={i18n.t('support')}
        />
        <Button
          onPress={() => navigation.navigate('Ticket')}
          title={i18n.t('report_issue')}
        />
        <Button
          onPress={() => navigation.navigate('Diagnostics')}
          title={i18n.t('device_troubleshooting')}
        />
        <Button
          onPress={() => Linking.openURL(links.twitter)}
          title={i18n.t('follow_us_on_twitter')}
        />
      </View>
    );
  }
}

export default Help;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingVertical: constants.PADDING * 5,
    backgroundColor: colors.sub_bg,
  },
});

import React, { useRef, useState } from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import YouTubePlayer from 'react-native-youtube-iframe';
import { colors } from '../common';
const deviceWidth = Dimensions.get('window').width;

export default Tutorial = () => {
  const playerRef = useRef(null);
  const [playing, setPlaying] = useState(true);
  return (
    <View style={styles.container}>
      <YouTubePlayer
        ref={playerRef}
        height={350}
        width={deviceWidth}
        videoId={'Vj-GSDSKPHw'}
        play={playing}
        onChangeState={event => console.log(event)}
        onReady={() => console.log('ready')}
        onError={e => console.log(e)}
        onPlaybackQualityChange={q => console.log(q)}
        volume={50}
        playbackRate={1}
        initialPlayerParams={{
          cc_lang_pref: 'us',
          showClosedCaptions: true,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.sub_bg,
  },
});

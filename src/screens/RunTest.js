import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  FlatList,
  Linking,
} from 'react-native';
import { colors, typo } from '../common';
import i18n from '../i18n';
import { Button, Step } from '../components';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { useCompatibilityProcess } from '../hooks';
import { useNavigation } from '@react-navigation/core';
import { makeGetEpicResult } from '../selectors';
import { useSelector } from 'react-redux';
import Clipboard from '@react-native-clipboard/clipboard';
import Snackbar from 'react-native-snackbar';

const steps = [
  i18n.t('searching_for_upnp_devices'),
  i18n.t('forwarding_port'),
  i18n.t('creating_socket_connection'),
  i18n.t('testing_port_forwarding'),
  i18n.t('getting_test_results'),
];

const RunTest = ({} = {}) => {
  const { step, logs } = useCompatibilityProcess();
  const [success, setSuccess] = useState(null);
  const [running, setRunning] = useState(true);
  const navigation = useNavigation();
  const getResult = makeGetEpicResult();
  const testResult = useSelector(state =>
    getResult(state, { epicName: 'testPortForwarding' })
  );

  useEffect(() => {
    if (step === 5) {
      const inError = logs.some(log => log?.error);
      if (inError) setSuccess(false);
      else setSuccess(true);
      setTimeout(() => {
        setRunning(false);
      }, 1000);
    }
  }, [logs, step]);

  const getStatus = index => {
    if (step === index) return 'inProgress';
    else if (step >= 5) return 'Done';
  };

  const renderStep = ({ item, index }) => (
    <Step
      text={item}
      index={index}
      log={logs[index]}
      status={getStatus(index)}
    />
  );

  const copyToClipboard = () => {
    Clipboard.setString(String(testResult.id));
    Snackbar.show({
      text: i18n.t('copied_to_clipboard'),
      duration: Snackbar.LENGTH_SHORT,
    });
  };

  const renderFooter = () => {
    if (running || success) return null;
    return (
      <View style={styles.footer}>
        <Button
          type={Button.types.SECONDARY}
          onPress={navigation.goBack}
          title={i18n.t('test_again')}
        />
      </View>
    );
  };

  const getHeaderText = () => {
    if (step < 5) return i18n.t('running_test');
    else if (step === 5) {
      result = logs[4]?.error;
      if (result) return i18n.t('test_failed');
      else return i18n.t('test_passed');
    }
  };

  const renderSuccess = () => {
    return (
      <View style={styles.success}>
        <Text style={styles.successText}>{i18n.t('network_compatible')}</Text>
        <View style={styles.footer}>
          <Text style={styles.footerText}>{i18n.t('share_internet')}</Text>
          <Button
            type={Button.types.PRIMARY}
            onPress={() => Linking.openURL('https://shop.we-pn.com/')}
            title={i18n.t('order_now')}
          />
        </View>
      </View>
    );
  };

  const getIcon = () => {
    if (step < 5) return require('../assets/test_run.png');
    else if (step === 5) {
      result = logs[4]?.error;
      if (result) return require('../assets/test_error.png');
      else return require('../assets/test_success.png');
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.runningCont}>
        <Image source={getIcon()} style={styles.comp} resizeMode={'contain'} />
        <Text style={styles.running}>{getHeaderText()}</Text>
        {step === 5 && (
          <View style={styles.copyToClipboard}>
            <Text style={styles.ref}>Ref: #{testResult?.id}</Text>
            <TouchableOpacity onPress={copyToClipboard}>
              <Icon
                name="ios-copy-outline"
                size={24}
                color={colors.blue_chill}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
      {running || !success ? (
        <FlatList
          data={steps.filter((_, index) => index <= step)}
          keyExtractor={item => item}
          renderItem={renderStep}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          ListHeaderComponent={() => <View style={styles.separator} />}
          ListFooterComponent={() => renderFooter()}
          style={styles.progress}
        />
      ) : (
        renderSuccess()
      )}
    </View>
  );
};

export default RunTest;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.charade,
    paddingTop: moderateVerticalScale(62),
  },
  running: {
    ...typo.primary,
    fontSize: moderateScale(16),
    color: colors.white,
    textAlign: 'center',
    paddingBottom: moderateVerticalScale(16),
  },
  runningCont: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: moderateScale(24),
  },
  progress: {
    flex: 1,
    width: '100%',
    height: '100%',
    paddingHorizontal: moderateScale(24),
  },
  ref: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.blue_chill,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  comp: {
    width: moderateScale(190),
    height: moderateVerticalScale(82),
    marginBottom: moderateVerticalScale(32),
  },
  separator: {
    width: '100%',
    height: moderateVerticalScale(16),
  },
  copyToClipboard: {
    width: moderateScale(170),
    height: moderateVerticalScale(40),
    borderRadius: moderateScale(8),
    backgroundColor: colors.colorWithAlpha('blue_chill', 0.25),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: moderateScale(16),
    marginBottom: moderateVerticalScale(8),
  },
  success: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  successText: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.white,
    textAlign: 'center',
    paddingVertical: moderateVerticalScale(16),
    width: '70%',
    lineHeight: moderateVerticalScale(24),
  },
  footer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: moderateScale(24),
    paddingVertical: moderateVerticalScale(16),
  },
  footerText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.white,
    textAlign: 'center',
    paddingBottom: moderateVerticalScale(16),
  },
});

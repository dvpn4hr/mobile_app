import i18n from '../i18n';
import React, { Component } from 'react';
import { FlatList, StyleSheet, View, RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { getFriends } from '../actions';
import { colors } from '../common';
import { FriendCard, EmptyView, LoadingView } from '../components';
import { makeGetEpicIds, makeGetIsEpicLoading } from '../selectors';

class ManageAccess extends Component {
  componentDidMount() {
    this.getFriends();
  }

  getFriends = () => {
    const { getFriends } = this.props;
    getFriends();
  };

  addFriend = () => {
    const { navigation } = this.props;
    navigation.navigate('AddFriend');
  };

  renderItem = ({ item, index }) => (
    <FriendCard entity_id={item} navigation={this.props.navigation} />
  );

  renderSeparator = () => <View style={styles.separator} />;

  renderEmpty = () => {
    return (
      <EmptyView
        iconName={'account-plus'}
        message={i18n.t('share_device')}
        actions={[
          {
            title: i18n.t('add_new_friend'),
            onPress: this.addFriend,
          },
        ]}
      />
    );
  };

  render() {
    const { isLoading, friends } = this.props;

    return (
      <LoadingView
        isLoading={isLoading}
        containerStyle={styles.container}
        indicatorColor={colors.main_color}
        backgroundColor={colors.sub_bg}
        unmount>
        <FlatList
          data={friends}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.renderSeparator}
          ListEmptyComponent={this.renderEmpty}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
              refreshing={isLoading}
              onRefresh={this.getFriends}
            />
          }
        />
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getIsLoading = makeGetIsEpicLoading();
  const getIds = makeGetEpicIds();
  const epicName = 'getFriends';
  const mapStateToProps = state => ({
    friends: getIds(state, { epicName }),
    isLoading:
      getIsLoading(state, { epicName }) ||
      getIsLoading(state, { epicName: 'deleteFriend' }),
  });
  return mapStateToProps;
};

const mapDispatchToProps = {
  getFriends,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(ManageAccess);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  separator: {
    height: 0.5,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: colors.gray,
  },
});

import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import ValidationComponent from 'react-native-form-validator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import { colors, constants } from '../common';
import { Button, InputText, LoadingView } from '../components';
import i18n from '../i18n';
import { resetUserPassword } from '../actions';
import { makeGetEpicError, makeGetIsEpicLoading } from '../selectors';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
class ForgotPassword extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  labels = {
    email: i18n.t('email'),
  };

  getFieldErrors = fieldName => {
    const { errors } = this.props;
    return (
      this.getErrorsInField(fieldName)[0] ||
      (errors && errors[fieldName] && errors[fieldName][0])
    );
  };

  resetPassword = async () => {
    const isValid = this.validate({
      email: { email: true, required: true },
    });
    if (isValid) {
      const { resetUserPassword, navigation } = this.props;
      const { email } = this.state;

      const { success } = await resetUserPassword({
        email,
      });
      if (success) {
        navigation.navigate('ResetPassword', { email });
      }
    }
  };

  render() {
    const { isLoading } = this.props;
    return (
      <LoadingView
        isLoading={isLoading}
        backgroundColor={colors.charade}
        indicatorColor={colors.main_color}>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps={'always'}>
          <Text style={styles.createAccount}>{i18n.t('forgot_password')}</Text>
          <Text style={styles.text}>{i18n.t('please_enter_email')}</Text>
          <View style={styles.form}>
            <InputText
              leftIcon={'email-outline'}
              placeholder={i18n.t('email')}
              showClear
              onChangeText={email => this.setState({ email: email.trim() })}
              error={this.getFieldErrors('email')}
              autoComplete={'email'}
              keyboardType={'email-address'}
              textContentType={'username'}
              returnKeyType={'done'}
              onSubmitEditing={this.resetPassword}
              autoCapitalize={'none'}
            />
          </View>
          <Button
            type={Button.types.PRIMARY}
            onPress={this.resetPassword}
            title={i18n.t('submit')}
          />
        </KeyboardAwareScrollView>
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getEpicError = makeGetEpicError();
  const getIsEpicLoading = makeGetIsEpicLoading();

  return state => ({
    errors: getEpicError(state, { epicName: 'resetUserPassword' }),
    isLoading: getIsEpicLoading(state, { epicName: 'resetUserPassword' }),
  });
};

const mapDispatchToProps = {
  resetUserPassword,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(ForgotPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    paddingTop: moderateVerticalScale(68),
    padding: moderateScale(32),
  },
  content: {
    flex: 1,
    alignContent: 'center',
  },
  text: {
    fontSize: moderateScale(16),
    color: colors.white,
    paddingVertical: constants.PADDING,
  },
  createAccount: {
    fontSize: moderateScale(24),
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
});

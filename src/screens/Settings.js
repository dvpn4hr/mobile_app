import React, { useMemo } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { colors, constants, SecureStorage, typo } from '../common';
import { Device } from '../components';
import i18n from '../i18n';
import { getIsProduction, getUserToken, makeGetEpicIds } from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation, CommonActions } from '@react-navigation/native';
import { signUserOut } from '../actions';
import VersionNumber from 'react-native-version-number';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import AsyncStorage from '@react-native-async-storage/async-storage';

const devicesEpic = { epicName: 'getDevices' };

const resetAction = CommonActions.reset({
  index: 0,
  routes: [{ name: 'Landing' }],
});

const ActionButton = ({ source, onPress, title, disabled }) => (
  <TouchableOpacity onPress={onPress} style={styles.action} disabled={disabled}>
    <View style={styles.imgCont}>
      <Image style={styles.icon} source={source} resizeMode={'contain'} />
    </View>
    <Text style={styles.actionTitle}>{title}</Text>
  </TouchableOpacity>
);

const MainAction = ({ title, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.mainAction}>
    <Text style={styles.email}>{title}</Text>
    <Icon name={'chevron-right'} size={24} color={colors.white} />
  </TouchableOpacity>
);
const Settings = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const getEpicIds = useMemo(makeGetEpicIds, []);
  const devices = useSelector(state => getEpicIds(state, devicesEpic));
  const token = useSelector(getUserToken);
  const isProduction = useSelector(getIsProduction);
  const navigate = (screen, params = {}) => {
    navigation.navigate(screen, params);
  };

  const logout = () => {
    dispatch(
      signUserOut({
        token: token?.access_token,
      })
    );
    navigation.dispatch(resetAction);
    AsyncStorage.clear();
    SecureStorage.clear();
  };

  const renderItem = ({ item, index }) => <Device entity_id={item} />;

  const renderSeparator = () => <View style={styles.separator} />;

  const renderAddPod = () => (
    <>
      {renderSeparator()}
      <ActionButton
        title={i18n.t('add_a_pod')}
        source={require('../assets/add.png')}
        onPress={() => navigate('Setup')}
      />
      {renderSeparator()}
    </>
  );

  const renderLogout = () => (
    <>
      {renderSeparator()}
      <ActionButton
        title={i18n.t('logout')}
        source={require('../assets/logout.png')}
        onPress={logout}
      />
      {renderSeparator()}
    </>
  );

  return (
    <View style={styles.container}>
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>
          {i18n.t('devices').toUpperCase()}
        </Text>
        <FlatList
          data={devices}
          renderItem={renderItem}
          ItemSeparatorComponent={renderSeparator}
          ListHeaderComponent={renderSeparator}
          ListFooterComponent={renderAddPod}
          showsVerticalScrollIndicator={false}
        />
      </View>
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>
          {i18n.t('account').toUpperCase()}
        </Text>
        {/* {renderSeparator()} */}
        {/* <MainAction title={i18n.t('my_account')} /> */}
        {renderLogout()}
      </View>
      <View style={styles.section}>
        <Text style={styles.sectionTitle}>
          {i18n.t('support').toUpperCase()}
        </Text>
        {renderSeparator()}
        <MainAction
          title={i18n.t('tutorials')}
          onPress={() => navigate('Tutorial')}
        />
        {renderSeparator()}
        <MainAction
          title={i18n.t('check_compatibility')}
          onPress={() =>
            navigate('Compatibility', {
              screen: 'CompatibilityTest',
            })
          }
        />
        {renderSeparator()}
        <MainAction
          title={i18n.t('contact_support')}
          onPress={() => navigate('Ticket')}
        />
        {renderSeparator()}
        <Text style={styles.version}>
          {i18n.t('version')} {VersionNumber.appVersion} (
          {VersionNumber.buildVersion}) {isProduction ? '' : 'DEV'}
        </Text>
      </View>
    </View>
  );
};

export default Settings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  section: {
    paddingTop: moderateVerticalScale(constants.PADDING),
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: moderateScale(12),
    color: colors.main_color,
    fontWeight: 'bold',
    paddingHorizontal: moderateScale(constants.PADDING),
    marginBottom: moderateVerticalScale(constants.PADDING),
  },
  separator: {
    width: '98%',
    height: moderateVerticalScale(0.4),
    backgroundColor: colors.manatee,
    alignSelf: 'center',
  },
  action: {
    flexDirection: 'row',
    paddingHorizontal: moderateScale(constants.PADDING),
    width: '100%',
    height: moderateVerticalScale(55),
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.chradeLight,
  },
  actionTitle: {
    ...typo.primary,
    fontSize: moderateScale(16),
    color: colors.main_color,
    paddingStart: moderateScale(constants.PADDING),
  },
  imgCont: {
    width: moderateScale(30),
    height: moderateScale(30),
    borderRadius: moderateScale(8),
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: moderateScale(16),
    height: moderateScale(16),
  },
  mainAction: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: moderateScale(55),
    backgroundColor: colors.chradeLight,
    paddingVertical: moderateVerticalScale(constants.PADDING / 2),
    paddingHorizontal: moderateScale(constants.PADDING),
  },
  email: {
    color: colors.white,
    fontWeight: 'bold',
  },
  version: {
    ...typo.secondary,
    fontSize: moderateScale(14),
    color: colors.gray,
    padding: moderateScale(constants.PADDING),
  },
});

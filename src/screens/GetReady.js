import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from 'i18n-js';
import { Button } from '../components';
import WifiManager from 'react-native-wifi-reborn';
import { checkLocationPermission } from '../Utils';
import {
  moderateScale,
  moderateVerticalScale,
  verticalScale,
} from 'react-native-size-matters/extend';
import { scale } from 'react-native-size-matters';

class GetReady extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ssid: i18n.t('detecting'),
    };
  }

  async componentDidMount() {
    const granted = await checkLocationPermission();
    if (granted) {
      this.getCurrentWifiSSID();
    } else {
      this.setState({ ssid: i18n.t('denied_by_user') });
    }
  }

  getCurrentWifiSSID = async () => {
    let ssid = '';
    try {
      ssid = await WifiManager.getCurrentWifiSSID();
    } catch (error) {
      ssid = i18n.t('not_detected');
    }
    this.setState({ ssid });
  };

  navigateToDevices = () => {
    const { goToNext } = this.props;

    goToNext();
  };
  render() {
    const { ssid } = this.state;

    return (
      <View style={styles.container}>
        <Text style={styles.title}>{i18n.t('get_ready')}</Text>
        <Text style={styles.body}>{i18n.t('to_setup_your_pod')}</Text>
        <Text style={styles.youNeed}>{i18n.t('you_will_need')}</Text>
        <View style={styles.row}>
          <View style={styles.section}>
            <Image
              style={styles.icon}
              source={require('../assets/router.png')}
            />
            <Text style={styles.sectionTitle}>{i18n.t('internet_router')}</Text>
            <Text style={styles.subTitle}>{ssid}</Text>
          </View>
          <View style={styles.section}>
            <Image
              style={styles.icon}
              source={require('../assets/wepnDevice.png')}
            />
            <Text style={styles.sectionTitle}>{i18n.t('wepn_pod')}</Text>
            <Text style={styles.subTitle}>{i18n.t('and_cables')}</Text>
          </View>
        </View>
        <Button
          title={i18n.t('continue')}
          type={Button.types.PRIMARY}
          onPress={this.navigateToDevices}
        />
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(GetReady);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    padding: moderateVerticalScale(32),
  },
  title: {
    ...typo.primary,
    fontSize: moderateScale(28),
    paddingBottom: moderateVerticalScale(constants.PADDING),
    color: colors.pampas,
  },
  body: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    paddingBottom: moderateVerticalScale(36),
    color: colors.white,
  },
  youNeed: {
    ...typo.primary,
    fontSize: moderateScale(16),
    paddingBottom: moderateVerticalScale(constants.PADDING),
    color: colors.pampas,
    textAlign: 'left',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: moderateVerticalScale(constants.PADDING * 2),
  },
  section: {
    width: '48%',
    height: moderateVerticalScale(120),
    backgroundColor: colors.charade,
    borderRadius: moderateScale(8),
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: constants.PADDING,
  },
  icon: {
    width: scale(64),
    height: moderateVerticalScale(43),
    resizeMode: 'contain',
  },
  sectionTitle: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: moderateScale(14),
  },
  subTitle: {
    ...typo.ancillary,
    color: colors.sliverChalice,
    fontSize: moderateScale(10),
  },
});

import compareVersions from 'compare-versions';
import React, { Component } from 'react';
import {
  Alert,
  Share,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
} from 'react-native';
import { connect } from 'react-redux';
import {
  addFriendHash,
  deleteFriend,
  getAccessLink,
  getFriend,
  removeFriendHash,
  setAccessData,
  getDevice,
  updateFriend,
} from '../actions';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import {
  getAccessData,
  getCachedLinks,
  getMainDeviceId,
  makeGetEntity,
  makeGetIsEpicLoading,
  makeGetMainDevice,
} from '../selectors';
import { Button, LoadingIndicator } from '../components';
import { CommonActions } from '@react-navigation/routers';
import RNShare from 'react-native-share';
import { Manuals } from '../assets/Manuals';
import { StackActions } from '@react-navigation/core';

class AccessKey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accessKey: '',
    };
  }
  async componentDidMount() {
    const { navigation, getDevice, deviceId, getFriend, friend } = this.props;
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={this.shareKey}>
          <Image source={require('../assets/share.png')} style={styles.share} />
        </TouchableOpacity>
      ),
      headerLeft: () => (
        <Button
          title={i18n.t('cancel')}
          type={Button.types.TERTIARY}
          style={styles.cancel}
          onPress={this.navigateToHome}
        />
      ),
    });
    await Promise.all([
      getDevice({ id: deviceId }),
      getFriend({ id: friend?.id }),
    ]);
    this.shareAccessLink();
  }

  navigateToHome = () => {
    const { navigation, route } = this.props;
    const { backToHome } = route?.params;
    if (backToHome) {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [
            {
              name: 'App',
            },
          ],
        })
      );
    } else {
      navigation.goBack();
    }
  };

  getUserDevice = async () => {
    const { device, getAccessLink, friend } = this.props;
    if (compareVersions(device?.software_version, '0.9.26') === -1) {
      Alert.alert(i18n.t('notice'), i18n.t('does_not_support'));
      return;
    }
    const { local_ip_address, local_token } = device;
    const { cert_id } = friend;

    const res = await getAccessLink({
      deviceIP: local_ip_address,
      local_token,
      certname: cert_id,
    });

    return res?.result;
  };

  checkUserDevices = async (expired = false) => {
    const { addFriendHash, friend, deviceId, updateFriend } = this.props;
    const device = await this.getUserDevice();
    if (device) {
      try {
        let resp = JSON.parse(device?.data);
        this.shareLink(resp);
        addFriendHash({ id: friend.id, device_id: deviceId, ...resp });
        updateFriend({ cert_hash: resp.digest, id: friend.id });
      } catch (e) {
        this.alertNotFound(expired);
      }
    } else {
      this.alertNotFound(expired);
    }
  };

  shareLink = device => {
    if (device.link === 'empty') {
      Alert.alert(
        i18n.t('processing'),
        i18n.t('please_wait_while'),
        [
          {
            text: i18n.t('retry'),
            onPress: this.shareAccessLink,
          },
          {
            text: i18n.t('cancel'),
            style: 'destructive',
          },
        ],
        {
          cancellable: true,
        }
      );
      return;
    }
    this.setState({ accessKey: device.link });
  };

  shareKey = () => {
    const { accessKey } = this.state;
    if (accessKey.length) {
      Share.share({
        message: `${accessKey}`,
        url: accessKey,
      });
    } else {
      this.shareLink({ link: 'empty' });
    }
  };

  shareAccessLink = () => {
    const { removeFriendHash, friend } = this.props;
    const cachedFriend = this.checkCachedFriend();
    if (cachedFriend) {
      if (friend.cert_hash === cachedFriend.digest) {
        this.shareLink(cachedFriend);
      } else {
        removeFriendHash(friend.id);
        this.checkUserDevices(true);
      }
    } else {
      this.checkUserDevices(false);
    }
  };

  checkCachedFriend = () => {
    const { cached, friend, deviceId } = this.props;
    return cached.find(
      hash => hash.id === friend.id && hash.device_id === deviceId
    );
  };

  alertNotFound = (expired = false) => {
    Alert.alert(
      i18n.t(expired ? 'expired_key' : 'are_you_away'),
      i18n.t('please_connect')
    );
  };

  shareManual = () => {
    const { navigation } = this.props;

    navigation.navigate('ImageSlider');
  };

  updateEmail = () => {
    const { friend, setAccessData, navigation, route } = this.props;
    setAccessData(friend);
    navigation.dispatch(
      StackActions.push('ContactInfo', {
        showConfirm: true,
      })
    );
  };

  render() {
    const { isLoading, isSharing, friend } = this.props;
    const { accessKey } = this.state;
    const isGenerating = isSharing || isLoading;
    return (
      <View style={styles.container}>
        <View style={styles.keyCont}>
          <View style={styles.header}>
            {isGenerating ? (
              <LoadingIndicator />
            ) : (
              <View style={styles.iconCont}>
                <Image
                  source={require('../assets/key.png')}
                  style={styles.icon}
                />
              </View>
            )}
          </View>
          <Text style={styles.accessKey}>{i18n.t('share_access_key')}</Text>
          <View style={styles.accessView}>
            <Text style={styles.key} numberOfLines={1}>
              {isGenerating
                ? i18n.t('generating_key').toUpperCase()
                : accessKey?.split('')?.join('  ')}
            </Text>
          </View>
          <Button
            title={i18n.t('share_access_key')}
            type={
              isGenerating || accessKey?.length === 0
                ? Button.types.DISABLED
                : Button.types.PRIMARY
            }
            onPress={this.shareKey}
          />
          {!friend.email ? (
            <Button
              title={i18n.t('send_email_instead')}
              onPress={this.updateEmail}
              type={Button.types.TERTIARY}
              underline
            />
          ) : (
            <View />
          )}
        </View>
        <View style={styles.footer}>
          <Text style={styles.sub}>{i18n.t('friend_can_connect')}</Text>
          <Button
            title={i18n.t('share_instructions')}
            type={Button.types.SECONDARY}
            onPress={this.shareManual}
            style={styles.button}
          />
        </View>
      </View>
    );
  }
}

const makeMapStateToProps = () => {
  const getIsLoading = makeGetIsEpicLoading();
  const getFriend = makeGetEntity();
  const getDevice = makeGetMainDevice();
  const mapStateToProps = (state, props) => ({
    cached: getCachedLinks(state),
    isLoading:
      getIsLoading(state, { epicName: 'getFriend' }) ||
      getIsLoading(state, { epicName: 'getDevice' }),
    isSharing: getIsLoading(state, { epicName: 'getAccessLink' }),
    friend: getFriend(state, {
      entity: 'friends',
      entity_id: props?.route?.params?.entity_id,
    }),
    data: getAccessData(state),
    device: getDevice(state),
    deviceId: getMainDeviceId(state),
  });

  return mapStateToProps;
};

const mapDispatchToProps = {
  getFriend,
  deleteFriend,
  getAccessLink,
  addFriendHash,
  removeFriendHash,
  setAccessData,
  getDevice,
  updateFriend,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(AccessKey);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    paddingTop: constants.PADDING * 3,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  accessKey: {
    ...typo.primary,
    fontSize: 21,
    color: colors.white,
  },
  accessView: {
    width: '100%',
    height: 48,
    backgroundColor: colors.colorWithAlpha('blue_chill', 0.05),
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: constants.PADDING,
  },
  key: {
    ...typo.primary,
    color: colors.main_color,
  },
  sub: {
    ...typo.ancillary,
    textAlign: 'left',
    padding: constants.PADDING,
    fontSize: 16,
    color: colors.white,
    textAlign: 'center',
  },
  share: {
    width: 23,
    height: 25,
    resizeMode: 'contain',
    marginHorizontal: constants.PADDING,
  },
  cancel: {
    width: 50,
    alignSelf: 'flex-start',
    marginHorizontal: constants.PADDING,
  },
  header: {
    height: '10%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconCont: {
    width: 50,
    height: 50,
    borderRadius: 10,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 40,
    height: 20,
    resizeMode: 'contain',
  },
  keyCont: {
    flex: 0.7,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  footer: {
    flex: 0.2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: '100%',
  },
});

import React, { Component } from 'react';
import {
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { connect } from 'react-redux';
import VersionNumber from 'react-native-version-number';
import { signUserOut } from '../actions';
import { getUserData } from '../selectors';
import { colors, constants } from '../common';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import i18n from '../i18n';
const Item = ({ title, iconName, onPress }) => (
  <TouchableOpacity style={styles.item} onPress={onPress}>
    <Text style={styles.title}>{title}</Text>
  </TouchableOpacity>
);

const resetAction = CommonActions.reset({
  index: 0,
  routes: [{ name: 'Landing' }],
});
class SideBar extends Component {
  navigate = (key, params = {}) => {
    const { navigation } = this.props;
    navigation.closeDrawer();
    navigation.navigate(key, params);
  };

  logout = () => {
    const { navigation, signUserOut } = this.props;
    signUserOut();
    navigation.dispatch(resetAction);
  };

  render() {
    const { navigation, user } = this.props;

    return (
      <View style={styles.container}>
        <View style={{ flex: 0.1 }} />
        <View style={styles.items}>
          <Item title={i18n.t('home')} onPress={() => this.navigate('App')} />

          <Item
            title={i18n.t('manage_access')}
            onPress={() => this.navigate('ManageAccess')}
          />
          <Item
            title={i18n.t('support')}
            onPress={() => this.navigate('Ticket')}
          />
          <Item
            title={i18n.t('settings')}
            onPress={() => this.navigate('Help')}
          />
        </View>
        <View style={styles.footer}>
          <Text style={styles.version}>
            {i18n.t('version')}: {VersionNumber.appVersion} (
            {VersionNumber.buildVersion})
          </Text>
          <View style={styles.separator} />
          <TouchableOpacity
            style={styles.logoutContainer}
            onPress={this.logout}
          >
            <Text style={styles.logout}>{i18n.t('logout').toUpperCase()}</Text>
            <Text style={styles.email}>{user.email}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  user: getUserData(state),
});

const mapDispatchToProps = {
  signUserOut,
};

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: colors.sub_bg,
    paddingBottom: constants.PADDING * 2,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  items: {
    flex: 1,
    width: '100%',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: constants.PADDING,
    paddingVertical: constants.PADDING / 2,
  },
  title: {
    color: colors.white,
    paddingStart: constants.PADDING,
  },
  version: {
    color: colors.white,
    alignSelf: 'center',
  },
  footer: {
    width: '100%',
    padding: constants.PADDING * 2,
  },
  separator: {
    height: 2,
    width: '100%',
    marginVertical: constants.PADDING,
    backgroundColor: colors.white,
    alignSelf: 'center',
  },
  logoutContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  logout: {
    color: colors.white,
    fontWeight: 'bold',
    marginBottom: constants.PADDING,
  },
  email: {
    color: colors.white,
  },
});

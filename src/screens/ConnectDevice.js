import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { setFirstTime } from '../actions';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import { Button } from '../components';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

class ConnectDevice extends Component {
  componentDidMount() {
    const { setFirstTime } = this.props;
    setFirstTime(false);
  }

  navigateToScan = () => {
    const { navigation } = this.props;
    navigation.navigate('QRScanner');
  };

  navigateToForm = () => {
    const { navigation } = this.props;
    navigation.navigate('ClaimDevice');
  };

  navigateToAuto = () => {
    const { navigation } = this.props;
    navigation.navigate('ClaimDevice', { isAutoDiscovery: true });
  };

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image
            style={styles.qrLogo}
            source={require('../assets/scanQR.png')}
          />
          <Text style={styles.createAccount}>
            {i18n.t('sync_pod_to_phone')}
          </Text>
          <Text style={styles.subTitle}>{i18n.t('scan_the_qr_code')}</Text>
          <Button
            title={i18n.t('scan_qr')}
            type={Button.types.PRIMARY}
            onPress={this.navigateToScan}
          />
          <View style={styles.row}>
            <View style={styles.line} />
            <Text style={styles.OR}>{i18n.t('or')}</Text>
            <View style={styles.line} />
          </View>
          <Button
            title={i18n.t('auto_discover')}
            onPress={this.navigateToAuto}
          />
          <View style={styles.separator} />
          <Button title={i18n.t('enter_code')} onPress={this.navigateToForm} />
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
  setFirstTime,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConnectDevice);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: moderateScale(32),
    paddingTop: 0,
  },
  createAccount: {
    ...typo.primary,
    fontSize: moderateScale(24),
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: moderateVerticalScale(constants.PADDING),
  },
  subTitle: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.white,
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(constants.PADDING / 2),
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: moderateVerticalScale(20),
  },
  line: {
    width: '30%',
    height: moderateScale(0.5),
    backgroundColor: colors.gray,
  },
  OR: {
    color: colors.white,
    paddingHorizontal: moderateScale(constants.PADDING),
  },
  separator: {
    height: moderateVerticalScale(constants.PADDING),
  },
  qrLogo: {
    width: moderateScale(150),
    height: moderateScale(150),
  },
  ask: {
    alignSelf: 'flex-start',
    marginBottom: moderateVerticalScale(10),
  },
  askText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.white,
    textDecorationLine: 'underline',
  },
});

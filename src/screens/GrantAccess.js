import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import ValidationComponent from 'react-native-form-validator';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import { InputText, Button, LoadingView } from '../components';
import { resetAccessData, setAccessData, updateFriend } from '../actions';
import { getAccessData, makeGetIsEpicLoading } from '../selectors';
import { languages, tunnels } from '../Constants';
const ActionButton = ({ title, subTitle, onPress }) => (
  <TouchableOpacity style={styles.action} onPress={onPress}>
    <View style={styles.details}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subTitle}>{subTitle}</Text>
    </View>
    <Image style={styles.forward} source={require('../assets/forward.png')} />
  </TouchableOpacity>
);

const PreviewButton = ({
  source,
  onPress,
  title,
  disabled,
  color = colors.main_color,
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={styles.previewAction}
    disabled={disabled}>
    <View style={[styles.imgCont, { backgroundColor: color }]}>
      <Image style={styles.icon} source={source} />
    </View>
    <Text style={[styles.actionTitle, { color }]}>{title}</Text>
  </TouchableOpacity>
);

class GrantAccess extends ValidationComponent {
  constructor(props) {
    super(props);
    const { data } = props;
    this.state = {
      nickname: data?.name,
    };
  }

  labels = {
    nickname: i18n.t('nickname'),
  };

  componentDidMount() {
    const { navigation, data } = this.props;

    navigation.setOptions({
      headerRight: () => (
        <Button
          title={i18n.t(data?.id ? 'done' : 'next')}
          type={Button.types.TERTIARY}
          style={{
            width: 50,
            alignSelf: 'flex-end',
            marginHorizontal: constants.PADDING,
          }}
          onPress={this.navigateToInfo}
        />
      ),
    });
  }

  componentWillUnmount() {
    const { resetAccessData } = this.props;
    resetAccessData();
  }

  onChangeText = nickname => this.setState({ nickname });

  navigateToLanguage = () => {
    const { navigation } = this.props;

    navigation.navigate('ChooseLanguage');
  };

  navigateToTunnel = () => {
    const { navigation } = this.props;

    navigation.navigate('ChangeTunnel');
  };

  navigateToInfo = async () => {
    const { navigation, setAccessData, updateFriend, data } = this.props;
    const { nickname } = this.state;
    const isValid = this.validate({
      nickname: { maxLength: 12, required: true },
    });
    if (isValid) {
      if (data?.id) {
        const resp = await updateFriend({
          ...data,
          name: nickname,
        });

        if (resp.success) {
          navigation.goBack();
        }
      } else {
        setAccessData({ name: nickname });

        navigation.navigate('ContactInfo');
      }
    }
  };

  previewInstructions = () => {
    const { navigation } = this.props;
    navigation.navigate('ImageSlider');
  };

  render() {
    const { data, isLoading } = this.props;
    return (
      <LoadingView
        isLoading={isLoading}
        containerStyle={styles.container}
        backgroundColor={colors.sub_bg}
        indicatorColor={colors.main_color}>
        <Text style={styles.grantAccess}>{i18n.t('grant_access')}</Text>
        <Text style={styles.subText}>{i18n.t('enter_the_name')}</Text>
        <InputText
          placeholder={i18n.t('enter_nickname')}
          onChangeText={this.onChangeText}
          leftIcon={'account-plus-outline'}
          containerStyle={styles.input}
          inputStyle={styles.inputStyle}
          error={this.getErrorsInField('nickname')[0]}
          defaultValue={data?.name}
        />
        <ActionButton
          title={i18n.t('language_for_instructions')}
          subTitle={
            languages.find(language => language.value === data?.language)?.label
          }
          onPress={this.navigateToLanguage}
        />
        <ActionButton
          title={i18n.t('change_tunnel')}
          subTitle={
            tunnels.find(tunnel => tunnel.value === data?.config?.tunnel)?.label
          }
          onPress={this.navigateToTunnel}
        />
        <View style={styles.actions}>
          <PreviewButton
            source={require('../assets/eye.png')}
            title={i18n.t('preview_instructions')}
            type={Button.types.TERTIARY}
            textStyle={{ color: colors.main_color }}
            style={{ alignSelf: 'flex-start' }}
            onPress={this.previewInstructions}
          />
        </View>
      </LoadingView>
    );
  }
}

const mapStateToProps = state => {
  const getIsLoading = makeGetIsEpicLoading();
  return {
    data: getAccessData(state),
    isLoading: getIsLoading(state, { epicName: 'updateFriend' }),
  };
};

const mapDispatchToProps = {
  setAccessData,
  updateFriend,
  resetAccessData,
};

export default connect(mapStateToProps, mapDispatchToProps)(GrantAccess);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    paddingTop: constants.PADDING * 3,
  },
  grantAccess: {
    ...typo.primary,
    fontSize: 28,
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
  subText: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: 16,
  },
  action: {
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'space-between',
    paddingVertical: constants.PADDING,
    paddingHorizontal: constants.PADDING / 2,
    height: 75,
    borderBottomWidth: 1,
    borderBottomColor: colors.abbey,
  },
  forward: {
    width: 8,
    height: 14,
    alignSelf: 'center',
  },
  details: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  title: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
  },
  subTitle: {
    ...typo.ancillary,
    fontSize: 14,
    color: colors.nobel,
  },
  input: {
    width: '100%',
    height: 48,
    marginTop: constants.PADDING,
  },
  actions: {
    borderBottomWidth: 1,
    borderBottomColor: colors.abbey,
  },
  previewAction: {
    flexDirection: 'row',
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  actionTitle: {
    ...typo.primary,
    fontSize: 16,
    color: colors.main_color,
    paddingStart: constants.PADDING,
  },
  imgCont: {
    width: 30,
    height: 30,
    borderRadius: 8,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 25,
    height: 15,
    resizeMode: 'contain',
  },
});

import i18n from 'i18n-js';
import toArray from 'lodash/toArray';
import React, { Component } from 'react';
import {
  Alert,
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import { checkLocalNetworkAccess } from 'react-native-local-network-permission';
import VersionNumber from 'react-native-version-number';
import { showNewTicket } from 'react-native-zendesk';
import { connect } from 'react-redux';
import { getDiagnosticInfo, getDevices } from '../actions';
import { colors, constants, typo } from '../common';
import { Button, LoadingView } from '../components';
import { getEntity, makeGetIsEpicLoading } from '../selectors';
import { RPIServices } from '../services';

class index extends Component {
  constructor(props) {
    super(props);
    const items = this.mapDevices();

    this.state = {
      info: '',
      isLoading: false,
      isWEPNAccessible: false,
      isUPNPExist: false,
      device: items?.[0]?.value || null,
      open: false,
    };
  }

  async componentDidMount() {
    const { getDevices } = this.props;

    await getDevices();

    Alert.alert(i18n.t('file_a_ticket'), i18n.t('we_need_device_connected'), [
      {
        text: i18n.t('skip'),
        onPress: () => this.showNewTicket(),
      },
      {
        text: i18n.t('ok'),
        onPress: () => this.onConfirm(),
      },
    ]);
  }

  onConfirm = async () => {
    const { devices } = this.props;
    if (devices.length === 0) {
      this.setState({ isLoading: true });
      await checkLocalNetworkAccess();
      RPIServices.searchDevice()
        .then(result => {
          const { respInfo } = result;
          const url = respInfo.redirects[0];
          var ip = url.split('/')[2].split(':')[0];
          this.getInfo({ local_ip_address: ip });
        })
        .catch(error => {
          console.log(error);
          this.showNewTicket();
          this.setState({ isLoading: false });
        });
    } else {
      this.getInfo(devices[0]);
    }
  };

  mapDevices = () => {
    const { devices } = this.props;
    return devices?.map(device => ({
      label: device.name,
      value: device.id,
      ...device,
    }));
  };

  componentWillUnmount() {
    this?.client?.removeAllListeners();
  }

  checkCompatibility = () => {
    const Client = require('react-native-ssdp').Client;
    this.client = new Client();
    this.client.on('response', (headers, statusCode) => {
      if (statusCode === 200 && headers?.ST?.includes('urn:schemas-upnp-org')) {
        this.client.stop();
        this.setState({ isUPNPExist: true });
      }
    });
    this.client.search('ssdp:all');
  };

  getInfo = async device => {
    this.setState({ isLoading: true });
    this.checkCompatibility();
    const result = await getDiagnosticInfo(device);
    let formattedErrorLogs = [];
    try {
      formattedErrorLogs = result?.error_logs
        ?.split('\n')
        ?.filter(o => o != '')
        ?.map(o => {
          try {
            return JSON.parse(o);
          } catch (error) {
            return { message: o };
          }
        });
    } catch (error) {
      formattedErrorLogs = result?.error_logs;
    }

    const info = {
      time: new Date().toUTCString(),
      device_accessible: result.isDeviceAccessible,
      error: result.deviceError,
      local_ip: device.local_ip_address,
      ip_address: device.ip_address,
      serial_number: device.serial_number,
      isUPNPExist: this.state.isUPNPExist,
      error_logs: formattedErrorLogs,
    };

    this.setState(
      {
        info: JSON.stringify(info, null, 4),
        isLoading: false,
        isWEPNAccessible: result.isWEPNAccessible,
      },
      () => {
        if (!result.isDeviceAccessible) {
          Alert.alert(
            i18n.t('no_devices_found'),
            i18n.t('you_need_to_be_connected'),
            [
              {
                text: i18n.t('skip'),
              },
              {
                text: i18n.t('retry'),
                onPress: () => this.getInfo(device),
              },
            ]
          );
        }
      }
    );
  };

  showNewTicket = () => {
    const { navigation } = this.props;
    const { info, isWEPNAccessible, device } = this.state;
    const device_sw_version = device?.software_version || '';
    showNewTicket({
      tags: ['appticket'],
      custom_fields: [
        {
          fieldId: 360034614551,
          value: `${VersionNumber.appVersion} (${VersionNumber.buildVersion})`,
        },
        {
          fieldId: 360034614571,
          value: String(info),
        },
        {
          fieldId: 360034614591,
          value: String(isWEPNAccessible),
        },
        {
          fieldId: 360034649152,
          value: device_sw_version,
        },
      ],
    });
    navigation.goBack();
  };

  onChangeText = info => {
    this.setState({ info });
  };

  setOpen = () => {
    this.setState({ open: true });
  };

  setClose = () => {
    this.setState({ open: false });
  };

  setValue = callback => {
    this.setState(
      state => ({
        device: callback(state.value),
      }),
      () => {
        const { device } = this.state;
        const selectedDevice = this.mapDevices()?.find(o => o.value === device);
        console.log({ selectedDevice });
        this.getInfo(selectedDevice);
      }
    );
  };

  render() {
    const { isLoading, info, open, device } = this.state;
    const { isGettingDevices } = this.props;
    const items = this.mapDevices();

    return (
      <LoadingView
        isLoading={isLoading || isGettingDevices}
        unmount={isGettingDevices}
        text={isLoading && i18n.t('please_wait_for_error_logs')}
        containerStyle={styles.container}
        textStyle={styles.text}
        indicatorColor={colors.main_color}
        backgroundColor={colors.sub_bg}>
        <View style={styles.content}>
          <Text style={styles.tip}>{i18n.t('you_will_get_updates')}</Text>
          {items.length > 1 && (
            <DropDownPicker
              open={open}
              value={device}
              items={items}
              onOpen={this.setOpen}
              onClose={this.setClose}
              setValue={this.setValue}
              containerStyle={styles.containerStyle}
              style={styles.dropDown}
              itemStyle={styles.itemStyle}
              dropDownStyle={styles.dropDown}
              placeholder={i18n.t('select_device')}
            />
          )}
          <TextInput
            style={styles.input}
            multiline
            value={info}
            onChangeText={this.onChangeText}
            returnKeyType={'done'}
            onSubmitEditing={() => Keyboard.dismiss()}
          />
          <Button
            title={i18n.t('next').toUpperCase()}
            onPress={this.showNewTicket}
            type={Button.types.PRIMARY}
          />
        </View>
      </LoadingView>
    );
  }
}
const makeMapStateToProps = () => {
  const getDevices = getEntity();
  const getIsLoading = makeGetIsEpicLoading();
  const mapStateToProps = state => ({
    devices: toArray(getDevices(state, { entity: 'devices' })),
    isGettingDevices: getIsLoading(state, { epicName: 'getDevices' }),
  });
  return mapStateToProps;
};

const mapDispatchToProps = {
  getDevices,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(index);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  content: {
    flex: 1,
    padding: constants.PADDING,
  },
  tip: {
    ...typo.ancillary,
    color: colors.white,
    paddingBottom: constants.PADDING,
  },
  input: {
    ...typo.ancillary,
    backgroundColor: colors.gray,
    height: 200,
    padding: constants.PADDING,
    fontSize: 12,
    marginBottom: constants.PADDING,
    borderRadius: 5,
    color: colors.black,
  },
  dropDown: {
    backgroundColor: colors.background_color,
  },
  containerStyle: {
    height: 40,
    marginBottom: constants.PADDING,
  },
  itemStyle: {
    justifyContent: 'flex-start',
  },
  text: {
    ...typo.ancillary,
    textAlign: 'center',
    padding: constants.PADDING * 2,
    lineHeight: 18,
    color: colors.white,
  },
});

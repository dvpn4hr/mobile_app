import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from 'i18n-js';
import CheckBox from '@react-native-community/checkbox';
import { Button } from '../components';
import {
  moderateScale,
  moderateVerticalScale,
  verticalScale,
} from 'react-native-size-matters/extend';

class SetupDevices extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCardPlugged: false,
      isRouterPlugged: false,
      isPodPlugged: false,
    };
  }

  getImage = () => {
    const { isRouterPlugged, isPodPlugged } = this.state;
    let source = require('../assets/connected.png');
    if (!isRouterPlugged && !isPodPlugged) {
      source = require('../assets/connectRouter.png');
    } else if (isRouterPlugged && !isPodPlugged) {
      source = require('../assets/connectPod.png');
    }
    return <Image style={styles.image} source={source} />;
  };

  getTitle = () => {
    const { isRouterPlugged, isPodPlugged } = this.state;
    let title = i18n.t('ta_da');
    if (!isRouterPlugged && !isPodPlugged) {
      title = i18n.t('plug_everything_in');
    } else if (isRouterPlugged && !isPodPlugged) {
      title = i18n.t('almost_there');
    }
    return <Text style={styles.title}>{title}</Text>;
  };

  getSubtitle = () => {
    const { isRouterPlugged, isPodPlugged } = this.state;
    let subTitle = ' ';
    if (isRouterPlugged && isPodPlugged) {
      subTitle = i18n.t('once_you_see');
    }
    return (
      <Text style={styles.subTitle} numberOfLines={2}>
        {subTitle}
      </Text>
    );
  };

  toggleRouter = isRouterPlugged => this.setState({ isRouterPlugged });

  togglePod = isPodPlugged => this.setState({ isPodPlugged });

  toggleCard = isCardPlugged => this.setState({ isCardPlugged });

  navigateSetup = () => {
    const { goToNext } = this.props;

    goToNext();
  };

  navigateToTicket = () => {
    const { navigation } = this.props;

    navigation.navigate('Ticket');
  };

  renderActions = () => {
    const { isRouterPlugged, isCardPlugged } = this.state;
    return (
      <View style={styles.actions}>
        <View style={styles.action}>
          <View>
            <Text style={styles.actionText}>{i18n.t('insert_sd_card')}</Text>
            <Text style={styles.ethernet}>
              {i18n.t('insert_sd_card_message')}
            </Text>
          </View>
          <CheckBox
            value={this.state.isCardPlugged}
            style={styles.checkbox}
            tintColor={colors.doveGray}
            onCheckColor={colors.main_color}
            onTintColor={colors.sub_bg}
            onFillColor={colors.sub_bg}
            boxType={'square'}
            disabled={this.state.isPodPlugged && this.state.isRouterPlugged}
            onValueChange={this.toggleCard}
          />
        </View>
        <View style={styles.action}>
          <View>
            <Text
              style={[
                styles.actionText,
                !isCardPlugged && { color: colors.gray },
              ]}>
              {i18n.t('connect_internet_router_to_pod')}
            </Text>
            <Text style={styles.ethernet}>
              {i18n.t('using_ethernet_cable')}
            </Text>
          </View>
          <CheckBox
            value={this.state.isRouterPlugged}
            style={styles.checkbox}
            tintColor={colors.doveGray}
            onCheckColor={colors.main_color}
            onTintColor={colors.sub_bg}
            onFillColor={colors.sub_bg}
            boxType={'square'}
            disabled={!this.state.isCardPlugged}
            onValueChange={this.toggleRouter}
          />
        </View>
        <View style={styles.action}>
          <View>
            <Text
              style={[
                styles.actionText,
                !isRouterPlugged && { color: colors.gray },
              ]}>
              {i18n.t('connect_pod_to_power_outlet')}
            </Text>
            <Text style={styles.ethernet}>
              {i18n.t('now_connect_your_device_to_power_outlet')}
            </Text>
          </View>
          <CheckBox
            value={this.state.isPodPlugged}
            style={styles.checkbox}
            tintColor={colors.doveGray}
            onCheckColor={colors.main_color}
            onTintColor={colors.sub_bg}
            onFillColor={colors.sub_bg}
            boxType={'square'}
            disabled={!this.state.isRouterPlugged && !this.state.isCardPlugged}
            onValueChange={this.togglePod}
          />
        </View>
      </View>
    );
  };

  renderButton = () => {
    const { isRouterPlugged, isPodPlugged, isCardPlugged } = this.state;
    const isEnabled = isRouterPlugged && isPodPlugged && isCardPlugged;
    return (
      <Button
        type={isEnabled ? Button.types.PRIMARY : Button.types.DISABLED}
        title={i18n.t('continue')}
        onPress={this.navigateSetup}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.getImage()}
        {this.getTitle()}
        {this.renderActions()}
        {this.getSubtitle()}
        {this.renderButton()}
        <Button
          type={Button.types.TERTIARY}
          title={i18n.t('get_help')}
          textStyle={styles.underline}
          onPress={this.navigateToTicket}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SetupDevices);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    padding: moderateScale(32),
    paddingTop: 0,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  image: {
    width: moderateScale(125),
    height: moderateScale(125),
    marginBottom: moderateVerticalScale(constants.PADDING),
  },
  title: {
    ...typo.primary,
    fontSize: moderateScale(28),
    color: colors.pampas,
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(16),
  },
  subTitle: {
    ...typo.ancillary,
    fontSize: moderateScale(15),
    color: colors.pampas,
    alignSelf: 'flex-start',
    height: moderateVerticalScale(55),
  },
  actions: {
    width: '100%',
  },
  action: {
    width: '100%',
    height: moderateVerticalScale(60),
    backgroundColor: colors.charade,
    borderRadius: moderateVerticalScale(8),
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: moderateScale(constants.PADDING),
    marginBottom: moderateVerticalScale(12),
    flexDirection: 'row',
  },
  checkbox: {
    width: moderateScale(24),
    height: moderateScale(24),
    borderColor: colors.main_color,
  },
  actionText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.pampas,
  },
  underline: {
    textDecorationLine: 'underline',
  },
  ethernet: {
    ...typo.ancillary,
    color: colors.doveGray,
    fontSize: moderateScale(12),
    paddingTop: moderateScale(4),
  },
});

import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import { colors } from '../common';
import Swiper from 'react-native-swiper';
import { ConnectDevice, GetReady, SetupDevices } from '.';
import StepIndicator from 'react-native-step-indicator';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { Button } from '../components';
import i18n from '../i18n';

const customStyles = {
  stepIndicatorSize: 18,
  currentStepIndicatorSize: 18,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 9,
  stepStrokeWidth: 2,
  stepStrokeCurrentColor: colors.blue_light,
  stepStrokeFinishedColor: colors.blue_light,
  stepStrokeUnFinishedColor: colors.nobel,
  separatorFinishedColor: colors.blue_light,
  separatorUnFinishedColor: colors.nobel,
  stepIndicatorFinishedColor: colors.blue_light,
  stepIndicatorUnFinishedColor: colors.sub_bg,
  stepIndicatorCurrentColor: colors.sub_bg,
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: colors.transparent,
  stepIndicatorLabelFinishedColor: colors.transparent,
  stepIndicatorLabelUnFinishedColor: colors.transparent,
  labelColor: colors.white,
  labelSize: 0,
  currentStepLabelColor: colors.blue_light,
};

const Setup = props => {
  const [step, setStep] = useState(0);
  let swiperRef = {};
  const { navigation } = props;
  useEffect(() => {
    if (step) {
      swiperRef.scrollBy(Platform.select({ ios: step, android: 1 }));
    }
  }, [step]);

  const goToNext = () => setStep(prev => prev + 1);

  const openTutorial = () => {
    navigation.navigate('Tutorial');
  };

  return (
    <View style={styles.container}>
      <View style={styles.indicator}>
        <StepIndicator
          customStyles={customStyles}
          currentPosition={step}
          stepCount={3}
        />
      </View>
      <View style={styles.swiper}>
        <Swiper
          ref={ref => (swiperRef = ref)}
          style={styles.wrapper}
          showsButtons={false}
          showsPagination={false}
          index={0}
          scrollEnabled={false}
          removeClippedSubviews>
          <GetReady goToNext={goToNext} {...props} />
          <SetupDevices goToNext={goToNext} {...props} />
          <ConnectDevice {...props} />
        </Swiper>
      </View>
      <Button
        title={i18n.t('watch_tutorial')}
        onPress={openTutorial}
        type={Button.types.TERTIARY}
        iconName={'video'}
        iconColor={colors.punch}
        iconSize={moderateScale(28)}
        textStyle={styles.tutorialText}
      />
    </View>
  );
};

export default Setup;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  indicator: {
    justifyContent: 'center',
    width: '50%',
    height: moderateVerticalScale(50),
    alignSelf: 'center',
  },
  swiper: {
    height: '85%',
  },
  tutorialText: {
    fontSize: moderateScale(16),
  },
});

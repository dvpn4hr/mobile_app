import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from 'i18n-js';
import { AlertDialog, Button, LoadingIndicator } from '../components';
import { searchDevice, getProgress } from '../actions';
import {
  makeGetEntity,
  makeGetEpicResult,
  makeGetIsEpicLoading,
} from '../selectors';
import { CommonActions } from '@react-navigation/routers';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

const Action = ({ title, state, error, onPress }) => (
  <TouchableOpacity onPress={onPress} style={styles.action}>
    <View>
      <Text style={styles.actionText}>{title}</Text>
      {error && <Text>{i18n.t('tap_for_help')}</Text>}
    </View>
    <View
      style={state === null ? styles.none : state ? styles.check : styles.warn}
    />
  </TouchableOpacity>
);

const initialState =
  '{"Can talk with WEPN Server": null, "Can forward ports": null, "VPN is ready": null}';

class RunSetup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    };
  }

  componentDidMount() {
    const { searchDevice } = this.props;
    searchDevice().then(res => {
      if (res.success) {
        const {
          result: { networkID, hostID },
        } = res;

        this.runSetup(networkID, hostID);
      } else {
        throw new Error('');
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { progress = {} } = this.props;
    const { data = initialState } = progress;
    if (Object.values(JSON.parse(data)).every(one => one)) {
      clearInterval(this.progress);
    }
  }

  componentWillUnmount() {
    clearInterval(this.progress);
  }

  runSetup = (networkID, hostID) => {
    const { getProgress } = this.props;
    getProgress({ deviceIP: `${networkID}.${hostID}` });
    this.progress = setInterval(() => {
      const { isLoading } = this.props;
      if (!isLoading) {
        getProgress({ deviceIP: `${networkID}.${hostID}` });
      }
    }, 5000);
  };

  navigateToApp = () => {
    const { navigation } = this.props;
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'App',
          },
        ],
      })
    );
  };

  toggleAlert = () => this.setState(prev => ({ isVisible: !prev.isVisible }));

  renderActions = () => {
    const { progress = {} } = this.props;
    const { data = initialState } = progress;
    let keys = {};
    try {
      keys = JSON.parse(data);
    } catch (error) {
      keys = data;
    }
    return (
      <View style={styles.actions}>
        {Object.keys(keys)?.map(key => (
          <Action title={key} state={keys[key]} />
        ))}
      </View>
    );
  };
  renderButton = () => {
    return (
      <Button
        type={Button.types.PRIMARY}
        title={i18n.t('continue')}
        onPress={this.navigateToApp}
      />
    );
  };

  render() {
    const { isVisible } = this.state;
    const { isLoading } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.ph}>{isLoading && <LoadingIndicator />}</View>
          {this.renderActions()}
          <View style={styles.ph}>{this.renderButton()}</View>
        </View>
        <AlertDialog
          visible={isVisible}
          iconSrc={require('../assets/fail.png')}
          title={i18n.t('can_t_talk_to_wepn_server')}
          description={i18n.t('can_t_talk_description')}
          actions={[
            {
              title: i18n.t('close'),
              onPress: this.toggleAlert,
            },
            {
              title: i18n.t('report'),
              onPress: this.toggleAlert,
            },
          ]}
        />
      </View>
    );
  }
}

const makeMapStateToProps = () => {
  const getDevice = makeGetEntity();
  const getResult = makeGetEpicResult();
  const getIsLoading = makeGetIsEpicLoading();
  return (state, props) => ({
    device: getDevice(state, {
      entity: 'devices',
      entity_id: props?.route?.params?.deviceId,
    }),
    progress: getResult(state, {
      epicName: 'getProgress',
    }),
    isLoading:
      getIsLoading(state, { epicName: 'searchDevice' }) ||
      getIsLoading(state, { epicName: 'getProgress' }),
  });
};

const mapDispatchToProps = {
  searchDevice,
  getProgress,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(RunSetup);

const styles = StyleSheet.create({
  check: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.main_color,
  },
  warn: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.ecstasy,
  },
  none: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(12),
    backgroundColor: colors.abbey,
  },
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  ph: {
    height: '30%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    padding: moderateScale(constants.PADDING),
    borderBottomLeftRadius: moderateScale(16),
    borderBottomRightRadius: moderateScale(16),
  },
  actions: {
    width: '100%',
  },
  action: {
    width: '100%',
    height: moderateVerticalScale(60),
    borderRadius: moderateScale(8),
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.charade,
    padding: moderateScale(constants.PADDING),
    marginBottom: moderateVerticalScale(constants.PADDING),
    flexDirection: 'row',
  },
  actionText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.pampas,
  },
});

import { CommonActions } from '@react-navigation/native';
import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ValidationComponent from 'react-native-form-validator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LinearGradient from 'react-native-linear-gradient';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { setUserPassword } from '../actions';
import { colors, constants } from '../common';
import { Button, InputText, LoadingView } from '../components';
import { makeGetEpicError, makeGetIsEpicLoading } from '../selectors';

class ResetPassword extends ValidationComponent {
  constructor(props) {
    super(props);
    const { route } = props;

    this.state = {
      email: route?.params?.email,
      one_time_password: '',
      new_password: '',
    };
  }

  labels = {
    email: i18n.t('email'),
    one_time_password: i18n.t('one_time_password'),
    new_password: i18n.t('new_password'),
  };

  componentWillUnmount() {
    const { setUserPassword } = this.props;

    setUserPassword({ reset: true });
  }

  reset = async () => {
    const isValid = this.validate({
      email: { email: true, required: true },
      one_time_password: { required: true },
      new_password: {
        required: true,
        minlength: 9,
        hasUpperCase: true,
        hasLowerCase: true,
        hasNumber: true,
      },
    });

    if (isValid) {
      const { setUserPassword, navigation } = this.props;
      const { email, one_time_password, new_password } = this.state;

      const { success } = await setUserPassword({
        email,
        one_time_password,
        new_password,
      });
      if (success) {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: 'Login' }],
          })
        );
      }
    }
  };

  getFieldErrors = fieldName => {
    const { errors } = this.props;
    return (
      this.getErrorsInField(fieldName)[0] ||
      (errors && errors[fieldName] && errors[fieldName][0])
    );
  };

  onValueChange = () => {
    this.setState(prev => ({
      terms: !prev.terms,
      termsError: false,
    }));
  };

  render() {
    const { isLoading, route, errors } = this.props;
    return (
      <LoadingView
        isLoading={isLoading}
        backgroundColor={colors.charade}
        indicatorColor={colors.main_color}>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.content}
          keyboardShouldPersistTaps={'always'}>
          <Text style={styles.createAccount}>{i18n.t('reset_password')}</Text>
          <Text style={styles.text}>
            {i18n.t('enter_the_code_sent')}: {this.state.email}
          </Text>
          <Text style={styles.errorTxt}>{errors?.error_description || ''}</Text>
          <View style={styles.form}>
            <InputText
              ref={ref => (this.oldPassword = ref)}
              leftIcon={'onepassword'}
              placeholder={i18n.t('one_time_password')}
              onChangeText={one_time_password =>
                this.setState({ one_time_password })
              }
              error={this.getFieldErrors('one_time_password')}
              returnKeyType={'next'}
              onSubmitEditing={() => this.newPassword.focus()}
              textContentType={'none'}
            />
            <View style={{ width: 0, height: 0 }}>
              <InputText
                leftIcon={'email-outline'}
                placeholder={i18n.t('email')}
                showClear
                onChangeText={email => this.setState({ email: email.trim() })}
                error={this.getFieldErrors('email')}
                autoComplete={'email'}
                keyboardType={'email-address'}
                textContentType={'username'}
                defaultValue={route?.params?.email}
                returnKeyType={'next'}
                onSubmitEditing={() => this.oldPassword.focus()}
              />
            </View>
            <InputText
              ref={ref => (this.newPassword = ref)}
              leftIcon={'form-textbox-password'}
              placeholder={i18n.t('new_password')}
              secureTextEntry
              onChangeText={new_password => this.setState({ new_password })}
              error={this.getFieldErrors('new_password')}
              autoComplete={'password'}
              textContentType={'newPassword'}
              passwordRules={
                'minlength: 8; required: lower; required: upper; required: digit;'
              }
              returnKeyType={'done'}
              onSubmitEditing={this.reset}
            />
            <Button
              title={i18n.t('submit')}
              onPress={this.reset}
              type={Button.types.PRIMARY}
            />
          </View>
        </KeyboardAwareScrollView>
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getEpicError = makeGetEpicError();
  const getIsEpicLoading = makeGetIsEpicLoading();

  return state => ({
    errors: getEpicError(state, { epicName: 'setUserPassword' }),
    isLoading: getIsEpicLoading(state, { epicName: 'setUserPassword' }),
  });
};

const mapDispatchToProps = {
  setUserPassword,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(ResetPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    paddingTop: moderateVerticalScale(68),
    padding: moderateScale(32),
  },
  content: {
    flex: 1,
  },
  createAccount: {
    fontSize: moderateScale(24),
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
  errorTxt: {
    fontSize: moderateScale(14),
    padding: constants.PADDING,
    color: colors.error,
  },
  text: {
    fontSize: moderateScale(16),
    color: colors.white,
    paddingTop: constants.PADDING,
  },
});

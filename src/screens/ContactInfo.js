import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import { Button, InputText, LoadingView } from '../components';
import { setAccessData } from '../actions';
import ValidationComponent from 'react-native-form-validator';
import { addFriend, updateFriend, resetAccessData } from '../actions';
import {
  getAccessData,
  makeGetEpicError,
  makeGetIsEpicLoading,
} from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Collapsible from 'react-native-collapsible';
import { moderateScale } from 'react-native-size-matters/extend';
class ContactInfo extends ValidationComponent {
  constructor(props) {
    super(props);
    const { data } = props;
    this.state = {
      email: data?.email || '',
      familiar_phrase: data?.passcode || '',
      collapsed: true,
      isWaiting: false,
    };
  }

  labels = {
    email: i18n.t('email'),
    familiar_phrase: i18n.t('passcode'),
  };

  componentDidMount() {
    this.setDone();
  }

  setDone = () => {
    const { navigation } = this.props;

    navigation.setOptions({
      headerRight: () => (
        <Button
          title={i18n.t('done')}
          type={Button.types.TERTIARY}
          style={styles.done}
          onPress={this.setData}
        />
      ),
    });
  };

  componentWillUnmount() {
    const { addFriend } = this.props;

    addFriend({ reset: true });
  }

  setData = async () => {
    const { navigation, resetAccessData, addFriend, updateFriend } = this.props;
    const { email, familiar_phrase } = this.state;

    const isValid = this.validate({
      email: { email: true },
      familiar_phrase: { required: !!email },
    });

    if (isValid) {
      navigation.setOptions({
        headerRight: () => null,
      });

      const { data, route, setAccessData } = this.props;
      const showConfirm = route?.params?.showConfirm;

      const params = {
        ...data,
        email,
        passcode: familiar_phrase,
        has_connected: 0,
        cert_id: (Math.random() * 1e4).toString(36).substr(1, 5),
      };

      if (showConfirm) {
        setAccessData(params);
        navigation.navigate('EmailFriend');
        return;
      }

      let resp = {};
      if (data?.id) {
        resp = await updateFriend({
          ...data,
          email,
          passcode: familiar_phrase,
        });
      } else {
        resp = await addFriend(params);
      }

      if (resp.success) {
        if (data?.id) {
          navigation.goBack();
        } else {
          this.setState({ isWaiting: true });
          setTimeout(() => {
            navigation.navigate('AccessKey', {
              entity_id: resp?.result?.id,
              backToHome: true,
            });

            this.setState({ isWaiting: false });
          }, 30000);
        }
        resetAccessData();
      } else {
        this.setDone();
      }
    }
  };

  onChangeText = email => this.setState({ email: email.trim() });

  onChangePhrase = familiar_phrase => this.setState({ familiar_phrase });

  toggleCollapsed = () =>
    this.setState(prev => ({ collapsed: !prev.collapsed }));

  render() {
    const { error, isLoading, data } = this.props;
    const { email, familiar_phrase, collapsed, isWaiting } = this.state;

    return (
      <LoadingView
        isLoading={isLoading || isWaiting}
        containerStyle={styles.container}
        backgroundColor={colors.sub_bg}
        indicatorColor={colors.main_color}
        text={i18n.t('adding_friend_this_may_take_a_few_seconds')}
        textStyle={styles.loadingText}>
        <Text style={styles.grantAccess}>{i18n.t('add_email')}</Text>
        <Text style={styles.subText}>
          {i18n.t('we_need_a_little', { name: data?.name })}
        </Text>
        <TouchableOpacity style={styles.moreBtn} onPress={this.toggleCollapsed}>
          <Text style={styles.learn_more}>{i18n.t('learn_why')}</Text>
          <Icon
            name={collapsed ? 'chevron-down' : 'chevron-up'}
            size={20}
            color={colors.main_color}
          />
        </TouchableOpacity>
        <Collapsible collapsed={collapsed}>
          <Text style={styles.subText}>{i18n.t('learn_why_desc')}</Text>
        </Collapsible>
        {error && (
          <Text style={styles.errorTxt}>{error?.error_description}</Text>
        )}
        <InputText
          placeholder={i18n.t('enter_email')}
          onChangeText={this.onChangeText}
          source={require('../assets/envelope.png')}
          containerStyle={styles.input}
          inputStyle={styles.inputStyle}
          error={this.getErrorsInField('email')[0]}
          autoComplete={'email'}
          keyboardType={'email-address'}
          textContentType={'emailAddress'}
          autoCapitalize={'none'}
          defaultValue={email}
        />
        <InputText
          placeholder={i18n.t('familiar_phrase')}
          onChangeText={this.onChangePhrase}
          source={require('../assets/message.png')}
          error={this.getErrorsInField('familiar_phrase')[0]}
          defaultValue={familiar_phrase}
          showBottomBorder
          imgStyle={styles.imgStyle}
        />
        <Text style={styles.description}>
          {i18n.t('any_email_sent_from_wepn')}
        </Text>
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getError = makeGetEpicError();
  const getLoading = makeGetIsEpicLoading();
  return state => ({
    data: getAccessData(state),
    error: getError(state, { epicName: 'addFriend' }),
    isLoading:
      getLoading(state, { epicName: 'addFriend' }) ||
      getLoading(state, { epicName: 'updateFriend' }),
  });
};

const mapDispatchToProps = {
  setAccessData,
  addFriend,
  updateFriend,
  resetAccessData,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(ContactInfo);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    padding: constants.PADDING,
    paddingTop: constants.PADDING * 3,
  },
  grantAccess: {
    ...typo.primary,
    fontSize: 28,
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING,
  },
  subText: {
    ...typo.ancillary,
    color: colors.white,
    fontSize: 16,
  },
  input: {
    width: '100%',
    height: 48,
    marginTop: constants.PADDING,
  },
  errorTxt: {
    fontSize: 14,
    paddingHorizontal: constants.PADDING,
    paddingVertical: constants.PADDING / 4,
    color: colors.error,
    alignSelf: 'center',
  },
  done: {
    width: 50,
    alignSelf: 'flex-end',
    marginHorizontal: constants.PADDING,
  },
  description: {
    ...typo.ancillary,
    color: colors.nobel,
    fontSize: 14,
  },
  imgStyle: {
    width: 25,
    height: 25,
  },
  moreBtn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginVertical: constants.PADDING / 2,
  },
  learn_more: {
    ...typo.secondary,
    color: colors.main_color,
    fontSize: 14,
    marginRight: constants.PADDING / 4,
  },
  loadingText: {
    ...typo.secondary,
    color: colors.white,
    fontSize: moderateScale(14),
    padding: moderateScale(constants.PADDING),
    textAlign: 'center',
  },
});

import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { colors, constants, typo } from '../common';

const Section = ({ title, body, index }) => (
  <View style={styles.section}>
    <View style={styles.index}>
      <Text style={styles.indexTxt}>{index + 1}</Text>
    </View>
    <Text style={styles.sectionTitle}>{title}</Text>
    <Text style={styles.body}>{body}</Text>
  </View>
);
const ProviderTips = () => {
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.title}>{i18n.t('five_things_to_know')}</Text>
        {i18n.t('provider_tips')?.map((tip, index) => (
          <Section title={tip.title} body={tip.body} index={index} />
        ))}
      </ScrollView>
    </View>
  );
};

export default ProviderTips;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 68,
    padding: constants.PADDING,
    backgroundColor: colors.sub_bg,
  },
  title: {
    ...typo.primary,
    fontSize: 28,
    paddingBottom: constants.PADDING,
    color: colors.white,
  },
  section: {
    paddingVertical: constants.PADDING,
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: 18,
    paddingVertical: constants.PADDING / 2,
    color: colors.white,
  },
  body: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
  },
  index: {
    width: 28,
    height: 28,
    borderRadius: 14,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: constants.PADDING / 2,
  },
  indexTxt: {
    ...typo.primary,
    color: colors.white,
  },
});

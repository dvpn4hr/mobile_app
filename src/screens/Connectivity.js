import { Linking, Platform, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colors } from '../common'
import { EmptyView } from '../components'
import I18n from 'i18n-js'
import AndroidOpenSettings from 'react-native-android-open-settings'

const Connectivity = (params) => {
    const { isConnected, isInternetReachable } = params;

    const mapStateToMessage = () => {
        if (!isConnected) {
            return I18n.t('please_check_your_internet_connection')
        } else if (!isInternetReachable) {
            return I18n.t('connected_but_not_reachable')
        }
    }

    const mapStateToTitle = () => {
        if (!isConnected) {
            return I18n.t('no_internet_connection')
        } else if (!isInternetReachable) {
            return I18n.t('not_reachable')
        }
    }

    const openSettings = () => {
        if (Platform.OS === 'ios')
            Linking.openURL("App-Prefs:root=WIFI");
        else
            AndroidOpenSettings.generalSettings();
    }

    return (
        <View style={styles.container}>
            <EmptyView
                title={mapStateToTitle()}
                message={mapStateToMessage()}
                source={require('../assets/router.png')}
                actions={[
                    {
                        title: I18n.t('open_settings'),
                        onPress: openSettings,
                    },
                ]}
            />
        </View>
    )
}

export default Connectivity

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.chradeLight,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
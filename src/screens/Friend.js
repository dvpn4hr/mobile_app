import React, { useMemo, useEffect, useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  Switch,
  Alert,
  RefreshControl,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import { makeGetEntity, makeGetIsEpicLoading } from '../selectors';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import { deleteFriend, getFriend } from '../actions';
import { LoadingView } from '../components';
import { setAccessData, updateFriend } from '../actions';
import { languages, tunnels } from '../Constants';
const ActionButton = ({
  source,
  onPress,
  title,
  disabled,
  color = colors.main_color,
}) => (
  <TouchableOpacity onPress={onPress} style={styles.action} disabled={disabled}>
    <View style={[styles.imgCont, { backgroundColor: color }]}>
      <Image style={styles.icon} source={source} resizeMode={'contain'} />
    </View>
    <Text style={[styles.actionTitle, { color }]}>{title}</Text>
  </TouchableOpacity>
);

const SectionItem = ({ title, body, subTitle, onPress, renderRight }) => (
  <TouchableOpacity
    style={styles.mainSection}
    disabled={!onPress}
    onPress={onPress}>
    <View style={styles.row}>
      <Text style={styles.title}>{title}</Text>
      {renderRight?.() ?? (
        <View style={styles.subRow}>
          <Text style={styles.body}>{body}</Text>
          {onPress && (
            <Icon name={'chevron-right'} size={24} color={colors.silver} />
          )}
        </View>
      )}
    </View>
    {subTitle && (
      <Text style={[styles.body, { paddingBottom: constants.PADDING }]}>
        {subTitle}
      </Text>
    )}
  </TouchableOpacity>
);

const Friend = ({ route }) => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { entity_id } = route?.params;
  const getFriendEntity = useMemo(makeGetEntity, []);
  const friend = useSelector(state =>
    getFriendEntity(state, { entity: 'friends', entity_id })
  );

  const getIsLoading = useMemo(makeGetIsEpicLoading, []);
  const isLoading = useSelector(
    state =>
      getIsLoading(state, { epicName: 'deleteFriend' }) ||
      getIsLoading(state, { epicName: 'getAccessLink' })
  );

  const isLoadingFriend = useSelector(state =>
    getIsLoading(state, { epicName: 'getFriend' })
  );

  useEffect(() => {
    navigation.setOptions({ headerTitle: friend?.name });
  }, []);

  const [notify, setNotify] = useState(friend?.subscribed);

  const onRefresh = () => {
    dispatch(getFriend({ id: entity_id }));
  };

  const removeAccess = () => {
    Alert.alert(
      i18n.t('delete_friend_from_list'),
      i18n.t('are_you_sure'),
      [
        {
          text: i18n.t('cancel'),
        },
        {
          text: i18n.t('ok'),
          onPress: async () => {
            const { success } = await dispatch(deleteFriend({ id: entity_id }));
            if (success) {
              navigation.goBack();
            }
          },
          style: 'destructive',
        },
      ],
      {
        cancellable: true,
      }
    );
  };

  const shareAccessLink = () => {
    navigation.navigate('GrantAccess', {
      screen: 'AccessKey',
      params: {
        entity_id,
      },
    });
  };

  const onValueChange = status => {
    setNotify(status);
    dispatch(
      updateFriend({
        ...friend,
        subscribed: status,
      })
    );
  };

  const renderSeparator = () => <View style={styles.separator} />;

  const renderRestart = () => (
    <>
      {renderSeparator()}
      <ActionButton
        title={i18n.t('share_access_link')}
        source={require('../assets/key.png')}
        onPress={shareAccessLink}
      />
      {renderSeparator()}
      <ActionButton
        title={i18n.t('revoke_access')}
        source={require('../assets/key.png')}
        onPress={removeAccess}
        color={colors.error}
      />
      {renderSeparator()}
    </>
  );

  const navigateToEdit = () => {
    dispatch(setAccessData(friend));
    navigation.navigate('GrantAccess', {
      screen: 'ContactInfo',
      params: {
        nextIndex: 1,
      },
    });
  };

  const navigateToEditName = () => {
    dispatch(setAccessData(friend));
    navigation.navigate('GrantAccess', {
      screen: 'GrantAccess',
    });
  };

  return (
    <LoadingView
      isLoading={isLoading}
      indicatorColor={colors.main_color}
      backgroundColor={colors.sub_bg}
      unmount>
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl refreshing={isLoadingFriend} onRefresh={onRefresh} />
        }>
        <View style={styles.container}>
          <View style={styles.section}>
            <Text style={styles.sectionTitle}>
              {i18n.t('about').toUpperCase()}
            </Text>
            {renderSeparator()}
            <SectionItem
              title={i18n.t('name')}
              body={friend?.name}
              onPress={navigateToEditName}
            />
            {renderSeparator()}
            <SectionItem
              title={i18n.t('language')}
              body={
                languages.find(language => language.value === friend?.language)
                  ?.label
              }
              onPress={navigateToEditName}
            />
            {renderSeparator()}
            <SectionItem
              title={i18n.t('tunnel_access')}
              body={
                tunnels.find(tunnel => tunnel.value === friend?.config?.tunnel)
                  ?.label
              }
              onPress={navigateToEditName}
            />
            {renderSeparator()}
          </View>
          {!!friend?.email && (
            <View style={styles.section}>
              <Text style={styles.sectionTitle}>
                {i18n.t('updates').toUpperCase()}
              </Text>
              {renderSeparator()}
              <SectionItem
                title={i18n.t('email_updates')}
                subTitle={i18n.t('email_this_friend')}
                renderRight={() => (
                  <Switch
                    value={notify}
                    onValueChange={onValueChange}
                    trackColor={{ true: colors.main_color }}
                    disabled={!friend?.email}
                  />
                )}
              />
              {renderSeparator()}
              <SectionItem
                title={i18n.t('email')}
                body={friend?.email}
                onPress={navigateToEdit}
              />
              {renderSeparator()}
              <SectionItem
                title={i18n.t('passcode')}
                subTitle={i18n.t('this_allows_your_friend')}
                body={friend?.passcode}
                onPress={navigateToEdit}
              />
              {renderSeparator()}
            </View>
          )}
          <View style={styles.section}>
            <Text style={styles.sectionTitle}>
              {i18n.t('other').toUpperCase()}
            </Text>
            {renderRestart()}
          </View>
        </View>
      </ScrollView>
    </LoadingView>
  );
};

export default Friend;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
  },
  section: {
    paddingTop: constants.PADDING,
  },
  sectionTitle: {
    ...typo.primary,
    fontSize: 12,
    color: colors.main_color,
    fontWeight: 'bold',
    paddingHorizontal: constants.PADDING,
    marginBottom: constants.PADDING,
  },
  separator: {
    width: '98%',
    height: 0.4,
    backgroundColor: colors.manatee,
    alignSelf: 'center',
  },
  action: {
    flexDirection: 'row',
    paddingHorizontal: constants.PADDING,
    width: '100%',
    height: 55,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.chradeLight,
  },
  actionTitle: {
    ...typo.primary,
    fontSize: 16,
    color: colors.main_color,
    paddingStart: constants.PADDING,
  },
  imgCont: {
    width: 30,
    height: 30,
    borderRadius: 8,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 16,
    height: 16,
  },
  mainSection: {
    backgroundColor: colors.chradeLight,
    paddingHorizontal: constants.PADDING,
  },
  row: {
    flexDirection: 'row',
    width: '100%',
    minHeight: 55,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    ...typo.secondary,
    color: colors.white,
    fontWeight: 'bold',
  },
  body: {
    ...typo.ancillary,
    color: colors.manatee,
  },
  subRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

import { StyleSheet, Text, View, Image } from 'react-native';
import React, { useMemo } from 'react';
import { colors, typo } from '../common';
import i18n from 'i18n-js';
import { useDispatch, useSelector } from 'react-redux';
import { getAccessData, makeGetIsEpicLoading } from '../selectors';
import { Button, LoadingView } from '../components';
import { StackActions, useNavigation } from '@react-navigation/core';
import { resetAccessData, updateFriend } from '../actions';
import Snackbar from 'react-native-snackbar';

const EmailFriend = () => {
  const navigation = useNavigation();
  const data = useSelector(getAccessData);
  const dispatch = useDispatch();
  const getIsLoading = useMemo(makeGetIsEpicLoading, []);
  const isLoading = useSelector(state =>
    getIsLoading(state, { epicName: 'updateFriend' })
  );

  const sendEmail = async () => {
    const { success } = await dispatch(updateFriend(data));
    if (success) {
      Snackbar.show({
        text: i18n.t('email_will_be_sent'),
        duration: Snackbar.LENGTH_LONG,
      });
      goBack();
      dispatch(resetAccessData());
    }
  };

  const goBack = () => {
    navigation.dispatch(StackActions.pop(2));
  };
  return (
    <LoadingView
      isLoading={isLoading}
      containerStyle={styles.container}
      backgroundColor={colors.charade}
      indicatorColor={colors.main_color}>
      <View style={styles.iconCont}>
        <Image source={require('../assets/email.png')} style={styles.icon} />
      </View>
      <Text style={styles.title}>
        {i18n.t('email_friend_an_access_key', { name: data?.name })}
      </Text>
      <Text style={styles.sub}>{i18n.t('we_will_send_an_invitation')}</Text>
      <Button
        title={i18n.t('send')}
        type={Button.types.PRIMARY}
        onPress={sendEmail}
      />
      <Button
        title={i18n.t('send_the_key_myself')}
        type={Button.types.TERTIARY}
        onPress={goBack}
        underline
      />
    </LoadingView>
  );
};

export default EmailFriend;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    alignItems: 'center',
    justifyContent: 'space-around',
    padding: 20,
  },
  iconCont: {
    width: 30,
    height: 30,
    borderRadius: 10,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 20,
    height: 10,
    resizeMode: 'contain',
  },
  title: {
    ...typo.primary,
    fontSize: 21,
    color: colors.white,
    textAlign: 'center',
  },
  sub: {
    ...typo.ancillary,
    fontSize: 16,
    color: colors.white,
    textAlign: 'center',
  },
});

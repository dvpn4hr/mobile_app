import i18n from 'i18n-js';
import React from 'react';
import { StyleSheet, Text, View, Linking, Keyboard } from 'react-native';
import ValidationComponent from 'react-native-form-validator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { signUserUp } from '../actions';
import { colors, constants, links, typo } from '../common';
import { Button, InputText, LoadingView } from '../components';
import { makeGetEpicError, makeGetIsEpicLoading } from '../selectors';

class SignUp extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
    };
  }

  labels = {
    email: i18n.t('email'),
    password: i18n.t('password'),
    password_confirmation: i18n.t('password_confirmation'),
  };

  componentWillUnmount() {
    const { signUserUp } = this.props;

    signUserUp({ reset: true });
  }

  signUp = async () => {
    const isValid = this.validate({
      email: { email: true, required: true },
      password: {
        required: true,
        minlength: 9,
        hasUpperCase: true,
        hasLowerCase: true,
        hasNumber: true,
      },
      password_confirmation: {
        required: true,
        minlength: 9,
        equalPassword: this.state.password,
        hasUpperCase: true,
        hasLowerCase: true,
        hasNumber: true,
      },
    });

    if (isValid) {
      const { signUserUp, navigation } = this.props;
      const { email, password } = this.state;

      var params = {
        username: email,
        email: email?.toLowerCase(),
        password: password,
        grant_type: 'password',
      };

      const { success } = await signUserUp(params);
      if (success) {
        navigation.navigate('Setup');
      }
    }
  };

  getFieldErrors = fieldName => {
    const { errors } = this.props;
    return (
      this.getErrorsInField(fieldName)[0] ||
      (errors && errors[fieldName] && errors[fieldName][0])
    );
  };

  navigateToLogin = () => {
    const { navigation } = this.props;

    navigation.replace('Login');
  };

  render() {
    const { isLoading } = this.props;
    return (
      <LoadingView
        isLoading={isLoading}
        backgroundColor={colors.sub_bg}
        indicatorColor={colors.main_color}
        containerStyle={styles.container}>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps={'always'}
          showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            <Text style={styles.createAccount}>
              {i18n.t('create_your_account')}
            </Text>
            <Text style={styles.subTitle}>{i18n.t('creating_an_account')}</Text>
            <View style={styles.form}>
              <InputText
                ref={ref => (this.emailRef = ref)}
                leftIcon={'email-outline'}
                placeholder={i18n.t('email')}
                showClear
                onChangeText={email => this.setState({ email: email.trim() })}
                error={this.getFieldErrors('email')}
                autoComplete={'email'}
                keyboardType={'email-address'}
                textContentType={'username'}
                returnKeyType={'next'}
                autoCapitalize={'none'}
                onSubmitEditing={() => this.passwordRef.focus()}
                containerStyle={styles.input}
              />
              <InputText
                ref={ref => (this.passwordRef = ref)}
                leftIcon={'form-textbox-password'}
                placeholder={i18n.t('password')}
                secureTextEntry
                onChangeText={password => this.setState({ password })}
                error={this.getFieldErrors('password')}
                textContentType={'newPassword'}
                autoComplete={'password-new'}
                passwordRules={
                  'minlength: 8; required: lower; required: upper; required: digit;'
                }
                returnKeyType={'next'}
                returnKeyLabel={i18n.t('next')}
                onSubmitEditing={() => this.passwordConfRef.focus()}
                showBottomBorder
              />
              <InputText
                ref={ref => (this.passwordConfRef = ref)}
                leftIcon={'form-textbox-password'}
                placeholder={i18n.t('password_confirmation')}
                secureTextEntry
                onChangeText={password_confirmation =>
                  this.setState({ password_confirmation })
                }
                error={this.getFieldErrors('password_confirmation')}
                textContentType={'newPassword'}
                autoComplete={'password-new'}
                passwordRules={
                  'minlength: 8; required: lower; required: upper; required: digit;'
                }
                returnKeyType={'done'}
                returnKeyLabel={i18n.t('done')}
                onSubmitEditing={() => Keyboard.dismiss()}
                showBottomBorder
              />
              <View style={styles.row}>
                <Text style={styles.text}>
                  <Text>{i18n.t('by_creating_an_account')}</Text>
                  <Text
                    style={styles.textLink}
                    onPress={() => Linking.openURL(links.terms_of_service)}>
                    {' '}
                    {i18n.t('terms_of_service')}
                  </Text>
                  <Text> and </Text>
                  <Text
                    style={styles.textLink}
                    onPress={() => Linking.openURL(links.privacy_policy)}>
                    {' '}
                    {i18n.t('privacy_policy')}.
                  </Text>
                </Text>
              </View>
              <Button
                title={i18n.t('create_account')}
                onPress={this.signUp}
                type={Button.types.PRIMARY}
              />
            </View>
            <Button
              type={Button.types.TERTIARY}
              title={i18n.t('already_have_an_account')}
              textStyle={styles.underline}
              onPress={this.navigateToLogin}
            />
          </View>
        </KeyboardAwareScrollView>
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getEpicError = makeGetEpicError();
  const getIsEpicLoading = makeGetIsEpicLoading();

  return state => ({
    errors: getEpicError(state, { epicName: 'signUserUp' }),
    isLoading: getIsEpicLoading(state, { epicName: 'signUserUp' }),
  });
};

const mapDispatchToProps = {
  signUserUp,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(SignUp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    paddingTop: moderateVerticalScale(68),
    paddingHorizontal: moderateScale(32),
  },
  content: {
    flex: 1,
  },
  createAccount: {
    ...typo.primary,
    fontSize: moderateScale(24),
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: moderateVerticalScale(constants.PADDING),
  },
  subTitle: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.white,
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(constants.PADDING),
  },
  row: {
    flexDirection: 'row',
    paddingBottom: moderateVerticalScale(constants.PADDING),
    marginHorizontal: moderateVerticalScale(constants.PADDING),
  },
  text: {
    flex: 1,
    fontSize: moderateScale(12),
    color: colors.silver,
  },
  textLink: {
    textDecorationLine: 'underline',
  },
  underline: {
    textDecorationLine: 'underline',
  },
  input: {
    width: '100%',
    marginTop: moderateVerticalScale(constants.PADDING),
  },
});

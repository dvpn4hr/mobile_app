import CheckBox from '@react-native-community/checkbox';
import { CommonActions } from '@react-navigation/native';
import i18n from 'i18n-js';
import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Keyboard,
} from 'react-native';
import ValidationComponent from 'react-native-form-validator';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import { connect } from 'react-redux';
import { signUserIn } from '../actions';
import { colors, constants, typo } from '../common';
import { Button, InputText, LoadingView } from '../components';
import { makeGetEpicError, makeGetIsEpicLoading } from '../selectors';

class Login extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      rememberPass: false,
      isChecking: false,
    };
  }

  labels = {
    email: i18n.t('email'),
    password: i18n.t('password'),
  };

  componentWillUnmount() {
    const { signUserIn } = this.props;

    signUserIn({ reset: true });
  }

  onValueChange = () => {
    this.setState(prev => ({
      rememberPass: !prev.rememberPass,
    }));
  };

  signIn = async () => {
    const isValid = this.validate({
      email: { email: true, required: true },
      password: { required: true, minlength: 8 },
    });

    if (isValid) {
      const { signUserIn, navigation, route } = this.props;
      const { email, password, rememberPass } = this.state;
      const isDirect = route?.params?.isDirect;

      const { success } = await signUserIn({
        username: email,
        password,
        grant_type: 'password',
        remember: rememberPass,
      });

      if (success) {
        if (isDirect) {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: 'App',
                },
              ],
            })
          );
        } else {
          navigation.navigate('Setup');
        }
      }
    }
  };

  signUp = () => {
    const { navigation } = this.props;

    navigation.replace('SignUp');
  };

  forgotPassword = () => {
    const { navigation } = this.props;

    navigation.navigate('ForgotPassword');
  };

  render() {
    const { errors, isLoading } = this.props;
    const { isChecking, rememberPass } = this.state;
    return (
      <LoadingView
        isLoading={isLoading || isChecking}
        backgroundColor={colors.sub_bg}
        indicatorColor={colors.main_color}
        containerStyle={styles.container}>
        <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'}>
          <View style={styles.contentContainer}>
            <Text style={styles.createAccount}>{i18n.t('welcome_back')}</Text>
            <View style={styles.form}>
              <Text style={styles.errorTxt}>
                {errors?.error_description || errors?.error}
              </Text>
              <InputText
                ref={ref => (this.email = ref)}
                leftIcon={'email-outline'}
                placeholder={i18n.t('email')}
                showClear
                onChangeText={email => this.setState({ email: email.trim() })}
                error={this.getErrorsInField('email')[0]}
                autoComplete={'email'}
                keyboardType={'email-address'}
                textContentType={'emailAddress'}
                returnKeyType={'next'}
                onSubmitEditing={() => this.password.focus()}
                autoCapitalize={'none'}
                containerStyle={styles.input}
              />
              <InputText
                ref={ref => (this.password = ref)}
                leftIcon={'form-textbox-password'}
                placeholder={i18n.t('password')}
                secureTextEntry
                onChangeText={password => this.setState({ password })}
                error={this.getErrorsInField('password')[0]}
                autoComplete={'password'}
                textContentType={'password'}
                returnKeyType={'done'}
                onSubmitEditing={() => Keyboard.dismiss()}
                showBottomBorder
              />
              <TouchableOpacity onPress={this.onValueChange} style={styles.row}>
                <CheckBox
                  value={rememberPass}
                  style={styles.checkbox}
                  tintColor={colors.white}
                  onCheckColor={colors.white}
                  onTintColor={colors.white}
                  onFillColor={colors.main_color}
                  boxType={'square'}
                />
                <Text style={styles.text}>{i18n.t('remember_password')}</Text>
              </TouchableOpacity>
              <Button
                title={i18n.t('login')}
                onPress={this.signIn}
                type={Button.types.PRIMARY}
              />

              <View style={styles.otherLinks}>
                <View />
                <Button
                  title={i18n.t('forgot_password')}
                  onPress={this.forgotPassword}
                  type={Button.types.TERTIARY}
                />
              </View>
            </View>
            <Button
              title={i18n.t('don_t_have_an_account')}
              onPress={this.signUp}
              type={Button.types.TERTIARY}
              textStyle={styles.underline}
            />
          </View>
        </KeyboardAwareScrollView>
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getEpicError = makeGetEpicError();
  const getIsEpicLoading = makeGetIsEpicLoading();

  return state => ({
    errors: getEpicError(state, { epicName: 'signUserIn' }),
    isLoading: getIsEpicLoading(state, { epicName: 'signUserIn' }),
  });
};

const mapDispatchToProps = {
  signUserIn,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.charade,
    paddingTop: moderateVerticalScale(60),
  },
  contentContainer: {
    flex: 1,
    padding: constants.PADDING * 2,
  },
  row: {
    flexDirection: 'row',
    paddingBottom: constants.PADDING,
    width: '70%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkbox: {
    width: moderateScale(16),
    height: moderateScale(16),
    marginEnd: moderateScale(10),
    borderColor: colors.white,
  },
  otherLinks: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  text: {
    fontSize: moderateScale(16),
    color: colors.white,
    marginHorizontal: constants.PADDING / 2,
  },
  errorTxt: {
    fontSize: moderateScale(14),
    color: colors.error,
    alignSelf: 'center',
  },
  createAccount: {
    ...typo.primary,
    fontSize: moderateScale(24),
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: constants.PADDING / 2,
    paddingHorizontal: constants.PADDING,
  },
  underline: {
    textDecorationLine: 'underline',
  },
  input: {
    width: '100%',
    marginTop: constants.PADDING,
  },
});

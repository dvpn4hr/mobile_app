import React, { Component } from 'react';
import { View, Text, StyleSheet, Linking, Image } from 'react-native';
import { connect } from 'react-redux';
import { colors, typo, links } from '../common';
import i18n from '../i18n';
import { AlertDialog, Button } from '../components';
import { canRequestPermission } from '../Utils';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';
import NetInfo from '@react-native-community/netinfo';
import { showArticle } from 'react-native-zendesk/lib';

class CompatibilityTest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connectionType: 'wifi',
      showAlert: false,
    };
  }

  async componentDidMount() {
    const { canRequest } = await this.checkPermissions();

    if (canRequest) {
      this.showDialog();
    }

    NetInfo.fetch().then(this.onNetInfoChange);

    this.unsubscribe = NetInfo.addEventListener(this.onNetInfoChange);
  }

  checkPermissions = async () => {
    const canRequest = await canRequestPermission();

    return { canRequest };
  };

  componentWillUnmount() {
    this?.unsubscribe?.();
  }

  openSettings = async () => {
    if (Platform.OS === 'ios') {
      await Linking.openURL('app-settings:');
    } else {
      await Linking.openSettings();
    }
  };

  checkPermission = () => {
    this.setState({ showAlert: false }, async () => {
      const { canRequest } = await this.checkPermissions();

      if (canRequest) {
        this.openSettings();
      }
    });
  };

  showDialog = () => {
    this.setState({ showAlert: true });
  };

  onClose = () => {
    const { navigation } = this.props;
    this.setState({ showAlert: false }, () => {
      navigation.goBack();
    });
  };

  onNetInfoChange = state => {
    this.setState({
      connectionType: state.type,
    });
  };

  openWebSite = () => {
    Linking.openURL(links.web_site);
  };

  navigateToRunTest = () => {
    const { navigation } = this.props;

    navigation.navigate('RunTest');
  };

  showHelp = () => {
    showArticle({
      articleId: '17936390539021',
    });
  };

  render() {
    const { ssid, connectionType, showAlert } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Image
            source={require('../assets/test_start.png')}
            style={styles.comp}
            resizeMode={'contain'}
          />
          <Text style={styles.mainText}>{i18n.t('check_compatibility')}</Text>
          <Text style={styles.sub}>{i18n.t('connect_to_internet')}</Text>
          <View style={styles.mainRouter}>
            <Text style={styles.mainRouterText}>
              {i18n.t('connect_to_main_router')}
            </Text>
          </View>
          {connectionType !== 'wifi' && (
            <Text style={styles.error}>{i18n.t('connect_to_wifi')}</Text>
          )}
          <Button
            type={
              connectionType === 'wifi' &&
              [i18n.t('waiting'), i18n.t('detecting')].indexOf(ssid) === -1
                ? Button.types.PRIMARY
                : Button.types.DISABLED
            }
            title={i18n.t('run_test')}
            onPress={this.navigateToRunTest}
          />
        </View>
        <AlertDialog
          visible={showAlert}
          title={i18n.t('permission_required')}
          description={i18n.t('local_network_permission_required_message')}
          showMore
          onPressMore={this.showHelp}
          onClose={this.onClose}
          iconSrc={require('../assets/unavailable.png')}
          actions={[
            {
              title: i18n.t('ok'),
              onPress: this.checkPermission,
            },
            {
              title: i18n.t('cancel'),
              onPress: this.onClose,
            },
          ]}
        />
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(CompatibilityTest);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.charade,
    paddingHorizontal: moderateScale(32),
    paddingTop: moderateVerticalScale(62),
    borderWidth: 1,
  },
  content: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  mainText: {
    ...typo.primary,
    fontSize: moderateScale(28),
    color: colors.white,
    textAlign: 'center',
    alignSelf: 'center',
    paddingBottom: moderateVerticalScale(32),
  },
  sub: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.white,
    textAlign: 'left',
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(16),
    lineHeight: moderateScale(22.4),
  },
  error: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.error,
    textAlign: 'left',
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(32),
  },
  comp: {
    width: moderateScale(190),
    height: moderateVerticalScale(82),
    marginBottom: moderateVerticalScale(32),
  },
  mainRouter: {
    width: '100%',
    height: moderateVerticalScale(41),
    backgroundColor: colors.chradeLight,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(8),
    marginBottom: moderateVerticalScale(16),
  },
  mainRouterText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.white,
    textAlign: 'center',
    alignSelf: 'center',
  },
  ask: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: moderateVerticalScale(16),
  },
  askText: {
    ...typo.ancillary,
    fontSize: moderateScale(14),
    color: colors.white,
    textDecorationLine: 'underline',
  },
});

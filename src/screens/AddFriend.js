import React from 'react';
import { View, Text, StyleSheet, Keyboard } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants } from '../common';
import Icon from 'react-native-vector-icons/MaterialIcons';
import i18n from '../i18n';
import { Button, InputText, LoadingView } from '../components';
import ValidationComponent from 'react-native-form-validator';
import {
  makeGetEpicError,
  makeGetIsEpicLoading,
  makeGetEntity,
} from '../selectors';
import { addFriend, updateFriend } from '../actions';
import RNPickerSelect from 'react-native-picker-select';
const languages = [
  { label: 'English', value: 'en' },
  { label: 'Persian', value: 'fa' },
  { label: 'Chinese', value: 'cn' },
];

class AddFriend extends ValidationComponent {
  constructor(props) {
    super(props);
    const { friend } = props;
    this.state = {
      email: friend?.email || '',
      passcode: friend?.passcode || '',
      language: friend?.language || 'en',
    };
  }

  componentWillUnmount() {
    const { addFriend } = this.props;
    addFriend({ reset: true });
  }

  labels = {
    email: i18n.t('email'),
    passcode: i18n.t('familiar_phrase'),
    language: i18n.t('language'),
  };

  onChangeText = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  addFriend = friend => {
    const { addFriend, updateFriend, navigation } = this.props;
    let action = friend?.id ? updateFriend : addFriend;
    action(friend).then(result => {
      if (result.success) {
        navigation.goBack();
      }
    });
  };

  submit = () => {
    const isValid = this.validate({
      email: { required: true },
      passcode: { required: true },
      language: { required: true },
    });
    if (isValid) {
      const { email, passcode, language } = this.state;
      const { friend } = this.props;
      this.addFriend({
        email,
        passcode,
        language,
        telegram_handle: friend?.telegram_handle || null,
        id: friend?.id || null,
        has_connected: friend?.has_connected || 0,
        cert_id:
          friend?.cert_id || (Math.random() * 1e4).toString(36).substr(1, 5),
      });
    }
  };

  getFieldErrors = fieldName => {
    const { errors } = this.props;
    return (
      this.getErrorsInField(fieldName)[0] ||
      (errors && errors[fieldName] && errors[fieldName][0])
    );
  };

  render() {
    const { errors, isLoading, isUpdating, friend } = this.props;
    const { email, passcode, language } = this.state;
    return (
      <LoadingView
        backgroundColor={colors.sub_bg}
        containerStyle={styles.container}
        indicatorColor={colors.main_color}
        isLoading={isLoading || isUpdating}
        textStyle={styles.loaderTitle}>
        <Icon name="people-alt" size={100} color={colors.white} />
        <Text style={styles.title}>
          {i18n.t(friend ? 'update_friend' : 'add_new_friend')}
        </Text>
        <Text style={styles.errorTxt}>{errors?.error_description || ''}</Text>
        <InputText
          ref={ref => (this.email = ref)}
          leftIcon={'email-outline'}
          placeholder={i18n.t('email')}
          showClear
          onChangeText={email => this.setState({ email: email.trim() })}
          error={this.getErrorsInField('email')[0]}
          autoComplete={'email'}
          keyboardType={'email-address'}
          textContentType={'emailAddress'}
          defaultValue={email}
          onSubmitEditing={() => this.phrase.focus()}
          returnKeyType={'next'}
        />
        <InputText
          ref={ref => (this.phrase = ref)}
          leftIcon={'lock'}
          placeholder={i18n.t('familiar_phrase')}
          showClear
          onChangeText={val => this.onChangeText('passcode', val)}
          error={this.getFieldErrors('passcode')}
          autoComplete={'name'}
          keyboardType={'default'}
          textContentType={'name'}
          defaultValue={passcode}
          onSubmitEditing={() => Keyboard.dismiss()}
          returnKeyType={'next'}
        />
        <>
          <View style={styles.dropdown}>
            <Icon name="language" size={24} color={colors.white} />
            <RNPickerSelect
              onValueChange={language => this.setState({ language })}
              items={languages}
              style={pickerStyles}
              value={language}
              touchableWrapperProps={{
                style: pickerStyles.touchable,
              }}
              placeholder={{
                label: i18n.t('select_language'),
                value: '',
              }}
              useNativeAndroidPickerStyle={false}
            />
          </View>
          <Text style={styles.errorTxt}>{this.getFieldErrors('language')}</Text>
        </>
        <Text style={styles.inform}>{i18n.t('inform_friend')}</Text>
        <Button title={i18n.t('submit')} onPress={this.submit} />
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getErrors = makeGetEpicError();
  const getIsLoading = makeGetIsEpicLoading();
  const getFriend = makeGetEntity();
  const mapStateToProps = (state, props) => ({
    errors: getErrors(state, { epicName: 'addFriend' }),
    isLoading: getIsLoading(state, { epicName: 'addFriend' }),
    isUpdating: getIsLoading(state, { epicName: 'updateFriend' }),
    friend: getFriend(state, {
      entity: 'friends',
      entity_id: props?.route?.params?.entity_id,
    }),
  });
  return mapStateToProps;
};

const mapDispatchToProps = {
  addFriend,
  updateFriend,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(AddFriend);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.main_color,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: constants.PADDING * 2,
  },
  title: {
    fontSize: 18,
    color: colors.white,
    fontWeight: 'bold',
    padding: constants.PADDING,
  },
  errorTxt: {
    fontSize: 14,
    paddingHorizontal: constants.PADDING,
    paddingVertical: constants.PADDING / 2,
    color: colors.error,
    alignSelf: 'flex-start',
  },
  loaderTitle: {
    color: colors.main_color,
    fontSize: 16,
    padding: constants.PADDING * 2,
    textAlign: 'center',
  },
  dropdown: {
    width: '100%',
    height: 50,
    borderRadius: 25,
    paddingHorizontal: constants.PADDING,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: colors.colorWithAlpha('white', 0.2),
  },
  inform: {
    color: colors.white,
    fontSize: 16,
    paddingBottom: constants.PADDING,
    textAlign: 'justify',
  },
});

const pickerStyles = StyleSheet.create({
  viewContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingHorizontal: constants.PADDING,
  },
  placeholder: {
    color: colors.white,
    fontSize: 16,
  },
  inputIOSContainer: {
    flex: 1,
  },
  inputIOS: {
    color: colors.white,
    fontSize: 16,
    flex: 1,
  },
  inputAndroid: {
    color: colors.white,
    fontSize: 16,
    height: '100%',
    paddingHorizontal: constants.PADDING,
  },
  touchable: {
    flex: 1,
    alignSelf: 'stretch',
  },
});

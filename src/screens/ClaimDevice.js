import React from 'react';
import { Text, StyleSheet, Alert } from 'react-native';
import { connect } from 'react-redux';
import { colors, constants, typo } from '../common';
import i18n from '../i18n';
import { Button, InputText, LoadingView } from '../components';
import ValidationComponent from 'react-native-form-validator';
import { makeGetEpicError, makeGetIsEpicLoading } from '../selectors';
import { addDevice, searchDevice, setDeviceKey } from '../actions';
import {
  moderateScale,
  moderateVerticalScale,
} from 'react-native-size-matters/extend';

class ClaimDevice extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      device_name: '',
      device_key: '',
      serial_number: '',
    };
  }

  componentDidMount() {
    const { route } = this.props;

    const isAutoDiscovery = route?.params?.isAutoDiscovery;

    if (isAutoDiscovery) {
      this.autoDiscovery();
    }
  }

  componentWillUnmount() {
    const { addDevice } = this.props;
    addDevice({ reset: true });
  }

  labels = {
    device_name: i18n.t('device_name'),
    device_key: i18n.t('device_key'),
    serial_number: i18n.t('serial_number'),
  };

  onChangeText = (key, value) => {
    this.setState({
      [key]: value,
    });
  };

  addDevice = device => {
    const { addDevice, navigation } = this.props;
    addDevice(device).then(resp => {
      if (resp.success) {
        navigation.navigate('ClaimStack');
      }
    });
  };

  submit = () => {
    const isValid = this.validate({
      device_name: { required: true },
      device_key: { required: true },
      serial_number: { required: true, minLength: 8, maxLength: 15 },
    });
    if (isValid) {
      const { device_name, device_key, serial_number } = this.state;
      const { addDevice } = this.props;
      this.addDevice({
        device_name,
        device_key,
        serial_number,
      });
    }
  };

  scanCode = () => {
    const { navigation, setDeviceKey } = this.props;
    navigation.navigate('QRScanner', {
      onQRRead: (device_key, serial_number, PK) => {
        console.log(device_key, serial_number);
        this.addDevice({
          device_name: 'WEPN Device',
          device_key,
          serial_number,
        });
        setDeviceKey(PK);
      },
    });
  };

  autoDiscovery = (title = i18n.t('automatic_discovery')) => {
    Alert.alert(
      title,
      i18n.t('please_make_sure'),
      [
        {
          text: i18n.t('cancel'),
          style: 'destructive',
        },
        {
          text: i18n.t('ok'),
          onPress: this.searchDevice,
        },
      ],
      {
        cancellable: true,
      }
    );
  };

  searchDevice = async () => {
    const { searchDevice } = this.props;
    searchDevice()
      .then(res => {
        if (res.success) {
          const {
            result: { data },
          } = res;
          this.claimDevice(data);
        } else {
          throw new Error('');
        }
      })
      .catch(() => {
        this.autoDiscovery(i18n.t('no_devices_found'));
      });
  };

  claimDevice = data => {
    const { navigation } = this.props;
    try {
      const device = JSON.parse(data);
      console.log({ device });
      if (device.claimed === '0') {
        const { serial_number, device_key } = device;
        Alert.alert(
          i18n.t('device_found'),
          i18n.t('claim_process'),
          [
            {
              text: i18n.t('cancel'),
              style: 'destructive',
            },
            {
              text: i18n.t('ok'),
              onPress: () =>
                this.addDevice({
                  device_name: 'WEPN Device',
                  device_key,
                  serial_number,
                }),
            },
          ],
          {
            cancellable: true,
          }
        );
      } else {
        Alert.alert(
          i18n.t('already_claimed'),
          i18n.t('device_already_claimed')
        );
        navigation.navigate('ClaimStack');
      }
    } catch (e) {
      Alert.alert(
        i18n.t('unexpected_error_occurred'),
        error?.message ||
          i18n.t(
            'please_file_a_ticket',
            [
              {
                text: i18n.t('cancel'),
                style: 'destructive',
              },
              {
                text: i18n.t('file_a_ticket'),
                onPress: this.fileTicket,
              },
            ],
            {
              cancellable: true,
            }
          )
      );
    }
  };

  fileTicket = () => {
    const { navigation } = this.props;
    navigation.navigate('Ticket');
  };

  getFieldErrors = fieldName => {
    const { errors } = this.props;
    return (
      this.getErrorsInField(fieldName)[0] ||
      (errors && errors[fieldName] && errors[fieldName][0])
    );
  };

  render() {
    const { errors, isLoading, isAdding } = this.props;
    return (
      <LoadingView
        containerStyle={styles.container}
        indicatorColor={colors.main_color}
        isLoading={isLoading || isAdding}
        text={
          isLoading ? i18n.t('searching_for_device') : i18n.t('adding_device')
        }
        textStyle={styles.loaderTitle}
        backgroundColor={colors.sub_bg}>
        <Text style={styles.createAccount}>{i18n.t('claim_device')}</Text>
        <Text style={styles.subTitle}>{i18n.t('enter_device_details')}</Text>
        {errors && (
          <Text style={styles.errorTxt}>{errors?.error_description || ''}</Text>
        )}
        <InputText
          ref={ref => (this.name = ref)}
          leftIcon={'rename-box'}
          placeholder={i18n.t('device_name')}
          showClear
          onChangeText={val => this.onChangeText('device_name', val)}
          error={this.getFieldErrors('device_name')}
          autoComplete={'name'}
          keyboardType={'default'}
          textContentType={'name'}
          onSubmitEditing={() => this.key.focus()}
          returnKeyType={'next'}
        />
        <InputText
          ref={ref => (this.key = ref)}
          leftIcon={'key'}
          placeholder={i18n.t('device_key')}
          showClear
          onChangeText={val => this.onChangeText('device_key', val)}
          error={this.getFieldErrors('device_key')}
          autoComplete={'name'}
          keyboardType={'default'}
          textContentType={'name'}
          onSubmitEditing={() => this.serial.focus()}
          returnKeyType={'next'}
        />

        <InputText
          ref={ref => (this.serial = ref)}
          leftIcon={'shield-lock'}
          placeholder={i18n.t('serial_number')}
          secureTextEntry
          onChangeText={val => this.onChangeText('serial_number', val)}
          error={this.getFieldErrors('serial_number')}
          autoComplete={'password'}
          textContentType={'password'}
          returnKeyType={'done'}
          onSubmitEditing={this.submit}
          showBottomBorder
        />

        <Button
          title={i18n.t('submit')}
          onPress={this.submit}
          type={Button.types.PRIMARY}
        />
      </LoadingView>
    );
  }
}

const makeMapStateToProps = () => {
  const getErrors = makeGetEpicError();
  const getIsLoading = makeGetIsEpicLoading();
  return state => ({
    errors: getErrors(state, { epicName: 'addDevice' }),
    isAdding: getIsLoading(state, { epicName: 'addDevice' }),
    isLoading: getIsLoading(state, { epicName: 'searchDevice' }),
  });
};

const mapDispatchToProps = {
  addDevice,
  searchDevice,
  setDeviceKey,
};

export default connect(makeMapStateToProps, mapDispatchToProps)(ClaimDevice);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.sub_bg,
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: moderateScale(32),
    paddingTop: moderateVerticalScale(68),
  },
  errorTxt: {
    fontSize: moderateScale(14),
    padding: moderateScale(constants.PADDING),
    color: colors.error,
  },
  loaderTitle: {
    color: colors.white,
    fontSize: 16,
    padding: moderateScale(constants.PADDING * 2),
    textAlign: 'center',
  },
  createAccount: {
    ...typo.primary,
    fontSize: 24,
    color: colors.white,
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    paddingBottom: moderateVerticalScale(constants.PADDING),
  },
  subTitle: {
    ...typo.ancillary,
    fontSize: moderateScale(16),
    color: colors.white,
    alignSelf: 'flex-start',
    paddingBottom: moderateVerticalScale(constants.PADDING),
  },
});

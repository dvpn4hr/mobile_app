import pickBy from 'lodash/pickBy';
import { checkLocalNetworkAccess } from 'react-native-local-network-permission';
export const allSettled = promises => {
  let wrappedPromises = promises.map(p =>
    Promise.resolve(p).then(
      val => ({ success: true, value: val }),
      err => ({ success: false, reason: err })
    )
  );
  return Promise.all(wrappedPromises);
};

const checkPermissions = async () => {
  const localNetworkGranted = await checkLocalNetworkAccess();

  return { localNetworkGranted };
};

export const canRequestPermission = async () => {
  const { localNetworkGranted } = await checkPermissions();
  return !localNetworkGranted;
};

export const shouldNavigateToSettings = async () => {
  const { localNetworkGranted } = await checkPermissions();

  return !localNetworkGranted;
};

export const clearObject = obj =>
  pickBy(obj, value => ![null, undefined, ''].includes(value));

export async function fetchWithTimeout(resource, options = {}) {
  const { timeout = 8000 } = options;

  const controller = new AbortController();
  const id = setTimeout(() => controller.abort(), timeout);

  const response = await fetch(resource, {
    ...options,
    signal: controller.signal,
  });
  clearTimeout(id);

  return response;
}

import { createSelector } from 'reselect';

const getEpics = state => state.epics;

export const makeGetEpic = () => {
  return createSelector(
    getEpics,
    (_, props) => props.epicName,
    (Epics, epicName) => Epics[epicName]
  );
};

export const makeGetIsEpicLoading = () => {
  return createSelector(makeGetEpic(), action => action?.loading || false);
};

export const makeGetEpicError = () => {
  return createSelector(makeGetEpic(), action => action?.error);
};

export const makeGetEpicIds = () => {
  return createSelector(makeGetEpic(), action => action?.ids);
};

export const makeGetEpicResult = () => {
  return createSelector(makeGetEpic(), action => action?.result);
};

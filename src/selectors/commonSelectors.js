import { createSelector } from 'reselect';

export const getIsFirstTime = state => state.firstTime;
export const getEntities = state => state.entities;
export const getAccessData = state => state.accessData;
export const getIsProduction = state => state.isProduction;
export const getEntity = () => {
  return createSelector(
    [getEntities, (state, props) => props.entity],
    (entities, entity) => entity && entities[entity]
  );
};

export const makeGetEntity = () => {
  return createSelector(
    [getEntity(), (state, props) => props.entity_id],
    (entity, entity_id) => entity && entity[entity_id]
  );
};

import { createSelector } from 'reselect';
import { getEntity, makeGetEpicIds } from '.';

const getEntities = getEntity();
const getIds = makeGetEpicIds();

export const getMainDeviceId = state => state.mainDeviceId;

export const getDeviceIssues = createSelector(
  (state, _) => getEntities(state, { entity: 'diagnostics' }),
  (state, _) => getIds(state, { epicName: 'getDeviceDiagnostics' }),
  (diagnostics, ids) => {
    const issueIds = ids?.filter(id => !diagnostics?.[id]?.state);
    return issueIds?.map(id => diagnostics?.[id]);
  }
);

export const makeGetMainDevice = () => {
  return createSelector(
    (state, _) => getEntities(state, { entity: 'devices' }),
    getMainDeviceId,
    (devices, mainDevice) => {
      return devices?.[mainDevice];
    }
  );
};

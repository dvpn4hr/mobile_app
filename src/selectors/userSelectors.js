import { createSelector } from 'reselect';
export const getUserToken = state => state.auth.userToken;
export const getUserData = state => state.auth.userData;

export const getShouldRefreshToken = createSelector(getUserToken, token => {
  return new Date() > new Date(token.expired_at) && token.remember;
});

export const getUserId = createSelector(getUserData, user => user?.id);

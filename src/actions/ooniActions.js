import actionTypes from '../actionTypes';

export const toggleOONISetup = state => ({
  type: actionTypes.TOGGLE_OONI_SETUP,
  state,
});

export const toggleOONIWorking = state => ({
  type: actionTypes.TOGGLE_OONI_WORKING,
  state,
});

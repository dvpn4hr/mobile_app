import actionTypes from '../actionTypes';

export const setFirstTime = state => ({
  type: actionTypes.SET_FIRST_TIME,
  state,
});

export const setAccessData = data => ({
  type: actionTypes.SET_ACCESS_DATA,
  data,
});

export const resetAccessData = () => ({
  type: actionTypes.RESET_ACCESS_DATA,
});

export const switchEnvironment = () => ({
  type: actionTypes.SWITCH_ENVIRONMENT,
});

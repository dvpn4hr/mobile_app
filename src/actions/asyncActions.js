import * as asyncActions from '../epics';
import each from 'lodash/each';
let actions = {};

each(asyncActions, (action, name) => {
  actions[name] = action.actionCreator;
});

module.exports = actions;

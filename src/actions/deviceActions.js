import Ping from 'react-native-ping';
import actionTypes from '../actionTypes';
import { APIServices, RPIServices } from '../services';
import { allSettled } from '../Utils';

export async function getDiagnosticInfo(device) {
  const { local_ip_address, local_token } = device;
  const result = await allSettled([
    APIServices.checkService(),
    Ping.start(local_ip_address, { timeout: 1000 }),
    RPIServices.getErrorLog({ deviceIP: local_ip_address, local_token }),
  ]);
  return {
    isWEPNAccessible: result[0].success,
    isDeviceAccessible: result[2].success,
    deviceError: result[1].reason?.message || result[2].reason?.message,
    error_logs: result[2].value?.data,
  };
}

export const setMainDevice = id => ({
  type: actionTypes.SET_MAIN_DEVICE,
  id,
});

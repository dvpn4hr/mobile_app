import { initialize, identifyAnonymous } from 'react-native-zendesk';
import actionTypes from '../actionTypes';
import { getUserToken } from '../selectors';
import { signUserIn } from './asyncActions';
const Config = require('../../environment');

export const setUserToken = data => ({
  type: actionTypes.SET_USER_TOKEN,
  data,
});

export const refreshToken = () => {
  return (dispatch, getState) => {
    const userToken = getUserToken(getState());
    return dispatch(
      signUserIn({
        refresh_token: userToken.refresh_token,
        grant_type: 'refresh_token',
        remember: userToken.remember,
      })
    );
  };
};

export const onStartUp = () => {
  return () => {
    initialize({
      appId: Config.ZENDESK_APP_ID,
      clientId: Config.ZENDESK_CLIENT_ID,
      zendeskUrl: Config.ZENDESK_APP_URL,
    });
  };
};

export const onSignIn = user => {
  return dispatch => {
    identifyAnonymous('');
    dispatch(setUserData(user));
  };
};

export const setUserData = data => ({
  type: actionTypes.SET_USER_DATA,
  data,
});

export const setDeviceKey = key => ({
  type: actionTypes.SET_DEVICE_KEY,
  key,
});

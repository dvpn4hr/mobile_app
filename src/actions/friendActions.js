import actionTypes from '../actionTypes';

export const addFriendHash = device => ({
  type: actionTypes.ADD_FRIEND_HASH,
  device,
});

export const removeFriendHash = friend_id => ({
  type: actionTypes.REMOVE_FRIEND_HASH,
  friend_id,
});

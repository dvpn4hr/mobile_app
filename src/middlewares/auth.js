import { signUserOut } from '../actions';
import { reset } from '../RootNavigation';
import { getUserToken } from '../selectors';

const auth =
  ({ dispatch, getState }) =>
  next =>
  action => {
    const { type, error } = action;
    const access_token = getUserToken(getState())?.access_token;

    if (type.includes('_FAILED') && error?.statusCode === 401 && access_token) {
      reset('Auth');
      dispatch(
        signUserOut({
          token: access_token,
        })
      );
      return;
    }

    return next(action);
  };

export default auth;

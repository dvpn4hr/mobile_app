import { StyleSheet, Text, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import NetInfo, { useNetInfo } from '@react-native-community/netinfo';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { colors } from './common';
import { Connectivity } from './screens';
import { withSecurityScreen } from './hooks';
import MainApp from './navigation';

const App = () => {
  const [isConnected, setIsConnected] = useState(true);
  const netInfo = useNetInfo();

  const checkConnectivity = ({ isConnected, isInternetReachable }) => {
    const connected = isConnected ?? true;
    const internetReachable = isInternetReachable ?? true;
    setIsConnected(connected && internetReachable);
  };

  useEffect(() => {
    NetInfo.fetch().then(checkConnectivity);
  }, []);

  useEffect(() => {
    checkConnectivity(netInfo);
  }, [netInfo]);

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <SafeAreaProvider style={{ flex: 1 }}>
        <SafeAreaView
          style={styles.container}
          forceInset={{ top: 'always', bottom: 'always' }}>
          {isConnected ? <MainApp /> : <Connectivity {...netInfo} />}
        </SafeAreaView>
      </SafeAreaProvider>
    </GestureHandlerRootView>
  );
};

export default withSecurityScreen(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.main_color,
  },
});

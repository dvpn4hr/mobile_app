import APIServices from './APIServices';
import RPIServices from './RPIServices';
import ModemServices from './ModemServices';
import { SpeedTest } from './SpeedTest';

const WEPNServices = new APIServices();
const DeviceServices = new RPIServices();
const RouterServices = new ModemServices();

export {
  WEPNServices as APIServices,
  DeviceServices as RPIServices,
  RouterServices as ModemServices,
  SpeedTest,
};

import RNFetchBlob from 'rn-fetch-blob';
import {
  deletePortForwardingXML,
  GetExternalIPAddressXML,
  addPortForwardingXML,
  getGenericPortMappingXML,
} from '../xml';

export default class ModemServices {
  makeRequest(method, url, body = {}, config = {}) {
    return RNFetchBlob.config({
      trusty: true,
      timeout: 15000,
      indicator: true,
      wifiOnly: true,
    }).fetch(
      method,
      url,
      {
        'Content-Type': 'text/xml; charset="utf-8"',
        Connection: 'close',
        'Content-Length': String(body.length),
        ...config.headers,
      },
      body
    );
  }

  get(url, body, config) {
    return this.makeRequest('GET', url, body, config);
  }

  post(url, body, config) {
    return this.makeRequest('POST', url, body, config);
  }

  addPortForwarding(params) {
    return this.post(
      `http://${params.ipAddress}:${params.port}${params.path}`,
      addPortForwardingXML(params),
      {
        headers: {
          SOAPAction: `${params.action}#AddPortMapping`,
        },
      }
    );
  }

  deletePortForwarding(params) {
    return this.post(
      `http://${params.ipAddress}:${params.port}${params.path}`,
      deletePortForwardingXML(params),
      {
        headers: {
          SOAPAction: `${params.action}#DeletePortMapping`,
        },
      }
    );
  }

  getGenericPortMapping(params) {
    return this.post(
      `http://${params.ipAddress}:${params.port}${params.path}`,
      getGenericPortMappingXML(params),
      {
        headers: {
          SOAPAction: `${params.action}#GetGenericPortMappingEntry`,
        },
      }
    );
  }

  getWanIP(params) {
    return this.post(
      `http://${params.ipAddress}:${params.port}${params.path}`,
      GetExternalIPAddressXML(params),
      {
        headers: {
          SOAPAction: `${params.action}#GetExternalIPAddress`,
        },
      }
    );
  }
}

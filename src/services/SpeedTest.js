import { fetchWithTimeout } from '../Utils';

const downloadSizeInBits = 20 * 1024 * 1024 * 8;
const metric = 'MBps';

export const SpeedTest = async () => {
  const url = 'https://status.we-pn.com/20MB.bin';

  const startTime = new Date().getTime();

  try {
    const response = await fetchWithTimeout(url, {
      method: 'GET',
      mode: 'no-cors',
      cache: 'no-cache',
      timeout: 10000,
    });
    console.log('response', { response });
    const endTime = new Date().getTime();
    const duration = (endTime - startTime) / 1000;
    const speed = (downloadSizeInBits / (1024 * 1024 * duration)).toFixed(2);

    return { metric, speed };
  } catch (error) {
    throw new Error(error);
  }
};

import { getIsProduction, getUserToken } from '../selectors';
import { clearObject } from '../Utils';

const Config = require('../../environment');
const Frisbee = require('frisbee');

export default class APIServices {
  constructor() {
    this.client = new Frisbee({
      logRequest: console.log,
      logResponse: console.log,
    });
  }

  setStore = store => {
    this?.unsubscribe?.();

    this.store = store;
    this.unsubscribe = store.subscribe(this.onStateUpdate);
  };

  onStateUpdate = () => {
    this.state = this.store.getState();
    const access_token = getUserToken(this.state)?.access_token;
    this.client.setOptions({
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${access_token}`,
        cache: 'no-cache',
      },
    });
  };

  handleRequest = async (
    method,
    path,
    body = {},
    options = {},
    api = 'api'
  ) => {
    const isProduction = getIsProduction(this.state);
    const baseURI = isProduction ? Config.URL : Config.DEV_URL;

    return this.client[method](`${baseURI}/${api}/${path}`, {
      headers: options.headers,
      body: clearObject(body),
      ...options,
    }).then(this.getContent);
  };

  getContent = async response => {
    if (!response.ok) throw { ...response.body, statusCode: response.status };
    return response.body;
  };

  // Configuration

  get(url, params, options, api) {
    return this.handleRequest('get', url, params, options, api);
  }

  post(url, params, options, api) {
    return this.handleRequest('post', url, params, options, api);
  }

  put(url, params, options, api) {
    return this.handleRequest('put', url, params, options, api);
  }

  patch(url, params, options, api) {
    return this.handleRequest('patch', url, params, options, api);
  }

  delete(url, params, options, api) {
    return this.handleRequest('delete', url, params, options, api);
  }

  // Service

  checkService() {
    return this.get('');
  }

  // Authentication

  login(params) {
    return this.post(
      'token/',
      {
        client_id: Config.CLIENT_ID,
        client_secret: Config.CLIENT_SECRET,
        ...params,
      },
      {},
      'o'
    );
  }

  signup(params) {
    return this.post(
      'user/',
      {
        client_id: Config.CLIENT_ID,
        client_secret: Config.CLIENT_SECRET,
        ...params,
      },
      {}
    );
  }

  forgotPassword(params) {
    return this.post('user/forgot_password/', params);
  }

  logout(params) {
    return this.post(
      'revoke_token/',
      {
        client_id: Config.CLIENT_ID,
        client_secret: Config.CLIENT_SECRET,
        ...params,
      },
      {
        headers: {
          'content-Type': 'application/json',
          'Accept-Charset': 'UTF-8',
          Authorization: `Bearer ${params?.token}`,
        },
      },
      'o'
    );
  }

  resetPassword(params) {
    return this.post('user/reset_password/', params, {
      dataType: 'jsonp',
    });
  }

  // Devices

  getDevices(params) {
    return this.get('device/', params);
  }

  getDevice(params) {
    const action = params.command ? params.command + '/' : '';
    return this.get(`device/${params.id}/${action}`);
  }

  claimDevice(params) {
    return this.post('device/claim/', params);
  }

  unclaimDevice(params) {
    return this.get(`device/${params.id}/unclaim/`, params);
  }

  updateDevice(params) {
    return this.patch(`device/${params.id}/`, params, { dataType: 'jsonp' });
  }

  shareAccessLink(params) {
    return this.get(`device/`, params, { dataType: 'jsonp' });
  }

  getDeviceDiagnosis(params) {
    return this.post('device/diagnosis/', params, { dataType: 'jsonp' });
  }

  // Friends

  getFriends(params) {
    return this.get('friend/', params);
  }

  getFriend(params) {
    return this.get(`friend/${params.id}/`, params);
  }

  addFriend(params) {
    return this.post('friend/', params);
  }

  updateFriend(params) {
    return this.patch(`friend/${params.id}/`, params);
  }

  deleteFriend(params) {
    return this.delete(`friend/${params.id}/`, params);
  }

  // Tests

  testPortForwarding(params) {
    return this.post('experiment/', params);
  }

  getExperimentResults(params) {
    return this.get(`/experiment/${params.id}`, params.body);
  }
}

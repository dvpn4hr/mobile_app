import { Platform } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';
import { NetworkInfo } from 'react-native-network-info';
import range from 'lodash/range';
import promiseAny from 'promise-any';

const wrapper = Platform.OS === 'ios' ? 'getIPAddress' : 'getGatewayIPAddress';

export default class RPIServices {
  makeRequest(method, url, body = {}, config = {}) {
    return RNFetchBlob.config({
      trusty: true,
      timeout: 15000,
      indicator: true,
      wifiOnly: true,
      ...config,
    }).fetch(
      method,
      url,
      {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        cache: 'no-cache',
      },
      JSON.stringify(body)
    );
  }

  get(url, body, config) {
    return this.makeRequest('GET', url, body, config);
  }

  /**
   * TODO: Fix This!
   * We shouldn't do this here
   * but until we can get this package work in iOS
   * we can use this WORKAROUND
   * https://github.com/Odinvt/react-native-lanscan
   */

  async searchDevice(params) {
    const curIPAddress = await NetworkInfo[wrapper]();
    const hostIndex = curIPAddress.lastIndexOf('.');
    const networkID = curIPAddress.substring(0, hostIndex);
    const hostsMap = range(255).map(async hostID => {
      const result = await this.get(
        `https://${networkID}.${hostID}:5000/api/v1/claim/info`,
        params
      );
      return { ...result, hostID };
    });
    const data = await promiseAny(hostsMap);
    return { ...data, networkID };
  }

  getErrorLog(params) {
    const URLParams = params?.local_token
      ? `?local_token=${params.local_token}`
      : '';
    return this.get(
      `https://${params.deviceIP}:5000/api/v1/diagnostics/error_log${URLParams}`,
      params
    );
  }

  getAccessLink(params) {
    return this.get(
      encodeURI(
        encodeURI(
          `https://${params.deviceIP}:5000/api/v1/friends/access_links/?local_token=${params.local_token}&certname=${params.certname}`
        )
      ),
      params
    );
  }

  getProgress(params) {
    return this.get(
      `https://${params.deviceIP}:5000/api/v1/claim/progress`,
      params
    );
  }
}
